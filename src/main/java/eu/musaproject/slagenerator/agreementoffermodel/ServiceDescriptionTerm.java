package eu.musaproject.slagenerator.agreementoffermodel;


import javax.xml.bind.annotation.*;
import java.io.Serializable;

/**
 * Created by adispataru on 4/24/15.
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "name", "serviceName",
        "serviceDescription",
})
@XmlRootElement(name = "ServiceDescriptionTerm", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
public class ServiceDescriptionTerm extends Term implements Serializable{
    private static final long serialVersionUID = -1107194530091494280L;
    @XmlAttribute(name = "Name", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private String name;
    @XmlAttribute(name = "ServiceName", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private String serviceName;
    @XmlElement(name = "serviceDescription", namespace = "http://www.specs-project.eu/resources/schemas/xml/SLAtemplate")
    private ServiceDescriptionType serviceDescription;

    public ServiceDescriptionType getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(ServiceDescriptionType serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
}
