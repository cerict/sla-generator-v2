package eu.musaproject.slagenerator.agreementoffermodel;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

/**
 * Created by adispataru on 8/3/15.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceReference", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement", propOrder = {
        "name", "serviceName", "endpoint"
})
@XmlRootElement(namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement", name = "ServiceReference")
public class ServiceReference extends Term implements Serializable {

    private static final long serialVersionUID = -7086083746242632932L;
    @XmlAttribute(name = "Name", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private String name;
    @XmlAttribute(name = "ServiceName", namespace = "http://schemas.ggf.org/graap/2007/03/ws-agreement")
    private String serviceName;
    @XmlElement(name = "endpoint", namespace = "http://www.specs-project.eu/resources/schemas/xml/SLAtemplate")
    private String endpoint;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }
}
