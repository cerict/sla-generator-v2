package eu.musaproject.slagenerator.agreementoffermodel;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SLO" type="{http://www.specs-project.eu/resources/schemas/xml/SLAtemplate}SLOType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "slo"
})
@XmlRootElement(name = "objectiveList", namespace = "http://www.specs-project.eu/resources/schemas/xml/SLAtemplate")
public class ObjectiveList implements Serializable {

    private static final long serialVersionUID = -3172152836381310737L;
    @XmlElement(name = "SLO", namespace = "http://www.specs-project.eu/resources/schemas/xml/SLAtemplate", required = true)
    protected List<SLOType> slo;

    public void setSlo(List<SLOType> slo) {
		this.slo = slo;
	}

	/**
     * Gets the value of the slo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the slo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSLO().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SLOType }
     * 
     * 
     */
    public List<SLOType> getSLO() {
        if (slo == null) {
            slo = new ArrayList<SLOType>();
        }
        return this.slo;
    }

}
