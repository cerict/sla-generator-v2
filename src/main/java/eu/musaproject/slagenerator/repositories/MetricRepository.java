package eu.musaproject.slagenerator.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.musaproject.model.Metric;

public interface MetricRepository extends JpaRepository<Metric, Long> {
	
	Metric findByMetricId(String metricId);
}
