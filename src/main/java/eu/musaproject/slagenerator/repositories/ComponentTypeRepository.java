package eu.musaproject.slagenerator.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import eu.musaproject.model.ComponentType;

@Repository
public interface ComponentTypeRepository extends JpaRepository<ComponentType, Long> {

	ComponentType findByName(String name);
	
	@Query(value="SELECT name FROM component_types", nativeQuery = true)
	List<String> getAllComponentTypes();
	
	@Query(value = "SELECT * FROM component_types_threats", nativeQuery = true )
	List<String> getComponentTypeThreads();

}
