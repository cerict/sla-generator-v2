package eu.musaproject.slagenerator.repositories;

import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import eu.musaproject.model.Component;
import eu.musaproject.model.ComponentControl;
import eu.musaproject.model.ComponentControlMetric;


@Repository
public interface ComponentControlMetricRepository extends JpaRepository<ComponentControlMetric, Long> {

		Set<ComponentControlMetric> findByComponentControlAndSelected(ComponentControl componentControl, Boolean selected);

		@Modifying
	    @Transactional
	    @Query("delete from ComponentControlMetric ctm where ctm.componentControl = ?1")
	    void deleteComponentControlMetricsByComponentControl(ComponentControl componentControl);
}
