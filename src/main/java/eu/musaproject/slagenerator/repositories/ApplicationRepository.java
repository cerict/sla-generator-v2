package eu.musaproject.slagenerator.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.musaproject.model.Application;

public interface ApplicationRepository extends JpaRepository<Application, Long> {

	Application findByKanbanId(String kanban_id);
	Application findByMacmId(String macmId);

}
