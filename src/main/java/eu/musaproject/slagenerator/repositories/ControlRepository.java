package eu.musaproject.slagenerator.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import eu.musaproject.model.Control;

public interface ControlRepository extends JpaRepository<Control, Long> {
	
	Control findByControlId(String controlId);
	
	@Query(value = "SELECT * FROM controls_metrics", nativeQuery = true )
	List<String> getControlMetrics();
	
	@Query(value = "SELECT * FROM controls_questions_control", nativeQuery = true )
	List<String> getControlQuestions();
	
    @Query(value = "select distinct (c.familyName) from Control c")
    List<String> findControlFamilies();
    
    List<Control> findByFamilyName(String familyName);
    
    @Query("select c from Control c join c.metrics m where (m.metricId= :metricId)")
    List<Control> findByMetricId(@Param("metricId")String metricId);
}
