package eu.musaproject.slagenerator.service;

import java.io.StringReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import eu.musaproject.enumeration.ComponentState;
import eu.musaproject.model.Application;
import eu.musaproject.model.Component;
import eu.musaproject.model.ComponentThreat;
import eu.musaproject.model.ComponentControl;
import eu.musaproject.model.ComponentType;
import eu.musaproject.model.Control;
import eu.musaproject.model.Threat;
import eu.musaproject.slagenerator.agreementoffermodel.AbstractSecurityControl;
import eu.musaproject.slagenerator.agreementoffermodel.AgreementOffer;
import eu.musaproject.slagenerator.agreementoffermodel.CapabilityType;
import eu.musaproject.slagenerator.agreementoffermodel.ControlFramework;
import eu.musaproject.slagenerator.agreementoffermodel.NISTSecurityControl;
import eu.musaproject.slagenerator.agreementoffermodel.ServiceDescriptionTerm;
import eu.musaproject.slagenerator.agreementoffermodel.ServiceDescriptionType.Capabilities;
import eu.musaproject.slagenerator.agreementoffermodel.Term;
import eu.musaproject.slagenerator.entity.JsonRiskAssessmentInput;
import eu.musaproject.slagenerator.entity.JsonRiskAssessmentResult;
import eu.musaproject.slagenerator.entity.RiskApp;
import eu.musaproject.slagenerator.entity.RiskComponent;
import eu.musaproject.slagenerator.repositories.ApplicationRepository;
import eu.musaproject.slagenerator.repositories.ComponentRepository;
import eu.musaproject.slagenerator.repositories.ComponentTypeRepository;
import eu.musaproject.slagenerator.repositories.ControlRepository;
import eu.musaproject.slagenerator.repositories.ThreatRepository;

@Service
public class RiskJsonHelper {
	public static final Logger logger = LoggerFactory.getLogger(RiskJsonHelper.class);

	@Autowired ApplicationRepository applicationRepository;
	@Autowired ComponentRepository componentRepository;
	@Autowired ComponentTypeRepository componentTypeRepository;
	@Autowired ThreatRepository threatRepository;
	@Autowired ControlRepository controlRepository;
	
	public List<RiskComponent> getRiskComponentsFromAppRiskJson(String appRiskJson){
		Type listType = new TypeToken<ArrayList<RiskApp>>(){}.getType();

		List<RiskApp> risksApp = new Gson().fromJson(appRiskJson, listType);
		
		
		logger.info("Start with getting of all the components from risk JSON");
		List<RiskComponent> rickComponents = new ArrayList<RiskComponent>();
		for(RiskApp riskApp : risksApp){
			logger.info("Checking JSON, element riskApp: "+riskApp.getName());
			for(RiskComponent riskComponent : riskApp.getItems()){
				logger.info("Adding riskComponent item: "+riskComponent.getText());
				rickComponents.add(riskComponent);
			}
		}
		return rickComponents;
	}
	
	public RiskComponent getRiskComponentFromAppRiskJson(Application application, String appRiskJson, String componentKanbanId){
		Type listType = new TypeToken<ArrayList<RiskApp>>(){}.getType();

		List<RiskApp> risksApp = new Gson().fromJson(appRiskJson, listType);
		RiskComponent riskComponentToReturn = null;
		
		logger.info("Start with adding of other components in database");
		for(RiskApp riskApp : risksApp){
			logger.info("Checking JSON, element riskApp: "+riskApp.getName());
			for(RiskComponent riskComponent : riskApp.getItems()){
//				logger.info("Checking JSON, item riskComponent: "+riskComponent.getText());
				if(riskComponent.getCid().equals(componentKanbanId)){
//					logger.error("The riskComponent is equal to component sent into url - CID: "+riskComponent.getCid());
					riskComponentToReturn = riskComponent;
				}else{
//					logger.info("Checking if the component is already saved in db...");
					Component component = componentRepository.findByKanbanId(riskComponent.getCid());
					if(component == null){
						logger.info("The component is not present in db, saving it...");
						component = new Component();
						component.setName(riskComponent.getText());
						component.setSlatSync(false);
						component.setState(ComponentState.CREATED);
						component.setKanbanId(riskComponent.getCid());
						ComponentType componentType = componentTypeRepository.findByName("CUSTOM WEB APP");
						component.setComponentType(componentType);
//						logger.info("Component with id: "+riskComponent.getCid()+" added to the application!");
						application.addComponent(component);
					}else{
						logger.error("The component is already present in db!");
					}
				}
			}
		}		

		return riskComponentToReturn;
	}

	public Component initRiskJsonModelOnComponent(RiskComponent riskComponent, Component component){
		component.setName(riskComponent.getText());
		component.setDescription(riskComponent.getCid());

		ComponentType componentType = componentTypeRepository.findByName("CUSTOM WEB APP");

		component.setComponentType(componentType);

		if(riskComponent.getRisks() != null){
			for (RiskComponent.RiskThreat riskThreat : riskComponent.getRisks()){

				Threat threat = threatRepository.findByThreatId(riskThreat.getThreatId());

				if(threat != null){
					ComponentThreat componentThreat = new ComponentThreat();
					componentThreat.setThreat(threat);
					componentThreat.setAwareness(threat.getAwareness());
					componentThreat.setEaseOfDiscovery(threat.getEaseOfDiscovery());
					componentThreat.setEaseOfExploit(threat.getEaseOfExploit());
					componentThreat.setIntrusionDetection(threat.getIntrusionDetection());
					componentThreat.setLossOfAccountability(threat.getLossOfAccountability());
					componentThreat.setLossOfAvailability(threat.getLossOfAvailability());
					componentThreat.setLossOfConfidentiality(threat.getLossOfConfidentiality());
					componentThreat.setLossOfIntegrity(threat.getLossOfIntegrity());
					componentThreat.setMotive(threat.getMotive());
					componentThreat.setOpportunity(threat.getOpportunity());
					componentThreat.setSize(threat.getSize());
					componentThreat.setSkillLevel(threat.getSkillLevel());

					if(riskThreat.getsR() != null){
						for(RiskComponent.RiskControl riskControl : riskThreat.getsR()){
							Control control = controlRepository.findByControlId(riskControl.getId());

							if(control != null){
								ComponentControl componentControl = new ComponentControl();
								componentControl.setControl(control);
								component.addComponentControl(componentControl);
							}else{
								logger.error("Control in json not found! "+riskControl.getId());
							}

						}
					}else{
						logger.error("The threat "+riskThreat.getThreatId()+" has not controls");
					}

					component.addComponentThreat(componentThreat);
				}else{
					logger.error("Threat in json not found! "+riskThreat.getThreatId());
				}
			}
		}else{
			logger.error("The component has not threats");
		}

//		logger.info("ComponentThreats added to component: "+component.getComponentThreats().size());
		component.setSlatSync(false);
		component.setState(ComponentState.PROFILED);

		return component;
	}

	public Long initRiskJsonModelFromAssessment(long idComponent, String assessmentJson){
		Component component = componentRepository.findOne(idComponent);
		if(component != null){
			Type listType = new TypeToken<ArrayList<JsonRiskAssessmentResult>>(){}.getType();

			List<JsonRiskAssessmentResult> jsonResults = new Gson().fromJson(assessmentJson, listType);

			Threat threat = threatRepository.findAll().get(0);

			ComponentThreat componentThreat = new ComponentThreat();
			componentThreat.setThreat(threat);
			componentThreat.setAwareness(threat.getAwareness());
			componentThreat.setEaseOfDiscovery(threat.getEaseOfDiscovery());
			componentThreat.setEaseOfExploit(threat.getEaseOfExploit());
			componentThreat.setIntrusionDetection(threat.getIntrusionDetection());
			componentThreat.setLossOfAccountability(threat.getLossOfAccountability());
			componentThreat.setLossOfAvailability(threat.getLossOfAvailability());
			componentThreat.setLossOfConfidentiality(threat.getLossOfConfidentiality());
			componentThreat.setLossOfIntegrity(threat.getLossOfIntegrity());
			componentThreat.setMotive(threat.getMotive());
			componentThreat.setOpportunity(threat.getOpportunity());
			componentThreat.setSize(threat.getSize());
			componentThreat.setSkillLevel(threat.getSkillLevel());

			for(JsonRiskAssessmentResult jsonResult : jsonResults){
				Control control = controlRepository.findByControlId(jsonResult.getFamily_id());

				if(control != null){
					ComponentControl componentControl = new ComponentControl();
					componentControl.setControl(control);
					component.addComponentControl(componentControl);
				}else{
					logger.error("Control in json not found! "+jsonResult.getFamily_id());
				}

			}
			component.addComponentThreat(componentThreat);

		}
		componentRepository.save(component);

		return component.getId();
	}

	public String generateRiskFromSla(String slaXml){

		List<JsonRiskAssessmentInput> jsonRiskAssessments = new ArrayList<JsonRiskAssessmentInput>();
		try {
			JAXBContext jc = JAXBContext.newInstance(AgreementOffer.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			AgreementOffer offer = (AgreementOffer) unmarshaller.unmarshal(new StringReader(
					slaXml));

			Integer controlCount = 1;
			List<Term> terms = offer.getTerms().getAll().getAll();
			Iterator iterator = terms.iterator();
			while(iterator.hasNext()){
				Object serviceDescriptionTermObj = iterator.next();
				if(serviceDescriptionTermObj instanceof ServiceDescriptionTerm){
					ServiceDescriptionTerm serviceDescriptionTerm = (ServiceDescriptionTerm) serviceDescriptionTermObj;
					Capabilities capabilities = serviceDescriptionTerm.getServiceDescription().getCapabilities();
					List<CapabilityType> capabilityTypes = capabilities.getCapability();
					for(int i = 0; i < capabilityTypes .size(); i++){
						ControlFramework controlFramework = capabilityTypes.get(i).getControlFramework();
						List<AbstractSecurityControl> abstractSecurityControls = controlFramework.getSecurityControl();

						for(int j = 0; j < abstractSecurityControls .size(); j++){
							if(abstractSecurityControls.get(j) instanceof NISTSecurityControl){
								NISTSecurityControl nistSecurityControl = (NISTSecurityControl) abstractSecurityControls.get(j);
								System.out.println("Security control id: "+nistSecurityControl.getId());

								JsonRiskAssessmentInput jsonRiskAssessment = new JsonRiskAssessmentInput(controlCount.toString(), 
										nistSecurityControl.getControlFamily(), nistSecurityControl.getId(), nistSecurityControl.getId(), nistSecurityControl.getName());
								jsonRiskAssessments.add(jsonRiskAssessment);
							}					 		

						}
					}
				}
			}
		} catch (JAXBException e) {
			logger.info(e.getMessage());
			return null;
		}

		String riskJsonFromWsag = new Gson().toJson(jsonRiskAssessments);
		System.out.println("Risk Json From wsag: "+riskJsonFromWsag);
		return riskJsonFromWsag;
	}
}
