package eu.musaproject.slagenerator.service;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.musaproject.enumeration.ApplicationState;
import eu.musaproject.enumeration.ComponentState;
import eu.musaproject.model.Application;
import eu.musaproject.model.Component;
import eu.musaproject.slagenerator.repositories.ApplicationRepository;
import eu.musaproject.slagenerator.repositories.ComponentRepository;

@Service
public class ComposerHelper {
	public static final Logger logger = LoggerFactory.getLogger(ComposerHelper.class);

	@Autowired ApplicationRepository applicationRepository;
	@Autowired ComponentRepository componentRepository;

	@Autowired RestClient restClient;

	public String composeApplication(long appId){

//		try {
//			SlaGenComposer genComposer = new SlaGenComposer(applicationRepository, componentRepository, appId, restClient);
//			genComposer.session();

			//Update status of application and components
			logger.info("Update status of application and components");
			Application app = applicationRepository.findOne(appId);
			Set<Component> components = app.getComponents();
			boolean componentNotGenerated = false;
			for(Component component : components){
				if(component.getSlatSync()){
					component.setState(ComponentState.SLA_GENERATED);
					componentRepository.save(component);
				}else{
					logger.error("Component not synched found! "+component.getName());
					componentNotGenerated = true;
				}
			}

			if(!componentNotGenerated){
				if(app.getState().equals(ApplicationState.SLATs_GENERATED)){
					app.setState(ApplicationState.SLAs_GENERATED);
				}else if(app.getState().equals(ApplicationState.SLATs_ASSESSED)){
					app.setState(ApplicationState.ASSESSED_SLAs_GENERATED);
				}
			}else{
				//In this case there is at least one component that is not syncronized with composer
				logger.error("There is at least one component that is not syncronized with composer");
				return "There is at least one component that is not syncronized with composer";
			}

			applicationRepository.save(app);
//		} catch (SQLException e) {
//			e.printStackTrace();
//			return "SQLException during composition!";
//		} catch (IOException e) {
//			e.printStackTrace();
//			return "IOException during composition!";
//		}
		return null;
	}
}
