package eu.musaproject.slagenerator.service;


import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.Base64;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

@Service
public class RestClient {
	public static final Logger logger = LoggerFactory.getLogger(RestClient.class);

	@Value("${component.kanban.dev}")
	private Boolean kanbanDevEnabled;

	@Value("${component.kanban.riskprofile}")
	private String kanbanRiskProfilePath;

	@Value("${component.kanban.username}")
	private String kanbanUsername;

	@Value("${component.kanban.password}")
	private String kanbanPassword;

	@Value("${component.kanban.dev.riskprofile}")
	private String kanbanDevRiskProfilePath;

	@Value("${component.kanban.dev.username}")
	private String kanbanDevUsername;

	@Value("${component.kanban.dev.password}")
	private String kanbanDevPassword;

	@Value("${component.sla.manager}")
	private String slaManagerPath;

	@Value("${component.kanban.getsession}")
	private String kanbanAppSession;

	@Value("${component.kanban.slamanager}")
	private String kanbanSlaManagerPath;

	public String requestAppRiskJsonFromKanban(String appId){
		RestTemplate restTemplate = new RestTemplate();
		logger.info("Is dev enabled:"+kanbanDevEnabled+"!");
		//Questa chiamata mi torna tutti i componenti dell'applicativo ed i risk profile associati
		String url = kanbanRiskProfilePath+"/"+appId;
		if(kanbanDevEnabled){
			url = kanbanDevRiskProfilePath+"/"+appId;
		}
		logger.info("Url to get riskprofile: "+url);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.TEXT_PLAIN);

		String auth = kanbanUsername + ":" + kanbanPassword;
		if(kanbanDevEnabled){
			auth = kanbanDevUsername + ":" + kanbanDevPassword;
		}
		byte[] encodedAuth = Base64.getEncoder().encode( 
				auth.getBytes(Charset.forName("US-ASCII")) );
		String authHeader = "Basic " + new String( encodedAuth );
		headers.set( "Authorization", authHeader );
		logger.info("Auth header: "+authHeader);
		System.out.println("Authorization header: "+authHeader);

		HttpEntity entity = new HttpEntity(headers);

		try{
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
			if(response.getStatusCode() == HttpStatus.OK){
				String appRiskProfile = response.getBody();
				return appRiskProfile;
			}else{
				return null;
			}
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	public String requestComponentRiskJsonFromKanban(String appId, String componentId, String jwtToken){
		RestTemplate restTemplate = new RestTemplate();
		logger.info("Is dev enabled:"+kanbanDevEnabled+"!");

		String url = kanbanRiskProfilePath+"/"+appId+"/component/"+componentId;
		if(kanbanDevEnabled){
			url = kanbanDevRiskProfilePath+"/"+appId+"/component/"+componentId;
		}
		logger.info("Url to get component riskprofile: "+url);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.TEXT_PLAIN);

		/*String auth = kanbanUsername + ":" + kanbanPassword;
		if(kanbanDevEnabled){
			auth = kanbanDevUsername + ":" + kanbanDevPassword;
		}
		byte[] encodedAuth = Base64.getEncoder().encode( 
				auth.getBytes(Charset.forName("US-ASCII")) );
		String authHeader = "Basic " + new String( encodedAuth );
		headers.set( "Authorization", authHeader );
		logger.info("Auth header: "+authHeader);*/

		String authHeader = jwtToken;
		headers.set( "Authorization", authHeader );

		System.out.println("Authorization header: "+authHeader);

		HttpEntity entity = new HttpEntity(headers);

		try{
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
			if(response.getStatusCode() == HttpStatus.OK){
				String componentRiskProfile = response.getBody();
				logger.info("Risk Profile get: "+componentRiskProfile);	
				return componentRiskProfile;
			}else{
				return null;
			}
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}


	public ResponseEntity<String> putSlaToRepo(String slaTypeToStore, String oldGeneratedSlaId, String oldAssessedSlaId, String oldComposedSlaId, String slaXml, String appId, String appKanbanId, String appMacmId, String compId, String compKanbanId){
		RestTemplate restPutSla = new RestTemplate();

		HttpHeaders headersPutSla = new HttpHeaders();
		headersPutSla.setContentType(MediaType.TEXT_XML);
		HttpEntity entityPuSla = new HttpEntity(slaXml, headersPutSla);

		String slaManagerPathToUse = slaManagerPath;
		HttpMethod methodToUse = HttpMethod.POST;

		String oldSlaId = null;
		if(slaTypeToStore == "GENERATED"){
			if(oldGeneratedSlaId != null && !oldGeneratedSlaId.equals("")){
				slaManagerPathToUse = slaManagerPath+"/"+oldGeneratedSlaId;
				methodToUse = HttpMethod.PUT;
			}
		}else if(slaTypeToStore == "ASSESSED"){
			if(oldAssessedSlaId != null && !oldAssessedSlaId.equals("")){
				slaManagerPathToUse = slaManagerPath+"/"+oldAssessedSlaId;
				methodToUse = HttpMethod.PUT;
			}
		}else if(slaTypeToStore == "COMPOSED"){
			if(oldComposedSlaId != null && !oldComposedSlaId.equals("")){
				slaManagerPathToUse = slaManagerPath+"/"+oldComposedSlaId;
				methodToUse = HttpMethod.PUT;
			}
		}
		
		ResponseEntity<String> response = restPutSla.exchange(slaManagerPathToUse, methodToUse, entityPuSla,
				String.class);

		if((oldSlaId == null && response.getStatusCode() == HttpStatus.CREATED) ||
				(oldSlaId != null  && !oldSlaId.equals("") && response.getStatusCode() == HttpStatus.OK)){
			oldSlaId = response.getBody();
			RestTemplate restPutAnnotation = new RestTemplate();

			HttpHeaders headersPutAnnotation = new HttpHeaders();
			headersPutSla.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

			if(appId != null && !appId.equals("")){
				MultiValueMap<String, String> bodyMap = new LinkedMultiValueMap<String, String>();
				bodyMap.add("key", "appId");
				bodyMap.add("value", appId);
				bodyMap.add("timestamp", String.valueOf(System.currentTimeMillis()));

				HttpEntity entityPutAnnotation = new HttpEntity(bodyMap, headersPutAnnotation);

				restPutAnnotation.exchange(slaManagerPath+"/"+oldSlaId+"/annotations", HttpMethod.POST, entityPutAnnotation,
						String.class);
			}
			
			if(appMacmId != null && !appMacmId.equals("")){
				MultiValueMap<String, String> bodyMap = new LinkedMultiValueMap<String, String>();
				bodyMap.add("key", "appMacmId");
				bodyMap.add("value", appMacmId);
				bodyMap.add("timestamp", String.valueOf(System.currentTimeMillis()));

				HttpEntity entityPutAnnotation = new HttpEntity(bodyMap, headersPutAnnotation);

				restPutAnnotation.exchange(slaManagerPath+"/"+oldSlaId+"/annotations", HttpMethod.POST, entityPutAnnotation,
						String.class);
			}
			
			if(appKanbanId != null && !appKanbanId.equals("")){
				MultiValueMap<String, String> bodyMap = new LinkedMultiValueMap<String, String>();
				bodyMap.add("key", "appKanbanId");
				bodyMap.add("value", appKanbanId);
				bodyMap.add("timestamp", String.valueOf(System.currentTimeMillis()));

				HttpEntity entityPutAnnotation = new HttpEntity(bodyMap, headersPutAnnotation);

				restPutAnnotation.exchange(slaManagerPath+"/"+oldSlaId+"/annotations", HttpMethod.POST, entityPutAnnotation,
						String.class);
			}
			
			if(compId != null && !compId.equals("")){
				MultiValueMap<String, String> bodyMap = new LinkedMultiValueMap<String, String>();
				bodyMap.add("key", "compId");
				bodyMap.add("value", compId);
				bodyMap.add("timestamp", String.valueOf(System.currentTimeMillis()));

				HttpEntity entityPutAnnotation = new HttpEntity(bodyMap, headersPutAnnotation);

				restPutAnnotation.exchange(slaManagerPath+"/"+oldSlaId+"/annotations", HttpMethod.POST, entityPutAnnotation,
						String.class);
			}
			
			if(compKanbanId != null && !compKanbanId.equals("")){
				MultiValueMap<String, String> bodyMap = new LinkedMultiValueMap<String, String>();
				bodyMap.add("key", "compKanbanId");
				bodyMap.add("value", compKanbanId);
				bodyMap.add("timestamp", String.valueOf(System.currentTimeMillis()));

				HttpEntity entityPutAnnotation = new HttpEntity(bodyMap, headersPutAnnotation);

				restPutAnnotation.exchange(slaManagerPath+"/"+oldSlaId+"/annotations", HttpMethod.POST, entityPutAnnotation,
						String.class);
			}

		}
		return response;
	}

	public ResponseEntity<String> getSlaFromRepo(String slaId){
		RestTemplate restPutSla = new RestTemplate();

		HttpHeaders headersGetSla = new HttpHeaders();
		headersGetSla.setContentType(MediaType.TEXT_XML);
		HttpEntity entityGetSla = new HttpEntity(headersGetSla);
		logger.info("Sla MAnager path: "+slaManagerPath+"/"+slaId);;
		ResponseEntity<String> response = restPutSla.exchange(slaManagerPath+"/"+slaId, HttpMethod.GET, entityGetSla,
				String.class);
		return response;
	}

	public String getSlaIdFromRepoByAnnotation(String annotationKey, String annotationValue){
		RestTemplate restPutSla = new RestTemplate();

		HttpHeaders headersGetSla = new HttpHeaders();
		headersGetSla.setContentType(MediaType.TEXT_XML);
		HttpEntity entityGetSla = new HttpEntity(headersGetSla);
		ResponseEntity<String> response = restPutSla.exchange(slaManagerPath+"?annotationKey="+annotationKey+"&annotationValue="+annotationValue, HttpMethod.GET, entityGetSla,
				String.class);

		if(response.getStatusCode() == HttpStatus.OK){
			logger.info("Request to sla manager 200 OK");

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder;
			try {
				builder = factory.newDocumentBuilder();
				InputSource is = new InputSource(new StringReader(response.getBody()));
				Document doc = builder.parse(is);
				if(doc.getElementsByTagName("item").getLength() > 0){
					logger.info("Sla by annotation with key:"+annotationKey+" and value:"+annotationValue+" found!");
					String compSlaId = ((Element)doc.getElementsByTagName("item").item(0)).getAttribute("id");
					if(compSlaId != null){
						logger.info("Sla id parsed:"+compSlaId);
						return compSlaId;
					}else{
						return null;
					}
				}else{
					logger.error("Sla by annotation with key:"+annotationKey+" and value:"+annotationValue+" not found!");

				}
			} catch (ParserConfigurationException | SAXException | IOException e) {
				e.printStackTrace();
				return null;
			}
		}

		return null;
	}

	public String getAppSessionFromKanban(String appId, String jwtToken, String userId){
		RestTemplate restTemplate = new RestTemplate();

		String url = kanbanAppSession+"/"+appId;

		logger.info("Url to get app session: "+url);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.TEXT_PLAIN);

		/*String auth = kanbanUsername + ":" + kanbanPassword;
		if(kanbanDevEnabled){
			auth = kanbanDevUsername + ":" + kanbanDevPassword;
		}
		byte[] encodedAuth = Base64.getEncoder().encode( 
				auth.getBytes(Charset.forName("US-ASCII")) );
		String authHeader = "Basic " + new String( encodedAuth );
		headers.set( "Authorization", authHeader );
		logger.info("Auth header: "+authHeader);*/

		String authHeader = jwtToken;
		headers.set( "Authorization", authHeader );
		headers.set( "UserId", userId );

		System.out.println("Authorization header: "+authHeader);

		HttpEntity entity = new HttpEntity(headers);

		try{
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
			if(response.getStatusCode() == HttpStatus.OK){
				String appSession = response.getBody();
				return appSession;
			}else{
				return null;
			}
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}


	public String updateSession(String appId, String componentId, String jwtToken, String userId, String state, String slatId, 
			String assessedSlatId, String slaId){
		RestTemplate restTemplate = new RestTemplate();

		String url = kanbanAppSession+"/"+appId;

		logger.info("Url to get update app session: "+url);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.TEXT_PLAIN);

		/*String auth = kanbanUsername + ":" + kanbanPassword;
		if(kanbanDevEnabled){
			auth = kanbanDevUsername + ":" + kanbanDevPassword;
		}
		byte[] encodedAuth = Base64.getEncoder().encode( 
				auth.getBytes(Charset.forName("US-ASCII")) );
		String authHeader = "Basic " + new String( encodedAuth );
		headers.set( "Authorization", authHeader );
		logger.info("Auth header: "+authHeader);*/

		String authHeader = jwtToken;
		headers.set( "Authorization", authHeader );
		headers.set( "UserId", userId );

		System.out.println("Authorization header: "+authHeader);

		String appSession = getAppSessionFromKanban(appId, jwtToken, userId);
//		System.out.println("Json Session before edit: "+appSession);
		if(appSession != null){
			JSONObject obj = new JSONObject(appSession);
			JSONArray columns = obj.getJSONArray("columns");
			for (int i = 0; i < columns.length(); i++){
				JSONArray items = columns.getJSONObject(i).getJSONArray("items");
				for (int j = 0; j < items.length(); j++){
					String compId = items.getJSONObject(j).getString("cid");
					if(compId.equals(componentId)){
						if(state != null){
							items.getJSONObject(j).put("status", state);
						}

						if(slatId != null){
							items.getJSONObject(j).put("getSlaTemplateUrl", kanbanSlaManagerPath+"/"+slatId);
						}

						if(assessedSlatId != null){
							items.getJSONObject(j).put("getAssessedSlaTemplateUrl", kanbanSlaManagerPath+"/"+assessedSlatId);
						}

						if(slaId != null){
							items.getJSONObject(j).put("getSlaUrl", kanbanSlaManagerPath+"/"+slaId);
						}

						items.getJSONObject(j).put("timestamp", System.currentTimeMillis());

						break;
					}
				}
			}

//			System.out.println("Json Session after edit: "+obj.toString());
			HttpEntity entity = new HttpEntity(obj.toString(), headers);

			try{
				ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class);
				if(response.getStatusCode() == HttpStatus.OK){
					return null;
				}else{
					return "Update session failed! Error: "+response.getStatusCode()+" "+response.getBody();
				}
			}catch(Exception e){
				e.printStackTrace();
				return "Update session failed! Exception: "+e.getMessage();
			}
		}else{
			return "Update session failed! App session get error!";
		}
	}

}
