package eu.musaproject.slagenerator.service;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import eu.musaproject.model.Component;
import eu.musaproject.model.ComponentControl;
import eu.musaproject.model.ComponentControlMetric;
import eu.musaproject.model.ComponentControlQuestion;
import eu.musaproject.model.Control;
import eu.musaproject.multicloud.models.graphs.Peer;
import eu.musaproject.multicloud.models.graphs.Relationship;
import eu.musaproject.multicloud.models.macm.MACM;
import eu.musaproject.multicloud.models.macm.macmPeerType;
import eu.musaproject.multicloud.models.macm.macmRelationshipType;
import eu.musaproject.slagenerator.agreementoffermodel.AbstractMetricType;
import eu.musaproject.slagenerator.agreementoffermodel.AbstractMetricType.AbstractMetricDefinition;
import eu.musaproject.slagenerator.agreementoffermodel.AbstractSecurityControl;
import eu.musaproject.slagenerator.agreementoffermodel.AgreementOffer;
import eu.musaproject.slagenerator.agreementoffermodel.CapabilityType;
import eu.musaproject.slagenerator.agreementoffermodel.Context;
import eu.musaproject.slagenerator.agreementoffermodel.ControlFramework;
import eu.musaproject.slagenerator.agreementoffermodel.CustomServiceLevel;
import eu.musaproject.slagenerator.agreementoffermodel.GuaranteeTerm;
import eu.musaproject.slagenerator.agreementoffermodel.NISTSecurityControl;
import eu.musaproject.slagenerator.agreementoffermodel.ObjectiveList;
import eu.musaproject.slagenerator.agreementoffermodel.OneOpOperator;
import eu.musaproject.slagenerator.agreementoffermodel.SLOType;
import eu.musaproject.slagenerator.agreementoffermodel.SLOexpressionType;
import eu.musaproject.slagenerator.agreementoffermodel.SLOexpressionType.OneOpExpression;
import eu.musaproject.slagenerator.agreementoffermodel.ServiceDescriptionTerm;
import eu.musaproject.slagenerator.agreementoffermodel.ServiceDescriptionType;
import eu.musaproject.slagenerator.agreementoffermodel.ServiceDescriptionType.Capabilities;
import eu.musaproject.slagenerator.agreementoffermodel.ServiceDescriptionType.SecurityMetrics;
import eu.musaproject.slagenerator.agreementoffermodel.ServiceDescriptionType.ServiceResources;
import eu.musaproject.slagenerator.agreementoffermodel.ServiceLevelObjective;
import eu.musaproject.slagenerator.agreementoffermodel.ServiceProperties;
import eu.musaproject.slagenerator.agreementoffermodel.ServiceProperties.VariableSet;
import eu.musaproject.slagenerator.agreementoffermodel.Term;
import eu.musaproject.slagenerator.agreementoffermodel.Terms;
import eu.musaproject.slagenerator.agreementoffermodel.Terms.All;
import eu.musaproject.slagenerator.agreementoffermodel.Variable;
import eu.musaproject.slagenerator.repositories.ApplicationRepository;
import eu.musaproject.slagenerator.repositories.ComponentControlMetricRepository;
import eu.musaproject.slagenerator.repositories.ComponentRepository;
import eu.musaproject.slagenerator.repositories.ControlRepository;

@Service
public class SlaGeneratorHelper {
	public static final Logger logger = LoggerFactory.getLogger(SlaGeneratorHelper.class);

	@Autowired ApplicationRepository applicationRepository;
	@Autowired ComponentRepository componentRepository;
	@Autowired ComponentControlMetricRepository componentControlMetricRepository;
	@Autowired ControlRepository controlRepository;
	
	@Autowired RestClient restClient;
	@Autowired RiskJsonHelper riskJsonHelper;


	public String generateSla(Long componentId, boolean isAssessed){
		AgreementOffer sla = new AgreementOffer();

		Component component = componentRepository.findOne(componentId);


		if(!isAssessed){
			sla.setName(component.getName()+"_slat");
		}else{
			sla.setName(component.getName()+"_assessed_slat");
		}

		Context context = new Context();
		context.setAgreementInitiator("CUSTOMER");
		context.setAgreementResponder("PROVIDER");
		context.setServiceProvider("AgreementResponder");
		if(!isAssessed){
			context.setTemplateName("SLA_TEMPLATE");
		}else{
			context.setTemplateName("ASSESSED_SLA_TEMPLATE");
		}

		sla.setContext(context);

		Terms terms = new Terms();

		List<Term> termsList = new ArrayList<Term>();

		//Start of Service Description Term
		ServiceDescriptionTerm serviceDescriptionTerm = new ServiceDescriptionTerm();
		serviceDescriptionTerm.setName(component.getName()+"_slat");
		if(!isAssessed){
			serviceDescriptionTerm.setName(component.getName()+"_slat");
		}else{
			serviceDescriptionTerm.setName(component.getName()+"_assessed_slat");
		}
		serviceDescriptionTerm.setServiceName(component.getComponentType().getName());

		ServiceDescriptionType serviceDescriptionType = new ServiceDescriptionType();

		//Start of Capabilities Manage
		List<CapabilityType> capabilitiesTypeList = new ArrayList<CapabilityType>();

		Capabilities capabilities = new Capabilities();

		CapabilityType capabilityType = new CapabilityType();
		capabilityType.setId("MUSA_CAP");

		ControlFramework controFramework = new ControlFramework();

		controFramework.setId("NIST_800_53_r4");
		controFramework.setFrameworkName("NIST Control framework 800-53 rev. 4");

		//Start of controls setting
		List<AbstractSecurityControl> nistSecurityControls = new ArrayList<AbstractSecurityControl>();

		Iterator<ComponentControl> controlsIterator = component.getComponentControls().iterator();

		while(controlsIterator.hasNext()){

			ComponentControl componentControl = controlsIterator.next();

			boolean addControl = true;
			Iterator<ComponentControlQuestion> controlQuestionsIterator = componentControl.getControlQuestions().iterator();
			while(controlQuestionsIterator.hasNext()){
				ComponentControlQuestion componentControlQuestion = controlQuestionsIterator.next();
				if(componentControlQuestion.getAnswer() != null && 
						!componentControlQuestion.getAnswer().toLowerCase().equals("yes") && 
						!componentControlQuestion.getAnswer().toLowerCase().equals("not applicable")){
					addControl = false;
				}
			}

			if(addControl){
				NISTSecurityControl nistSecurityControl = new NISTSecurityControl();
				nistSecurityControl.setId(componentControl.getControl().getControlId());
				nistSecurityControl.setName(componentControl.getControl().getControlName());
				nistSecurityControl.setControlFamily(componentControl.getControl().getFamilyId());
				nistSecurityControl.setSecurityControl(componentControl.getControl().getControl().toString());
				nistSecurityControl.setControlEnhancement(componentControl.getControl().getEnhancement().toString());
				nistSecurityControl.setControlDescription(componentControl.getControl().getControlDescription());
				nistSecurityControl.setImportanceWeight(null);
				nistSecurityControls.add(nistSecurityControl);
			}

		}
		//End of controls setting

		//Add capabilities to Service Description Type
		controFramework.setSecurityControl(nistSecurityControls);
		capabilityType.setControlFramework(controFramework);
		capabilitiesTypeList.add(capabilityType);
		capabilities.setCapability(capabilitiesTypeList);
		serviceDescriptionType.setCapabilities(capabilities);
		serviceDescriptionType.setServiceResources(getServiceResourcesOfComponent(component));
		//End of Capabilities Manage

		//Start of Metrics Manage
		SecurityMetrics securityMetrics = new SecurityMetrics();

		List<AbstractMetricType> abstractMetricTypes = new ArrayList<AbstractMetricType>();

		controlsIterator = component.getComponentControls().iterator();

		while(controlsIterator.hasNext()){

			ComponentControl componentControl = controlsIterator.next();

			Set<ComponentControlMetric> componentControlMetrics = componentControlMetricRepository.findByComponentControlAndSelected(componentControl, true);
			Iterator<ComponentControlMetric> componentControlMetricsIterator = componentControlMetrics.iterator();

			while(componentControlMetricsIterator.hasNext()){
				ComponentControlMetric componentControlMetric = componentControlMetricsIterator.next();

				System.out.println("Is Assessed: "+isAssessed);
				System.out.println("Assessed Selected: "+componentControlMetric.getAssessedSelected());

				if((isAssessed && componentControlMetric.getAssessedSelected()) || !isAssessed){
					AbstractMetricType abstractMetricType = new AbstractMetricType();
					abstractMetricType.setName(componentControlMetric.getMetric().getMetricName());
					abstractMetricType.setReferenceId(componentControlMetric.getMetric().getMetricId());

					AbstractMetricDefinition abstractMetricDefinition = new AbstractMetricDefinition();
					abstractMetricDefinition.setDefinition(componentControlMetric.getMetric().getMetricDescription());
					abstractMetricDefinition.setExpression(componentControlMetric.getMetric().getFormula());

					abstractMetricType.setAbstractMetricDefinition(abstractMetricDefinition);

					abstractMetricTypes.add(abstractMetricType);
				}
			}

		}

		securityMetrics.setSecurityMetric(abstractMetricTypes);

		//Add metrics to Service Description Type
		serviceDescriptionType.setSecurityMetrics(securityMetrics);
		//End of Metrics Manage

		serviceDescriptionTerm.setServiceDescription(serviceDescriptionType);
		termsList.add(serviceDescriptionTerm);
		//End of Service Description Term

		//Start of Guarantee Term
		GuaranteeTerm guaranteeTerm = new GuaranteeTerm();
		guaranteeTerm.setName("//ns3:capability[@id='MUSA_CAP']");

		ServiceLevelObjective serviceLevelObjective = new ServiceLevelObjective();
		CustomServiceLevel customServiceLevel = new CustomServiceLevel();

		ObjectiveList objectiveList = new ObjectiveList();

		List<SLOType> slos = new ArrayList<SLOType>();

		controlsIterator = component.getComponentControls().iterator();
		int sloCount = 0;
		while(controlsIterator.hasNext()){

			ComponentControl componentControl = controlsIterator.next();
			Set<ComponentControlMetric> componentControlMetrics = componentControlMetricRepository.findByComponentControlAndSelected(componentControl, true);
			Iterator<ComponentControlMetric> componentControlMetricsIterator = componentControlMetrics.iterator();

			while(componentControlMetricsIterator.hasNext()){
				ComponentControlMetric componentControlMetric = componentControlMetricsIterator.next();
				if((isAssessed && componentControlMetric.getAssessedSelected()) || !isAssessed){
					SLOType slo = new SLOType();

					slo.setSLOID("slo"+String.valueOf(sloCount));
					slo.setMetricREF(componentControlMetric.getMetric().getMetricId());

					SLOexpressionType sLOexpressionType = new SLOexpressionType();

					OneOpExpression oneOpExpression = new OneOpExpression();
					oneOpExpression.setOperand(componentControlMetric.getValue());
					oneOpExpression.setOperator(getOperatorFromString(componentControlMetric.getMetric().getOperator()));

					sLOexpressionType.setOneOpExpression(oneOpExpression);

					slo.setSLOexpression(sLOexpressionType);

					slos.add(slo);
					sloCount++;
				}
			}

		}

		objectiveList.setSlo(slos);

		customServiceLevel.setObjectiveList(objectiveList);
		serviceLevelObjective.setCustomServiceLevel(customServiceLevel);
		guaranteeTerm.setServiceLevelObjective(serviceLevelObjective);
		termsList.add(guaranteeTerm);
		//End of Guarantee Term
		
		//Start of Service Properties
		ServiceProperties serviceProperties = new ServiceProperties();
		serviceProperties.setName("//ns3:capability[@id='MUSA_CAP']");
		serviceProperties.setServiceName(component.getName());
		
		VariableSet variableSet = new VariableSet();
		List<Variable> variables = new ArrayList<>();

		int count = 1;
		for(AbstractMetricType abstrMetricType : securityMetrics.getSecurityMetric()){
			Variable var = new Variable();
			var.setName("MUSA_"+component.getName()+"_M"+count);
			var.setMetric(abstrMetricType.getReferenceId());
			List<Control> controls = controlRepository.findByMetricId(abstrMetricType.getReferenceId());
			String location = "";
			for(int i = 0; i < controls.size(); i++){
				Control nistControl = controls.get(i);
					location += "//n3:NISTsecurityControl[@id='"+nistControl.getControlId()+"']";
					if(i < (controFramework.getSecurityControl().size() - 1)){
						location += " | ";
					}
			}
			var.setLocation(location);
			variables.add(var);
			count++;
		}
		variableSet.setVariables(variables);
		serviceProperties.setVariableSet(variableSet);
		termsList.add(serviceProperties);
		//Start of Service Properties

		All all = new All();
		all.setAll(termsList);
		terms.setAll(all);
		sla.setTerms(terms);
		
		//Generate sla xml from object
		try {
			String slaXml = buildXmlFromOffer(sla);
			//			logger.info("Sla xml: "+slaXml);
			return slaXml;
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return "";
	}

	private OneOpOperator getOperatorFromString(String value){
		if(value.equals("ge (>=)") || value.equals("ge(>=)")){
			return OneOpOperator.GREATER_THAN_OR_EQUAL;
		}else if(value.equals("leq (<=)") || value.equals("leq(<=)")){
			return OneOpOperator.LESS_THAN_OR_EQUAL;
		}else if(value.equals("eq (=)") || value.equals("eq(=)")){
			return OneOpOperator.EQUAL;
		}else if(value.equals("le (<=)") || value.equals("le(<=)")){
			return OneOpOperator.LESS_THAN_OR_EQUAL;
		}

		return null;
	}

	private List<ServiceDescriptionType.ServiceResources> getServiceResourcesOfComponent(Component component){
		logger.info("Getting of deploy information from MACM");

		MACM appMacm = new MACM();
		logger.info("Read neo for app with MACM id: "+component.getApplication().getMacmId());
		appMacm.readNeo(component.getApplication().getMacmId());

		//Devo trovare il peer di tipo CSP associato al componente

		//Cerco il peer che rappresenta il componente
		List<Peer> peers = appMacm.getPeersByType(macmPeerType.SaaS);
		logger.info("Number of peers SAAS found: "+peers.size());
		Peer componentPeer = null;
		if(peers.size() > 0){
			for(Peer peer : peers){
//				logger.info("component_id found: "+peer.getProperty("component_id"));
//				logger.info("component_id to find: "+component.getKanbanId().toString());
				if(peer.getProperty("component_id").equals(component.getKanbanId().toString())){
					componentPeer = peer;
					logger.info("Component peer found!");
					break;
				}
			}
		}

		Peer cspPeer = null;
		Peer vmPeer = null;

		if(componentPeer == null){
			logger.error("Component peer not found!");
		}else{

			//Cerco il peer che rappresenta la vm associata al componente
			List<Relationship> relationships = appMacm.getRelationship();

			for (Relationship relationship:relationships){
				if(relationship.type.equals(macmRelationshipType.hosts)){
//					logger.info("Relationship hosts found!");
//					logger.info("Start node: Name-"+relationship.startNode.getName()+" Type-"+relationship.startNode.getType());
//					logger.info("End node: Name-"+relationship.endNode.getName()+" Type-"+relationship.endNode.getType());

					if(relationship.startNode != null && relationship.endNode != null){
						Peer otherPeer = null;
						if(relationship.startNode.getProperty("component_id") != null){
							otherPeer = relationship.startNode.getProperty("component_id").equals(componentPeer.getProperty("component_id")) ? relationship.endNode : null;
						}
						if(otherPeer == null && relationship.endNode.getProperty("component_id") != null){
							otherPeer = relationship.endNode.getProperty("component_id").equals(componentPeer.getProperty("component_id")) ? relationship.startNode : null;
						}
//						logger.error("otherPeer "+(otherPeer != null ? "No NULL" : "Is Null"));
						if(otherPeer != null){
//							logger.info("otherPeer.getType(): "+otherPeer.getType());
//							logger.info("otherPeer.getName(): "+otherPeer.getName());
						}
						if(otherPeer != null && otherPeer.getType().equals(macmPeerType.IaaS)){
							vmPeer = otherPeer;
							logger.info("VM peer found!");
							break;
						}
					}
				}
			}

			if(vmPeer == null){
				logger.error("VM peer not found! I can't find CSP peer");
			}else{
				//Cerco il peer che rappresenta il csp associato alla vm del componente

				relationships = appMacm.getRelationship();
				for (Relationship relationship:relationships){
					if(relationship.type.equals(macmRelationshipType.provides)){
//						logger.info("Relationship provides found!");
//						logger.info("Start node is null: "+(relationship.startNode != null ? "NO" : "YES"));
//						logger.info("End node is null: "+(relationship.endNode != null ? "NO" : "YES"));

						if(relationship.startNode != null){
//							logger.info("Start node: Name-"+relationship.startNode.getName()+" Type-"+relationship.startNode.getType());
						}
						
						if(relationship.endNode != null){
//							logger.info("End node: Name-"+relationship.endNode.getName()+" Type-"+relationship.endNode.getType());
						}
						
						if(relationship.startNode != null && relationship.endNode != null){
							Peer otherPeer = null;
							otherPeer = (relationship.startNode.getName().equals(vmPeer.getName()) && relationship.endNode.getType().equals(macmPeerType.CSP)) ? relationship.endNode : null;

							if(otherPeer == null){
								otherPeer = (relationship.endNode.getName().equals(vmPeer.getName()) && relationship.startNode.getType().equals(macmPeerType.CSP)) ? relationship.startNode : null;
							}

							if(otherPeer != null && otherPeer.getType().equals(macmPeerType.CSP)){
								cspPeer = otherPeer;
								logger.info("CSP peer found!");
								break;
							}
						}

					}
				}
			}

			if(cspPeer == null){
				logger.error("CSP peer not found!");
			}
		}

		List<ServiceDescriptionType.ServiceResources> serviceResourcesList = new ArrayList<ServiceDescriptionType.ServiceResources>();
		ServiceDescriptionType.ServiceResources serviceResources = new ServiceDescriptionType.ServiceResources();
		List<ServiceDescriptionType.ServiceResources.ResourcesProvider> resourcesProviderList = new ArrayList<ServiceDescriptionType.ServiceResources.ResourcesProvider>();
		ServiceDescriptionType.ServiceResources.ResourcesProvider resourcesProvider = new ServiceDescriptionType.ServiceResources.ResourcesProvider();

		if(componentPeer != null && vmPeer != null && cspPeer != null){

			List<ServiceDescriptionType.ServiceResources.ResourcesProvider.VM> vmList = new ArrayList<ServiceDescriptionType.ServiceResources.ResourcesProvider.VM>();
			logger.info("Adding service resources info");

			logger.info("Adding service resources info for VM");
			logger.info("VM SLA name: "+"VM_"+vmPeer.getProperty("hardware_core")+"core_"+vmPeer.getProperty("hardware_ram")+"RAM_"+cspPeer.getName().toUpperCase());
			String storedSlaId = restClient.getSlaIdFromRepoByAnnotation("VM",
					"VM_"+vmPeer.getProperty("hardware_core")+"core_"+vmPeer.getProperty("hardware_ram")+"RAM_"+cspPeer.getName().toUpperCase());

			logger.info("VM sla id: "+storedSlaId);
			if(storedSlaId != null){
				ResponseEntity<String> response = restClient.getSlaFromRepo(storedSlaId);
				if(response.getStatusCode() == HttpStatus.OK){
					String vmSlaXml = response.getBody();
					AgreementOffer vmOffer = buildOfferFromXml(vmSlaXml);

					for(Term term : vmOffer.getTerms().getAll().getAll()){
						if(term instanceof ServiceDescriptionTerm){
							for(ServiceResources serviceResource : ((ServiceDescriptionTerm) term).getServiceDescription().getServiceResources()){
								if(serviceResource.getResourcesProvider().size() > 0){
									resourcesProvider = serviceResource.getResourcesProvider().get(0);
									resourcesProviderList.add(resourcesProvider);
									vmList = serviceResource.getResourcesProvider().get(0).getVM();
									if(resourcesProviderList.size() > 0){
										resourcesProviderList.get(0).setVM(vmList);
									}
								}else{
									logger.error("No ResourcesProvider found in VM SLA!");
								}
							}
							break;
						}
					}
				}else{
					logger.error("VM SLA id get error!");
				}
			}else{
				logger.error("VM SLA id not found!");
			}

			serviceResources.setResourcesProvider(resourcesProviderList);;
			serviceResourcesList.add(serviceResources);
			return serviceResourcesList;
		}else{
			resourcesProviderList.add(resourcesProvider);
			serviceResources.setResourcesProvider(resourcesProviderList);
			serviceResourcesList.add(serviceResources);
			return serviceResourcesList;
		}
	}

	public String buildXmlFromOffer(AgreementOffer offer) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(AgreementOffer.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(offer, sw);
		return sw.toString();
	}

	public AgreementOffer buildOfferFromXml(String xml) {
		AgreementOffer offer = null;
		try {
			JAXBContext jc = JAXBContext.newInstance(AgreementOffer.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			offer = (AgreementOffer) unmarshaller.unmarshal(new StringReader(
					xml));
		} catch (JAXBException e) {
			System.out.println("JAXBException "+e.getMessage());
		}
		return offer;
	}
}
