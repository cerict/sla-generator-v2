package eu.musaproject.slagenerator.service;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import eu.musaproject.enumeration.ApplicationState;
import eu.musaproject.enumeration.ComponentState;
import eu.musaproject.model.Application;
import eu.musaproject.model.Component;
import eu.musaproject.model.ComponentControl;
import eu.musaproject.model.ComponentControlQuestion;
import eu.musaproject.model.ComponentThreat;
import eu.musaproject.model.ComponentType;
import eu.musaproject.model.Control;
import eu.musaproject.model.QuestionControl;
import eu.musaproject.model.Threat;
import eu.musaproject.multicloud.models.graphs.Peer;
import eu.musaproject.multicloud.models.macm.MACM;
import eu.musaproject.multicloud.models.macm.macmPeerType;
import eu.musaproject.multicloud.utilities.NeoDriver;
import eu.musaproject.slagenerator.entity.RiskComponent;
import eu.musaproject.slagenerator.repositories.ApplicationRepository;
import eu.musaproject.slagenerator.repositories.ComponentControlRepository;
import eu.musaproject.slagenerator.repositories.ComponentRepository;
import eu.musaproject.slagenerator.repositories.ComponentThreatRepository;
import eu.musaproject.slagenerator.repositories.ComponentTypeRepository;
import eu.musaproject.slagenerator.repositories.ControlRepository;
import eu.musaproject.slagenerator.repositories.ThreatRepository;

@Service
public class AppSetupHelper {
	public static final Logger logger = LoggerFactory.getLogger(AppSetupHelper.class);

	@Autowired ApplicationRepository applicationRepository;
	@Autowired ComponentRepository componentRepository;
	@Autowired ComponentControlRepository componentControlRepository;
	@Autowired ComponentThreatRepository componentThreatRepository;
	@Autowired ComponentTypeRepository componentTypeRepository;
	@Autowired ThreatRepository threatRepository;
	@Autowired ControlRepository controlRepository;

	@Autowired RestClient restClient;
	@Autowired RiskJsonHelper riskJsonHelper;

	String errorMessage;


	public String getErrorMessage() {
		return errorMessage;
	}

	public Application setupApplicationFromMacmByAppId(String applicationId){
		Application application = applicationRepository.findByMacmId(applicationId);
		if(application == null){
			logger.info("The application does not exist in db, proceed with creation");
			application = new Application();
			application.setMacmId(applicationId);
			application.setKanbanId(null);
			application.setState(ApplicationState.CREATED);
		}

		try {
			MACM appMacm = new MACM();
			appMacm.readNeo(applicationId);

			//Prelevo tutti i peer di tipo saas
			List<Peer> peers = appMacm.getPeersByType(macmPeerType.SaaS);
			logger.info("Number of peers found: "+peers.size());
			if(peers.size() > 0){
				for(Peer peer : peers){

					Component component = componentRepository.findOne(Long.valueOf(peer.getProperty("component_id")));
					if(component == null){
						logger.info("Component is null. Adding component...");
						component  = new Component();
						component.setSlatSync(false);
						component.setName(peer.getProperty("name"));
						component.setState(ComponentState.CREATED);
						component.setComponentGroup(String.valueOf(peer.getType()));
						component.setKanbanId(peer.getProperty("component_id"));
						if(peer.getProperty("type").equals("COTS.Web") || peer.getProperty("type").equals("SERVICE.Web")){
							ComponentType componentType = componentTypeRepository.findByName("CUSTOM WEB APP");
							component.setComponentType(componentType);
						}else if(peer.getProperty("type").equals("COTS.Storage") || peer.getProperty("type").equals("SERVICE.Storage")){
							ComponentType componentType = componentTypeRepository.findByName("STORAGE AS SERVICE");
							component.setComponentType(componentType);
						}else{
							ComponentType componentType = componentTypeRepository.findByName("IDM");
							component.setComponentType(componentType);
						}
						application.addComponent(component);
					}else{
						logger.info("Component is already present.");
					}

				}
				applicationRepository.save(application);
			}else{
				errorMessage = "No application found!";
				return null;
			}
		} catch (NumberFormatException e1) {
			e1.printStackTrace();
			errorMessage = "No application found!";
			return null;
		}

		return application;
	}

	public Application setupApplicationFromMacmStringAndId(String appMacm, String appMacmId){
		MACM applicationMacm = new MACM();
		NeoDriver neo= new NeoDriver();
		Application application = new Application();
		application.setMacmId(appMacmId);
		application.setKanbanId(null);

		neo.execute(appMacm);
		applicationMacm.readNeo(appMacmId);
		logger.info("Neo readed!");

		application.setState(ApplicationState.CREATED);

		//Prelevo tutti i peer di tipo saas
		List<Peer> peers = applicationMacm.getPeersByType(macmPeerType.SaaS);
		logger.info("Number of peers found: "+peers.size());
		if(peers.size() > 0){
			for(Peer peer : peers){

				Component component = componentRepository.findOne(Long.valueOf(peer.getProperty("component_id")));
				if(component == null){
					component  = new Component();
					component.setSlatSync(false);
					component.setName(peer.getProperty("name"));
					component.setState(ComponentState.CREATED);
					component.setComponentGroup(String.valueOf(peer.getType()));
					component.setKanbanId(peer.getProperty("component_id"));
					if(peer.getProperty("type").equals("COTS.Web") || peer.getProperty("type").equals("SERVICE.Web")){
						ComponentType componentType = componentTypeRepository.findByName("CUSTOM WEB APP");
						component.setComponentType(componentType);
					}else if(peer.getProperty("type").equals("COTS.Storage") || peer.getProperty("type").equals("SERVICE.Storage")){
						ComponentType componentType = componentTypeRepository.findByName("STORAGE AS SERVICE");
						component.setComponentType(componentType);
					}else{
						ComponentType componentType = componentTypeRepository.findByName("IDM");
						component.setComponentType(componentType);
					}
					application.addComponent(component);
				}

			}
			applicationRepository.save(application);
			return application;
		}else{
			errorMessage = "Application hasn't components!";
			return null;
		}

	}

	//This method is called when the tool is opened from kanban
	public Application setupApplicationFromKanban(String applicationKanbanId, String componentKanbanId, String jwtToken, String userId){
		Application application = applicationRepository.findByKanbanId(applicationKanbanId);
		if(application == null){
			logger.info("The application does not exist in db, proceed with creation");
			application = new Application();
			application.setKanbanId(applicationKanbanId);
			application.setMacmId(applicationKanbanId);
			application.setState(ApplicationState.CREATED);
			applicationRepository.save(application);
		}

		try {
			MACM appMacm = new MACM();
			appMacm.readNeo(applicationKanbanId);

			//Prelevo tutti i peer di tipo saas
			List<Peer> peers = appMacm.getPeersByType(macmPeerType.SaaS);
			logger.info("Number of peers found: "+peers.size());

			if(peers.size() > 0){
				for(Peer peer : peers){
					if(peer.getType() == macmPeerType.SaaS){

						logger.info("Kanban ID of peer: "+peer.getProperty("component_id"));
						Component component = componentRepository.findByApplicationKanbanIdAndKanbanId(applicationKanbanId, peer.getProperty("component_id"));
						if(component == null){
							component  = new Component();
							component.setKanbanId(peer.getProperty("component_id"));
							component.setSlatSync(false);
							component.setName(peer.getProperty("name"));
							component.setComponentGroup(String.valueOf(peer.getType()));
							if(peer.getProperty("type").equals("COTS.Web") || peer.getProperty("type").equals("SERVICE.Web")){
								ComponentType componentType = componentTypeRepository.findByName("CUSTOM WEB APP");
								component.setComponentType(componentType);
							}else if(peer.getProperty("type").equals("COTS.Storage") || peer.getProperty("type").equals("SERVICE.Storage")){
								ComponentType componentType = componentTypeRepository.findByName("STORAGE AS SERVICE");
								component.setComponentType(componentType);
							}else{
								ComponentType componentType = componentTypeRepository.findByName("IDM");
								component.setComponentType(componentType);
							}
							application.addComponent(component);
							applicationRepository.save(application);
						}
						//Riprendo il componente dal db in questo modo sono sicuro ci sia anche l'id
						component = componentRepository.findByApplicationKanbanIdAndKanbanId(applicationKanbanId, peer.getProperty("component_id"));

						//Settare lo stato del componente pari a quello che trovo nel session
						component.setState(getStateOfComponentFromSession(applicationKanbanId, component.getKanbanId(), jwtToken, userId));
						//						component.setState(ComponentState.PROFILED);
						componentRepository.save(component);

						//adesso tocca settare il risk profile per il componente
						if(component.getState().equals(ComponentState.PROFILED)){
							String riskAnalysis = restClient.requestComponentRiskJsonFromKanban(applicationKanbanId, component.getKanbanId(), jwtToken);
							if(riskAnalysis != null){

								RiskComponent riskComponent = new Gson().fromJson(riskAnalysis, RiskComponent.class);
								logger.info("Analyze risk component CID: "+riskComponent.getCid());
								logger.info("Analyze risk component name: "+riskComponent.getText());


								component = initRiskJsonModelOnComponentWithSync(riskComponent, component);
								componentRepository.save(component);
								logger.info("Id component saved in db: "+component.getId());

							}else{
								/*Application app = new Application();
								app.setState(ApplicationState.ERROR);
								app.setName("The attempt to download the risk analysis for a component has failed!");
								return app;*/
								//TODO: al momento non torno errore perchè la get di alcuni risk profile va in errore e mi blocca il flusso
							}
						}
					}
				}

				//Check if there are component to delete
				logger.info("Check if components are all in MACM");
				for(Component component : application.getComponents()){
					boolean compFoundInMACM = false;
					for(Peer peer : peers){
						if(peer.getType() == macmPeerType.SaaS){
							if(component.getKanbanId().equals(peer.getProperty("component_id"))){
								compFoundInMACM = true;
								break;
							}
						}
					}
					if(!compFoundInMACM){
						logger.info("Component "+component.getKanbanId()+" is not in MACM. I remove it!");
						componentRepository.delete(component);
					}else{
						logger.info("Component "+component.getKanbanId()+" is in MACM!");
					}
				}

			}else{
				errorMessage = "The application with id: "+applicationKanbanId+" no has peers in neo4j!";
				return null;
			}
		} catch (NumberFormatException e1) {
			e1.printStackTrace();
			errorMessage = "Exception! Number format exception!";
			return null;
		}

		return application;
	}

	public Component initRiskJsonModelOnComponentWithSync(RiskComponent riskComponent, Component component){

		boolean updateThreatsOnComponent = false;

		logger.info("Start with init or update component by json infos (sync logic)");
		logger.info("Component is already present in DB, try to remove all threats by component.");

		//Se il componente è in db e deve essere aggiornato occorre cancellare i vecchi dati tranne gli sla id
		//in realtà forse occorre cancellare tutto, anche gli sla id perchè non sono più validi
		component.setName(riskComponent.getText());
		component.setDescription(riskComponent.getCid());
		componentThreatRepository.deleteComponentThreatsByComponent(component);
		component.removeAllComponentThreats();
		updateThreatsOnComponent = true;

		if(updateThreatsOnComponent){
			boolean threatsFound = false;
			if(riskComponent.getRisks() != null){
				logger.info("Risks found");

				List<ComponentControl> componentControlsPresent = componentControlRepository.findComponentControlByComponent(component);
				for(ComponentControl componentControl : componentControlsPresent){
					componentControl.setConfirmed(false);
					componentControlRepository.save(componentControl);
				}

				for (RiskComponent.RiskThreat riskThreat : riskComponent.getRisks()){
					logger.info("Analyzing Threat ID: "+riskThreat.getId());
					logger.info("Analyzing Threat name: "+riskThreat.getName());
					Threat threat = threatRepository.findByThreatId(riskThreat.getThreatId());

					if(threat != null){
						threatsFound = true;
						logger.info("Threat found in list of threat in db. Proceed with analysis");
						ComponentThreat componentThreat = new ComponentThreat();
						componentThreat.setThreat(threat);
						componentThreat.setAwareness(threat.getAwareness());
						componentThreat.setEaseOfDiscovery(threat.getEaseOfDiscovery());
						componentThreat.setEaseOfExploit(threat.getEaseOfExploit());
						componentThreat.setIntrusionDetection(threat.getIntrusionDetection());
						componentThreat.setLossOfAccountability(threat.getLossOfAccountability());
						componentThreat.setLossOfAvailability(threat.getLossOfAvailability());
						componentThreat.setLossOfConfidentiality(threat.getLossOfConfidentiality());
						componentThreat.setLossOfIntegrity(threat.getLossOfIntegrity());
						componentThreat.setMotive(threat.getMotive());
						componentThreat.setOpportunity(threat.getOpportunity());
						componentThreat.setSize(threat.getSize());
						componentThreat.setSkillLevel(threat.getSkillLevel());

						if(riskThreat.getsR() != null){
							for(RiskComponent.RiskControl riskControl : riskThreat.getsR()){
								logger.info("Risk Control: "+riskControl);
								if(riskControl != null){
									logger.info("Risk Control id: "+riskControl.getId());
									Control control = controlRepository.findByControlId(riskControl.getId());

									if(control != null){
										List<ComponentControl> componentControls = null;
										logger.info("Check if control "+control.getId()+" is already present in component: "+component.getId());
										componentControls = componentControlRepository.findComponentControlByComponentAndControl(component, control);
										if(componentControls != null && componentControls.size() > 0){
											for(ComponentControl componentControl : componentControls){
												logger.info("The control is already present: "+componentControl.getConfirmed());
												componentControl.setConfirmed(true);
												componentControlRepository.save(componentControl);
											}

										}else{
											ComponentControl componentControl = new ComponentControl();
											componentControl.setConfirmed(true);
											componentControl.setControl(control);									

											Set<QuestionControl> questionControls = control.getQuestionControl();

											Iterator<QuestionControl> iterator = questionControls.iterator();

											while(iterator.hasNext()){
												QuestionControl questionControl = iterator.next();
												ComponentControlQuestion componentThreatControlQuestion = new ComponentControlQuestion();
												componentThreatControlQuestion.setAnswer(null);
												componentThreatControlQuestion.setQuestionControl(questionControl);
												componentControl.addControlQuestion(componentThreatControlQuestion);
											}
											component.addComponentControl(componentControl);
										}
										
										
										
									}else{
										logger.error("Control in json not found! "+riskControl.getId());
									}
								}
							}
							
							
							
							/*
							//TODO: remove of controls not present in riskproflie
							List<ComponentControl> componentControlsAlreadyPresent = componentControlRepository.findComponentControlByComponent(component);
							for(ComponentControl controlToCheck : componentControlsAlreadyPresent){
								boolean controlIsStillPresent = false;
								for(RiskComponent.RiskControl riskControl : riskThreat.getsR()){
									
								}
							}*/
							
						}else{
							logger.error("The threat "+riskThreat.getThreatId()+" has not controls");
						}

						component.addComponentThreat(componentThreat);
					}else{
						logger.error("Threat not found in list of threat in db. The adding is not possible.");
					}
				}
				List<ComponentControl> componentControls = componentControlRepository.findComponentControlByComponent(component);
				for(ComponentControl componentControl : componentControls){
					if(!componentControl.getConfirmed()){
						componentControlRepository.delete(componentControl);
					}
				}					
			}else{
				logger.error("The component has not threats");
			}

			logger.info("ComponentThreats added to component: "+component.getComponentThreats().size());

			component.setSlatSync(false);
		}

		return component;
	}

	public ComponentState getStateOfComponentFromSession(String appId, String compId, String jwtToken, String userId){
		Component myComponent = componentRepository.findByApplicationKanbanIdAndKanbanId(appId, compId);
		String appSession = restClient.getAppSessionFromKanban(appId, jwtToken, userId);
		if(appSession != null){
			JSONObject obj = new JSONObject(appSession);
			JSONArray columns = obj.getJSONArray("columns");
			System.out.println("Number of columns found: "+columns.length());
			for (int i = 0; i < columns.length(); i++){
				JSONArray items = columns.getJSONObject(i).getJSONArray("items");
				System.out.println("Number of items found: "+items.length());
				for (int j = 0; j < items.length(); j++){
					try{
						String componentId = items.getJSONObject(j).getString("cid");
						System.out.println("Component id to check: "+compId);
						System.out.println("Component id in session: "+componentId);

						if(componentId.equals(compId)){
							String componentState = items.getJSONObject(j).getString("status");
							if(componentState != null){
								System.out.println("Component state found for component "+items.getJSONObject(j).getString("cid")+": "+componentState);
								if(componentState.equals("CREATED")){
									return ComponentState.CREATED;
								}else if(componentState.equals("PROFILED")){
									return ComponentState.PROFILED;
								}else if(componentState.equals("SLAT_GENERATED")){
									return ComponentState.SLAT_GENERATED;
								}else if(componentState.equals("SLAT_ASSESSED")){
									return ComponentState.SLAT_ASSESSED;
								}else if(componentState.equals("SLA_GENERATED")){
									return ComponentState.SLA_GENERATED;
								}
							}else{
								if(myComponent != null){
									return myComponent.getState();
								}
								return ComponentState.CREATED;
							}
						}
					}catch(JSONException e){
						e.printStackTrace();
					}
				}
			}
		}
		if(myComponent != null){
			return myComponent.getState();
		}
		return ComponentState.CREATED;
	}
}
