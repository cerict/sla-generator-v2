package eu.musaproject.slagenerator.entity;

public class JsonRiskAssessmentResult {

	private String family_id;

	public JsonRiskAssessmentResult(){}
	
	public JsonRiskAssessmentResult(String family_id){
		this.family_id = family_id;
	}
	
	public String getFamily_id() {
		return family_id;
	}

	public void setFamily_id(String family_id) {
		this.family_id = family_id;
	}
	
}
