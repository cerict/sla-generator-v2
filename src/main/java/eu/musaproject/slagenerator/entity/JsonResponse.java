package eu.musaproject.slagenerator.entity;

public class JsonResponse {

	private Status status;
	private String message;
	private Object data;

	public JsonResponse(){

	}

	public JsonResponse(Status status, String message, Object data){
		setStatus(status);
		setMessage(message);
		setData(data);
	}

	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
}
