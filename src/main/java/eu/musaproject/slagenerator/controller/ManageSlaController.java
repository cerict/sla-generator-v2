package eu.musaproject.slagenerator.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import eu.musaproject.slagenerator.entity.JsonResponse;
import eu.musaproject.slagenerator.entity.Status;
import eu.musaproject.slagenerator.repositories.ComponentRepository;

@RestController
@RequestMapping("/sla-generator/api/manageSla")
public class ManageSlaController {

	public static final Logger logger = LoggerFactory.getLogger(RestApiRiskJsonController.class);

	@Autowired ComponentRepository componentRepository;

	@Value("${component.sla.manager}")
	private String slaManagerPath;

	@RequestMapping(path = "/getAllSla", method = RequestMethod.GET)
	public ResponseEntity<?> getAllSla() {

		RestTemplate restPutSla = new RestTemplate();

		ResponseEntity<String> response = restPutSla.exchange(slaManagerPath, HttpMethod.GET, null,
				String.class);
		return response;
	}

	@RequestMapping(path = "/getSlaContent", method = RequestMethod.GET)
	public ResponseEntity<JsonResponse> getSlaContent(@RequestParam(value = "slaId") String slaId) {

		RestTemplate restPutSla = new RestTemplate();
		logger.warn("Sla path: "+slaManagerPath+"/"+slaId);
		ResponseEntity<String> responseSlaManager = restPutSla.exchange(slaManagerPath+"/"+slaId, HttpMethod.GET, null,
				String.class);
		JsonResponse response = new JsonResponse(Status.OK, "Sla generated successfully", responseSlaManager.getBody());

		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

}