package eu.musaproject.slagenerator.controller;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import eu.musaproject.enumeration.ComponentState;
import eu.musaproject.model.Component;
import eu.musaproject.model.ComponentControl;
import eu.musaproject.model.ComponentThreat;
import eu.musaproject.model.ComponentType;
import eu.musaproject.model.Control;
import eu.musaproject.model.QuestionThreat;
import eu.musaproject.model.Stride;
import eu.musaproject.model.Threat;
import eu.musaproject.slagenerator.entity.JsonResponse;
import eu.musaproject.slagenerator.entity.Status;

import eu.musaproject.slagenerator.repositories.ComponentControlMetricRepository;
import eu.musaproject.slagenerator.repositories.ComponentControlQuestionRepository;
import eu.musaproject.slagenerator.repositories.ComponentControlRepository;
import eu.musaproject.slagenerator.repositories.ComponentRepository;
import eu.musaproject.slagenerator.repositories.ComponentThreatRepository;
import eu.musaproject.slagenerator.repositories.ControlRepository;
import eu.musaproject.slagenerator.repositories.ThreatRepository;
import eu.musaproject.slagenerator.service.RestClient;

@RestController
@RequestMapping("/sla-generator/api/riskAnalysis")
public class RiskAnalysisController {

	public static final Logger logger = LoggerFactory.getLogger(RiskAnalysisController.class);

	@Autowired ThreatRepository threatRepository;


	@Autowired ComponentRepository componentRepository;
	@Autowired ComponentControlMetricRepository componentControlMetricRepository;
	@Autowired ComponentControlQuestionRepository componentControlQuestionRepository;
	@Autowired RestClient restClient;
	@Autowired ComponentThreatRepository componentThreatRepository;
	@Autowired ComponentControlRepository componentControlRepository;
	@Autowired ControlRepository controlRepository;

	@RequestMapping(path="/componentQuestions", method = RequestMethod.GET)
	public ResponseEntity<?> getComponentQuestions(@RequestParam(value = "componentId") Long componentId, UriComponentsBuilder ucBuilder) {

		System.out.println("Get Questions List called: "+componentId);

		Component component = componentRepository.findOne(componentId);

		//		if(component.getState() != ComponentState.CREATED){
		//Store the metrics for the component
		ComponentType compType = component.getComponentType();
		Set<ComponentType> types = new HashSet<ComponentType>();
		types.add(compType);
		List<Threat> threats = threatRepository.findByComponentTypes(types);
		List<QuestionThreat> questions = new ArrayList<QuestionThreat>();
		for(Threat threat : threats){
			for(QuestionThreat questionThreat : threat.getQuestions()){
				if(!questions.contains(questionThreat)){
					questions.add(questionThreat);
				}
			}
		}

		return new ResponseEntity<List<QuestionThreat>>(questions, HttpStatus.OK);

		//		}else{
		//			return new ResponseEntity<JsonResponse>(new JsonResponse(Status.KO, "The component is in CREATED state. No risk profile associated to the component found!", null), HttpStatus.BAD_REQUEST);
		//		}
	}

	@RequestMapping(path="/submitQuestions", method = RequestMethod.POST)
	public ResponseEntity<?> submitQuestions(@RequestParam(value = "compId") Long compId, @RequestParam(value = "threatQuestions") String threatQuestions, UriComponentsBuilder ucBuilder) {

		logger.info("submitQuestions called: "+threatQuestions);
		Type listType = new TypeToken<ArrayList<QuestionThreat>>(){}.getType();

		List<QuestionThreat> questionThreats = new Gson().fromJson(threatQuestions, listType);

		List<Threat> threats = new ArrayList<Threat>();
		for(QuestionThreat questionThreat : questionThreats){
			if(questionThreat.getSelected()){	
				Set<QuestionThreat> questions = new HashSet<QuestionThreat>();
				questions.add(questionThreat);
				List<Threat> threatsQ = threatRepository.findByQuestions(questions);
				for(Threat threat : threatsQ){
					if(!threats.contains(threat)){
						threats.add(threat);
					}
				}
			}
		}

		Component component = componentRepository.findOne(compId);
		componentThreatRepository.deleteComponentThreatsByComponent(component);

		List<ComponentThreat> componentThreats = new ArrayList<ComponentThreat>();
		for(Threat threat : threats){
			ComponentThreat componentThreat = new ComponentThreat(threat);
			componentThreats.add(componentThreat);
			component.addComponentThreat(componentThreat);
		}
		componentRepository.save(component);

		return new ResponseEntity<List<ComponentThreat>>(componentThreats, HttpStatus.OK);
	}

	@RequestMapping(path="/saveEvaluation", method = RequestMethod.POST)
	public ResponseEntity<?> saveEvaluation(@RequestParam(value = "compId") Long compId, 
			@RequestParam(value = "componentThreats") String componentThreatsString, 
			@RequestParam(value = "spoofingRisk") String spoofingRisk,
			@RequestParam(value = "tamperingRisk") String tamperingRisk,
			@RequestParam(value = "repudiationRisk") String repudiationRisk,
			@RequestParam(value = "informationDisclosureRisk") String informationDisclosureRisk,
			@RequestParam(value = "denialOfServiceRisk") String denialOfServiceRisk,
			@RequestParam(value = "elevationOfPrivilegesRisk") String elevationOfPrivilegesRisk,
			UriComponentsBuilder ucBuilder) {

		logger.info("saveEvaluation called: "+componentThreatsString);
		Type listType = new TypeToken<ArrayList<ComponentThreat>>(){}.getType();

		List<ComponentThreat> componentThreats = new Gson().fromJson(componentThreatsString, listType);

		List<Control> controls = new ArrayList<Control>();
		List<String> controlsId = new ArrayList<String>();

		Component component = componentRepository.findOne(compId);
		componentThreatRepository.deleteComponentThreatsByComponent(component);
		for(ComponentThreat componentThreat : componentThreats){
			component.addComponentThreat(componentThreat);
			for(Control control : componentThreat.getThreat().getSuggestedControls()){
				String controlRisk = control.getRisk();
				
				String riskToConsider = "LOW";
				for(Stride stride : componentThreat.getThreat().getStrides()){
					if(stride.getName().equals("SPOOFING")){
						if(spoofingRisk.equals("MEDIUM")){
							riskToConsider = "MOD";
						}else if(spoofingRisk.equals("HIGH") || spoofingRisk.equals("CRITICAL")){
							riskToConsider = "HIGH";
						}
					}else if(stride.getName().equals("INFORMATION DISCLOSURE")){
						if(informationDisclosureRisk.equals("MEDIUM")){
							riskToConsider = "MOD";
						}else if(informationDisclosureRisk.equals("HIGH") || informationDisclosureRisk.equals("CRITICAL")){
							riskToConsider = "HIGH";
						}
					}else if(stride.getName().equals("ELEVATION OF PRIVILEGES")){
						if(elevationOfPrivilegesRisk.equals("MEDIUM")){
							riskToConsider = "MOD";
						}else if(elevationOfPrivilegesRisk.equals("HIGH") || elevationOfPrivilegesRisk.equals("CRITICAL")){
							riskToConsider = "HIGH";
						}
					}else if(stride.getName().equals("DENIAL OF SERVICE")){
						if(denialOfServiceRisk.equals("MEDIUM")){
							riskToConsider = "MOD";
						}else if(denialOfServiceRisk.equals("HIGH") || denialOfServiceRisk.equals("CRITICAL")){
							riskToConsider = "HIGH";
						}
					}else if(stride.getName().equals("TAMPERING")){
						if(tamperingRisk.equals("MEDIUM")){
							riskToConsider = "MOD";
						}else if(tamperingRisk.equals("HIGH") || tamperingRisk.equals("CRITICAL")){
							riskToConsider = "HIGH";
						}
					}else if(stride.getName().equals("REPUDIATION")){
						if(repudiationRisk.equals("MEDIUM")){
							riskToConsider = "MOD";
						}else if(repudiationRisk.equals("HIGH") || repudiationRisk.equals("CRITICAL")){
							riskToConsider = "HIGH";
						}
					}
				}

				if(controlRisk.equals("LOW")){
					if(!controlsId.contains(control.getControlId())){
						control.setSelected(true);
						controls.add(control);
						controlsId.add(control.getControlId());
					}
				}else if(controlRisk.equals("MOD") && (riskToConsider.equals("MOD") || riskToConsider.equals("HIGH"))){
					if(!controlsId.contains(control.getControlId())){
						control.setSelected(true);
						controls.add(control);
						controlsId.add(control.getControlId());
					}
				}else if(controlRisk.equals("HIGH") && riskToConsider.equals("HIGH")){
					if(!controlsId.contains(control.getControlId())){
						control.setSelected(true);
						controls.add(control);
						controlsId.add(control.getControlId());
					}
				}
			}
		}
		componentRepository.save(component);


		return new ResponseEntity<List<Control>>(controls, HttpStatus.OK);
	}

	@RequestMapping(path="/submitControls", method = RequestMethod.POST)
	public ResponseEntity<JsonResponse> submitControls(@RequestParam(value = "compId") Long compId, @RequestParam(value = "controls") String controlsString, @RequestParam(value = "jwtToken") String jwtToken, @RequestParam(value = "userId") String userId, UriComponentsBuilder ucBuilder) {

		logger.info("submitControls called: "+controlsString);
		Type listType = new TypeToken<ArrayList<Control>>(){}.getType();

		List<Control> controls = new Gson().fromJson(controlsString, listType);


		Component component = componentRepository.findOne(compId);
		for(ComponentControl componentCntrol : component.getComponentControls()){
			componentControlMetricRepository.deleteComponentControlMetricsByComponentControl(componentCntrol);
		}
		for(ComponentControl componentCntrol : component.getComponentControls()){
			componentControlQuestionRepository.deleteComponentControlQuestionsByComponentControl(componentCntrol);
		}
		componentControlRepository.deleteComponentControlsByComponent(component);

		List<ComponentControl> componentControls = new ArrayList<ComponentControl>();
		for(Control control : controls){
			if(control.getSelected()){
				Control repoControl = controlRepository.findOne(control.getId());
				ComponentControl componentControl = new ComponentControl(repoControl);
				componentControls.add(componentControl);
				component.addComponentControl(componentControl);
			}
		}
		if(component.getApplication().getKanbanId() != null && !component.getApplication().getKanbanId().equals("")){
			String errorMessage = restClient.updateSession(component.getApplication().getKanbanId(), component.getKanbanId(), jwtToken, userId, "PROFILED", null, null, null);
			if(errorMessage != null){
					return new ResponseEntity<JsonResponse>(new JsonResponse(Status.KO, errorMessage, null), HttpStatus.BAD_REQUEST);
			}
		}
		component.setState(ComponentState.PROFILED);
		componentRepository.save(component);

		return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(Status.OK, "Controls saved successfully", null));
	}

}



