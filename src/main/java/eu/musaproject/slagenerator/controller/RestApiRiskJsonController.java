package eu.musaproject.slagenerator.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.gson.Gson;

import eu.musaproject.enumeration.ApplicationState;
import eu.musaproject.enumeration.ComponentState;
import eu.musaproject.model.Application;
import eu.musaproject.model.Component;
import eu.musaproject.slagenerator.entity.JsonResponse;
import eu.musaproject.slagenerator.entity.RiskComponent;
import eu.musaproject.slagenerator.entity.Status;
import eu.musaproject.slagenerator.repositories.ApplicationRepository;
import eu.musaproject.slagenerator.repositories.ComponentRepository;
import eu.musaproject.slagenerator.service.RiskJsonHelper;

@RestController
@RequestMapping("/sla-generator/api/riskJson")
public class RestApiRiskJsonController {

	public static final Logger logger = LoggerFactory.getLogger(RestApiRiskJsonController.class);

	@Autowired ApplicationRepository applicationRepository;
	@Autowired ComponentRepository componentRepository;

	@Autowired RiskJsonHelper riskJsonHelper;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<JsonResponse> addRiskJson(@RequestParam(value = "riskJson") String riskJson, UriComponentsBuilder ucBuilder) {

		RiskComponent riskComponent = new Gson().fromJson(riskJson, RiskComponent.class);
		if(riskComponent != null){
			Application application = new Application();
			application.setState(ApplicationState.CREATED);
			Component component = new Component();
			component = riskJsonHelper.initRiskJsonModelOnComponent(riskComponent, component);
			component.setState(ComponentState.PROFILED);
			application.addComponent(component);
			applicationRepository.save(application);
			List<Component> components = componentRepository.findByApplicationId(application.getId());
			if(components.size() > 0){
				Long[] returns = new Long[2];
				returns[0] = application.getId();
				returns[1] = components.get(0).getId();
				return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(Status.OK, "Component created successfully", returns));
			}else{
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new JsonResponse(Status.KO, "Error processing", null));
			}
		}else{
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new JsonResponse(Status.KO, "JSON format not recognized", null));

		}

	}

	@RequestMapping(method = RequestMethod.POST, path = "/fromAssessment")
	public ResponseEntity<?> addRiskJsonFromAssessemntResult(@RequestParam(value = "assessmentJson") String assessmentJson, @RequestParam(value = "componentId") Long componentId, UriComponentsBuilder ucBuilder) {

		Long idComponent = riskJsonHelper.initRiskJsonModelFromAssessment(componentId, assessmentJson);

		return ResponseEntity.status(HttpStatus.OK).body(idComponent);
	}

}



