package eu.musaproject.slagenerator.controller;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import eu.musaproject.enumeration.ComponentState;
import eu.musaproject.model.Component;
import eu.musaproject.model.ComponentControl;
import eu.musaproject.model.ComponentControlMetric;
import eu.musaproject.model.ComponentControlQuestion;
import eu.musaproject.model.Control;
import eu.musaproject.model.Metric;
import eu.musaproject.slagenerator.entity.JsonResponse;
import eu.musaproject.slagenerator.entity.Status;
import eu.musaproject.slagenerator.repositories.ComponentControlMetricRepository;
import eu.musaproject.slagenerator.repositories.ComponentControlQuestionRepository;
import eu.musaproject.slagenerator.repositories.ComponentControlRepository;
import eu.musaproject.slagenerator.repositories.ComponentRepository;
import eu.musaproject.slagenerator.repositories.ControlRepository;



@RestController
@RequestMapping("/sla-generator/api/assessmentnew")
public class AssessmentController {

	public static final Logger logger = LoggerFactory.getLogger(AssessmentController.class);

	@Autowired ComponentRepository componentRepository;
	@Autowired ComponentControlQuestionRepository componentControlQuestionRepository;
	@Autowired ControlRepository controlRepository;
	@Autowired ComponentControlRepository componentControlRepository;
	@Autowired ComponentControlMetricRepository componentControlMetricRepository;

	@RequestMapping(path = "submitControlFamilies", method = RequestMethod.POST)
	public ResponseEntity<?> submitControlFamilies(@RequestParam(value = "componentId") Long componentId, 
			@RequestParam(value = "controlFamilies") String controlFamiliesList, UriComponentsBuilder ucBuilder) {
		Type listType = new TypeToken<ArrayList<String>>(){}.getType();
		List<String> controlFamilies = new Gson().fromJson(controlFamiliesList, listType);

		logger.info("Control families received: "+controlFamilies.size());

		Component component = componentRepository.getOne(componentId);
		for(ComponentControl componentControl : component.getComponentControls()){
			componentControlQuestionRepository.deleteComponentControlQuestionsByComponentControl(componentControl);
			componentControlMetricRepository.deleteComponentControlMetricsByComponentControl(componentControl);
		}
		componentControlRepository.deleteComponentControlsByComponent(component);		
		component.removeAllComponentControls();
		
		logger.info("Old Component controls removed");

		for(String controlFamily : controlFamilies){
			List<Control> controls = controlRepository.findByFamilyName(controlFamily);
			for(Control control : controls){
				ComponentControl compControl = new ComponentControl(control);
				logger.info("Number of metrics found for control "+control.getControlName()+": "+control.getMetrics().size());
				for(Metric metric : control.getMetrics()){
					ComponentControlMetric compContrMetric = new ComponentControlMetric(metric, compControl);
					compContrMetric.setSelected(true);
					compControl.addThreatControlMetric(compContrMetric);
				}

				component.addComponentControl(compControl);
			}
		}
		componentRepository.save(component);
		return new ResponseEntity<Object>(component.getComponentControls(), HttpStatus.OK);
	}

	@RequestMapping(path = "getControlFamilies", method = RequestMethod.GET)
	public ResponseEntity<?> getControlFamilies(UriComponentsBuilder ucBuilder) {
		List<String> controlFamilies = controlRepository.findControlFamilies();
		return new ResponseEntity<Object>(controlFamilies, HttpStatus.OK);
	}


	@RequestMapping(path = "getComponentQuestions", method = RequestMethod.GET)
	public ResponseEntity<?> getComponentQuestions(@RequestParam(value = "componentId") Long componentId, UriComponentsBuilder ucBuilder) {

		logger.info("Component id: "+componentId);

		Component component = componentRepository.getOne(componentId);
		logger.info("component: "+component);
		logger.info("component state: "+component.getState());
		if(component != null && !component.equals("null") && !component.getState().equals(ComponentState.CREATED)
				|| (component.getState().equals(ComponentState.CREATED) && component.getComponentControls().size() > 0)){
			logger.info("Component is not null");
			logger.info("Number of controls to fill questions: "+component.getComponentControls().size());
			return new ResponseEntity<Object>(component.getComponentControls(), HttpStatus.OK);
		}else{
			logger.info("err component: "+component);
			if(component != null){
				logger.info("err component state: "+component.getState());
			}
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new JsonResponse(Status.KO, "Component not found or state is CREATED!", null));
		}
	}

	@RequestMapping(path = "submitQuestionnaire", method = RequestMethod.POST)
	public ResponseEntity<?> submitQuestionnaire(@RequestParam(value = "activeComponentQuestions") String activeComponentQuestionsJson, UriComponentsBuilder ucBuilder) {
		try {
			System.out.println("Active Component Questions JSON: "+activeComponentQuestionsJson);
			JSONArray questions = new JSONArray(activeComponentQuestionsJson);

			for(int i = 0; i < questions.length(); i++){
				JSONArray controlQuestions = questions.getJSONObject(i).getJSONArray("controlQuestions");

				for(int z = 0; z < controlQuestions.length(); z++){
					//						System.out.println("Element: "+controlQuestions.getJSONObject(z));
					Long questionId = controlQuestions.getJSONObject(z).getLong("id");
					String questionAnswer = "";
					if(!controlQuestions.getJSONObject(z).isNull("answer")){
						questionAnswer = controlQuestions.getJSONObject(z).getString("answer");
					}
					ComponentControlQuestion question = componentControlQuestionRepository.findOne(questionId);
					if(question != null){
						question.setAnswer(questionAnswer);
						componentControlQuestionRepository.save(question);
					}
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new JsonResponse(Status.KO, "Questionnaire not submitted!", null));
		}

		return ResponseEntity.status(HttpStatus.OK).body(null);
	}

}
