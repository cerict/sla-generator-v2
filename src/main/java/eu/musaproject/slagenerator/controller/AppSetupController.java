package eu.musaproject.slagenerator.controller;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.gson.Gson;

import eu.musaproject.enumeration.ApplicationState;
import eu.musaproject.enumeration.ComponentState;
import eu.musaproject.model.Application;
import eu.musaproject.model.Component;
import eu.musaproject.slagenerator.entity.JsonResponse;
import eu.musaproject.slagenerator.entity.RiskComponent;
import eu.musaproject.slagenerator.entity.Status;
import eu.musaproject.slagenerator.repositories.ApplicationRepository;
import eu.musaproject.slagenerator.repositories.ComponentRepository;
import eu.musaproject.slagenerator.repositories.ComponentTypeRepository;
import eu.musaproject.slagenerator.service.AppSetupHelper;
import eu.musaproject.slagenerator.service.RestClient;

@RestController
@RequestMapping("/sla-generator/api/appSetup")
public class AppSetupController {

	public static final Logger logger = LoggerFactory.getLogger(AppSetupController.class);

	@Autowired ApplicationRepository applicationRepository;
	@Autowired ComponentRepository componentRepository;
	@Autowired ComponentTypeRepository componentTypeRepository;
	@Autowired AppSetupHelper appSetupHelper;

	@Autowired RestClient restClient;

	@Value("${component.sla.manager}")
	private String slaManagerPath;

	@RequestMapping(path = "/importAppFromMACM", method = RequestMethod.GET)
	public ResponseEntity<JsonResponse> importAppFromMACM(@RequestParam(value = "appId") String appId, UriComponentsBuilder ucBuilder) {
		Application application = appSetupHelper.setupApplicationFromMacmByAppId(appId);
		if(application != null){
			return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(Status.OK, "Application created successfully", application));
		}else{
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new JsonResponse(Status.KO, appSetupHelper.getErrorMessage(), null));
		}
	}

	@RequestMapping(path = "/setupAppFromMACM", method = RequestMethod.POST)
	public ResponseEntity<JsonResponse> setupAppFromMACM(@RequestParam(value = "appMacm") String appMacm, @RequestParam(value = "appMacmId") String appMacmId, UriComponentsBuilder ucBuilder) {
		logger.info("Macm App String: "+appMacm);
		logger.info("Macm App Id: "+appMacmId);

		Application application = appSetupHelper.setupApplicationFromMacmStringAndId(appMacm, appMacmId);
		if(application != null){
			return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(Status.OK, "Application created successfully", application));
		}else{
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new JsonResponse(Status.KO, appSetupHelper.getErrorMessage(), null));
		}
	}

	@RequestMapping(path = "/applications/new", method = RequestMethod.POST)
	public ResponseEntity<JsonResponse> createNewApp(@RequestParam(value = "appName") String appName, UriComponentsBuilder ucBuilder) {
		Application application = new Application();
		application.setName(appName);
		application.setKanbanId(null);
		application.setMacmId(null);
		application.setState(ApplicationState.CREATED);

		if(applicationRepository.save(application) != null){
			return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(Status.OK, "Application created successfully", application));
		}else{
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new JsonResponse(Status.KO, "Error saving new app", null));
		}

	}


	@RequestMapping(path = "/applications", method = RequestMethod.GET)
	public ResponseEntity<JsonResponse> getApplications(@RequestParam(value = "appId", required = false) Long appId, UriComponentsBuilder ucBuilder) {
		if(appId == null){
			List<Application> applications = applicationRepository.findAll();
			
			return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(Status.OK, "Applications get successfully", applications));
		}else{
			Application application = applicationRepository.findOne(appId);
			if(application != null){
				return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(Status.OK, "Application get successfully", application));
			}else{
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new JsonResponse(Status.OK, "Application not found!", null));

			}
		}
	}

	@RequestMapping(path = "/applications/state", method = RequestMethod.GET)
	public ResponseEntity<JsonResponse> getApplicationsState(@RequestParam(value="appId") Long appId, UriComponentsBuilder ucBuilder) {
		logger.info("Get state for application with id: "+appId);
		Application application = applicationRepository.findOne(appId);
		return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(Status.OK, "Application get successfully", application.getState()));
	}

	@RequestMapping(path = "/applications/{appId}/component/new", method = RequestMethod.POST)
	public ResponseEntity<JsonResponse> createNewComponent(@PathVariable(value = "appId") Long appId, @RequestParam(value = "compName") String compName, @RequestParam(value = "compType") String compType, UriComponentsBuilder ucBuilder) {
		Application application = applicationRepository.findOne(appId);

		System.out.println("Request to create new component in app");
		Component component = new Component();
		component.setApplication(application);
		component.setName(compName);
		component.setComponentType(componentTypeRepository.findByName(compType));
		component.setState(ComponentState.CREATED);
		component.setComponentGroup("saas");

		application.addComponent(component);

		if(applicationRepository.save(application) != null){
			return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(Status.OK, "Component added successfully", application));
		}else{
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new JsonResponse(Status.KO, "Error saving new component", null));
		}

	}

	//Api utile a settare l'app dalla kanban
	@RequestMapping(path = "/setuppAppFromKanban", method = RequestMethod.POST)
	public ResponseEntity<JsonResponse> setuppAppFromKanban(@RequestParam(value = "appId") String appId, @RequestParam(value = "componentId") String componentId,
			@RequestParam(value = "jwtToken") String jwtToken, @RequestParam(value = "userId") String userId, UriComponentsBuilder ucBuilder) {

		logger.info(" App id: "+appId+" - Component id: "+componentId+" - jwt token: "+jwtToken+" - userId: "+userId);

		Application app = appSetupHelper.setupApplicationFromKanban(appId, componentId, jwtToken, userId);

		if(app != null && app.getState() != ApplicationState.ERROR){
			return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(Status.OK, "Component get successfully", app));
		}else{
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new JsonResponse(Status.KO, "Error! "+appSetupHelper.getErrorMessage(), null));
		}
	}

	@RequestMapping(path = "/removeAllApps", method = RequestMethod.GET)
	public ResponseEntity<JsonResponse> removeAllApps(UriComponentsBuilder ucBuilder) {
		applicationRepository.deleteAll();
		return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(Status.OK, "Applications removed successfully", null));
	}

	@RequestMapping(path = "/loadSlaXml", method = RequestMethod.GET)
	public ResponseEntity<?> loadSLAxml(@RequestParam(value = "slaId") Long slaId, UriComponentsBuilder ucBuilder) {
		logger.info("slaId value: "+slaId);

		RestTemplate restPutSla = new RestTemplate();

		HttpHeaders headersGetSla = new HttpHeaders();
		headersGetSla.setContentType(MediaType.TEXT_XML);
		HttpEntity entityGetSla = new HttpEntity(null, headersGetSla);
		ResponseEntity<String> response = restPutSla.exchange(slaManagerPath, HttpMethod.GET, entityGetSla,
				String.class);
		if(response.getStatusCode() == HttpStatus.OK){
			logger.info("Response from sla manager: "+response.getBody());
		}
		return response;
	}

	@RequestMapping(path = "/setRequiredSLA", method = RequestMethod.POST)
	public ResponseEntity<?> setRequiredSLA(@RequestParam(value = "componentId") Long componentId, @RequestParam(value = "slaId") String slaId, 
			@RequestParam(value = "jwtToken") String jwtToken, @RequestParam(value = "userId") String userId, UriComponentsBuilder ucBuilder) {
		logger.info("slaId value: "+slaId);

		ResponseEntity<String> responseSlaManager = restClient.getSlaFromRepo(slaId);
		if(responseSlaManager.getStatusCode() == HttpStatus.OK){

			Component component = componentRepository.findOne(componentId);
			if(component != null){
				ResponseEntity<String> response = restClient.putSlaToRepo("GENERATED", component.getSlaId(), component.getAssessedSlaId(), component.getComosedSlaId(), responseSlaManager.getBody(), component.getApplication().getId().toString(), component.getApplication().getMacmId(), component.getApplication().getKanbanId(), component.getId().toString(), component.getKanbanId());
				if(response.getStatusCode() == HttpStatus.CREATED  || response.getStatusCode() == HttpStatus.OK){

					component.setSlaId(response.getBody());
					component.setState(ComponentState.SLAT_GENERATED);
					componentRepository.save(component);
					String updateSessionMessage = null;
					if(component.getApplication().getKanbanId() != null && !component.getApplication().getKanbanId().equals("")){
						updateSessionMessage = restClient.updateSession(component.getApplication().getKanbanId(), component.getKanbanId(), jwtToken, userId, "SLAT_GENERATED", response.getBody(), null, null);
					}

					Application application = applicationRepository.findOne(component.getApplication().getId());
					Set<Component> components = application.getComponents();
					boolean isSLATsGENERATED = false;
					for(Component comp : components){
						if(!comp.getId().equals(componentId) && 
								(comp.getState().equals(ComponentState.SLAT_GENERATED) ||
										comp.getState().equals(ComponentState.SLAT_ASSESSED) ||
										comp.getState().equals(ComponentState.SLA_GENERATED))){
							isSLATsGENERATED = true;
							if(comp.getState().equals(ComponentState.SLA_GENERATED)){
								logger.info("[Set Assessed SLA] Component is in Generated state! Proceed with removing SLA");
								comp.setSlaId(null);
								if(comp.getAssessedSlaId() != null && !comp.getAssessedSlaId().equals("")){
									comp.setState(ComponentState.SLAT_ASSESSED);
								}else{
									comp.setState(ComponentState.SLAT_GENERATED);
								}
								componentRepository.save(comp);
							}
						}else if(comp.getId().equals(componentId)){
							isSLATsGENERATED = true;
						}else{
							isSLATsGENERATED = false;
							break;
						}
					}
					if(isSLATsGENERATED){
						application.setState(ApplicationState.SLATs_GENERATED);
					}
					applicationRepository.save(application);
					if(updateSessionMessage != null){
						return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(Status.OK, "Sla set successfully but there is an error in session update; "+updateSessionMessage, component));
					}else{
						return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(Status.OK, "Sla set successfully", component));
					}
				}else{
					return response;
				}
			}else{
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new JsonResponse(Status.KO, "Error! Component not found!", null));
			}
		}else{
			return responseSlaManager;
		}
	}

	@RequestMapping(path = "/setAssessedSLA", method = RequestMethod.POST)
	public ResponseEntity<?> setAssessedSLA(@RequestParam(value = "componentId") Long componentId, @RequestParam(value = "slaId") String slaId, 
			@RequestParam(value = "jwtToken") String jwtToken, @RequestParam(value = "userId") String userId, UriComponentsBuilder ucBuilder) {
		logger.info("slaId value: "+slaId);

		ResponseEntity<String> responseSlaManager = restClient.getSlaFromRepo(slaId);
		if(responseSlaManager.getStatusCode() == HttpStatus.OK){

			Component component = componentRepository.findOne(componentId);

			if(component != null){
				ResponseEntity<String> response = restClient.putSlaToRepo("ASSESSED", component.getSlaId(), component.getAssessedSlaId(), component.getComosedSlaId(), responseSlaManager.getBody(), component.getApplication().getId().toString(), component.getApplication().getMacmId(), component.getApplication().getKanbanId(), component.getId().toString(), component.getKanbanId());
				if(response.getStatusCode() == HttpStatus.CREATED  || response.getStatusCode() == HttpStatus.OK){
					component.setAssessedSlaId(response.getBody());
					if(component.getSlaId() == null){
						component.setSlaId(response.getBody());
					}
					component.setState(ComponentState.SLAT_ASSESSED);
					componentRepository.save(component);

					String updateSessionMessage = null;
					if(component.getApplication().getKanbanId() != null && !component.getApplication().getKanbanId().equals("")){
						updateSessionMessage = restClient.updateSession(component.getApplication().getKanbanId(), component.getKanbanId(), jwtToken, userId, "SLAT_ASSESSED", null, response.getBody(), null);
					}

					Application application = applicationRepository.findOne(component.getApplication().getId());
					Set<Component> components = application.getComponents();
					boolean isSLATsASSESSED = true;
					for(Component comp : components){
						if(!comp.getId().equals(componentId) && 
								(comp.getState().equals(ComponentState.SLAT_ASSESSED) ||
										comp.getState().equals(ComponentState.SLA_GENERATED))){
							logger.info("[Set Assessed SLA] Component in Assessed or Generated found!");
							isSLATsASSESSED = true;
							if(comp.getState().equals(ComponentState.SLA_GENERATED)){
								logger.info("[Set Assessed SLA] Component is in Generated state! Proceed with removing SLA");
								comp.setSlaId(null);
								if(comp.getAssessedSlaId() != null && !comp.getAssessedSlaId().equals("")){
									comp.setState(ComponentState.SLAT_ASSESSED);
								}else{
									comp.setState(ComponentState.SLAT_GENERATED);
								}
								componentRepository.save(comp);
							}
						}else if(comp.getId().equals(componentId)){
							isSLATsASSESSED = true;
						}else{
							isSLATsASSESSED = false;
							break;
						}	
					}
					if(isSLATsASSESSED){
						application.setState(ApplicationState.SLATs_ASSESSED);
					}
					applicationRepository.save(application);
					components = application.getComponents();

					if(updateSessionMessage != null){
						return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(Status.OK, "Sla set successfully but there is an error in session update: "+updateSessionMessage, components));
					}else{
						return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(Status.OK, "Sla set successfully", components));
					}
				}else{
					return response;
				}
			}else{
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new JsonResponse(Status.KO, "Error! Component not found!", null));
			}
		}else{
			return responseSlaManager;
		}
	}


}



