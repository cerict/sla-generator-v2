package eu.musaproject.slagenerator.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import eu.musaproject.enumeration.ApplicationState;
import eu.musaproject.enumeration.ComponentState;
import eu.musaproject.model.Application;
import eu.musaproject.model.Component;
import eu.musaproject.multicloud.converters.macm2wsag;
import eu.musaproject.multicloud.converters.wsag2neo;
import eu.musaproject.multicloud.engines.composition.SLAcomposer;
import eu.musaproject.multicloud.engines.composition.SLAreasoner;
import eu.musaproject.multicloud.models.graphs.Peer;
import eu.musaproject.multicloud.models.graphs.Relationship;
import eu.musaproject.multicloud.models.macm.MACM;
import eu.musaproject.multicloud.models.macm.macmPeerType;
import eu.musaproject.multicloud.models.macm.macmRelationshipType;
import eu.musaproject.multicloud.models.macmSLA.macmSLA;
import eu.musaproject.multicloud.models.macmSLA.macmSLAT;
import eu.musaproject.slagenerator.agreementoffermodel.AgreementOffer;
import eu.musaproject.slagenerator.agreementoffermodel.ServiceDescriptionTerm;
import eu.musaproject.slagenerator.agreementoffermodel.Term;
import eu.musaproject.slagenerator.entity.JsonResponse;
import eu.musaproject.slagenerator.entity.Status;
import eu.musaproject.slagenerator.repositories.ApplicationRepository;
import eu.musaproject.slagenerator.repositories.ComponentRepository;
import eu.musaproject.slagenerator.service.ComposerHelper;
import eu.musaproject.slagenerator.service.RestClient;
import eu.musaproject.slagenerator.service.SlaGeneratorHelper;

@RestController
@RequestMapping("/sla-generator/api/composer")
public class ComposeController extends SLAcomposer{

	boolean composedFinish = false;
	Thread compositionThread;

	public ComposeController() throws SQLException {
		super();
		// TODO Auto-generated constructor stub
	}

	public static final Logger logger = LoggerFactory.getLogger(ComposeController.class);

	@Autowired ApplicationRepository applicationRepository;
	@Autowired ComponentRepository componentRepository;

	@Autowired RestClient restClient;

	@Autowired ComposerHelper composerHelper;
	@Autowired SlaGeneratorHelper slaGeneratorHelper;

	@Value("${component.sla.manager}")
	private String slaManagerPath;

	private Application application;
	private String errorMessage;

	@RequestMapping(path = "/prepareComposition", method = RequestMethod.POST)
	public ResponseEntity<JsonResponse> prepareComposition(@RequestParam(value = "appId") Long appId, @RequestParam(value = "jwtToken") String jwtToken, @RequestParam(value = "userId") String userId, UriComponentsBuilder ucBuilder) throws SQLException {

		application = applicationRepository.findOne(appId);
		mcapp = new MACM();
		logger.info("[composeApplication] Read neo with appId: "+application.getMacmId());
		mcapp.setAppid(application.getMacmId());
		mcapp.readNeo(application.getMacmId());
		slar = new SLAreasoner(mcapp);

		cleanMACMslas(application.getMacmId());
		logger.info("[composeApplication] Read Peer values: ");
		for(Map.Entry<String, Peer> entry : mcapp.getPeers().entrySet()) {
			String key = entry.getKey();
			Peer value = entry.getValue();
//			System.out.println(key + " = " + value.getName());
		}

		for(Component component :application.getComponents()){
			errorMessage = null;
			setSLATinNeo(component.getName());
			if(errorMessage != null){
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new JsonResponse(Status.KO, "App Composition error! "+errorMessage, null));
			}
		}

		List<Peer> cspPeers = mcapp.getPeersByType(macmPeerType.CSP);
		for(Peer cspPeer : cspPeers){
			errorMessage = null;
			setSLAinNeo(cspPeer.getName(), application.getMacmId());
			if(errorMessage != null){
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new JsonResponse(Status.KO, "App Composition error! "+errorMessage, null));
			}
		}

		List<Peer> vmPeers = mcapp.getPeersByType(macmPeerType.IaaS);
		for(Peer vmPeer : vmPeers){
			errorMessage = null;

			Peer cspPeer = getCspPeerForVm(mcapp, vmPeer);
			String cspName = cspPeer != null ? cspPeer.getName().toUpperCase() : "AIMES";

			setSLATForVMinNeo(vmPeer.getName(), vmPeer.getProperty("hardware_core"), vmPeer.getProperty("hardware_ram"), cspName, application.getMacmId());
			if(errorMessage != null){
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new JsonResponse(Status.KO, "App Composition error! "+errorMessage, null));
			}
		}

		try{
			prepare();
		}catch(Exception e){
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new JsonResponse(Status.KO, "Error during the prepare function!", null));
		}

		return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(Status.OK, "Prepare to composition executed successfully", null));

	}

	@RequestMapping(path = "/composeApplication", method = RequestMethod.POST)
	public ResponseEntity<JsonResponse> composeApplication(@RequestParam(value = "appId") Long appId, @RequestParam(value = "jwtToken") String jwtToken, @RequestParam(value = "userId") String userId, UriComponentsBuilder ucBuilder) throws SQLException {
		try{
			compose();
		}catch(Exception e){
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new JsonResponse(Status.KO, "Error during the compose function!", null));
		}

		if(application.getState().equals(ApplicationState.SLATs_GENERATED)){
			application.setState(ApplicationState.SLAs_GENERATED);
		}else if(application.getState().equals(ApplicationState.SLATs_ASSESSED)){
			application.setState(ApplicationState.ASSESSED_SLAs_GENERATED);
		}
		applicationRepository.save(application);

		for(Component component :application.getComponents()){
			if(component.getApplication().getKanbanId() != null && !component.getApplication().getKanbanId().equals("")){
				String errorMessage = restClient.updateSession(component.getApplication().getKanbanId(), component.getKanbanId(), jwtToken, userId, "SLA_GENERATED", null, null, null);
				if(errorMessage != null){
					return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new JsonResponse(Status.KO, errorMessage, null));
				}
			}
			component.setState(ComponentState.SLA_GENERATED);
			componentRepository.save(component);
		}

		return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(Status.OK, "App Composition executed successfully", null));

	}

	@RequestMapping(path = "/retieveComposedSLAS", method = RequestMethod.POST)
	public ResponseEntity<JsonResponse> retieveComposedSLAS(@RequestParam(value = "appId") Long appId, @RequestParam(value = "jwtToken") String jwtToken, @RequestParam(value = "userId") String userId, UriComponentsBuilder ucBuilder) throws SQLException {
		application = applicationRepository.findOne(appId);
		mcapp = new MACM();
		mcapp.setAppid(application.getMacmId());
		mcapp.readNeo(application.getMacmId());
		ArrayList<String> componentSlas = new ArrayList<String>();

		for(Component component :application.getComponents()){
			String slaXml = macm2wsag.macm2wsagString(mcapp, component.getName());
		
			slaXml = addServiceResourceToSla(component.getSlaId(), component.getAssessedSlaId(), slaXml);

			ResponseEntity<String> response = restClient.putSlaToRepo("COMPOSED", component.getSlaId(), component.getAssessedSlaId(), component.getComosedSlaId(), slaXml, component.getApplication().getId().toString(), component.getApplication().getMacmId(), component.getApplication().getKanbanId(), component.getId().toString(), component.getKanbanId());

			if(response.getStatusCode() == HttpStatus.CREATED || response.getStatusCode() == HttpStatus.OK){
				componentSlas.add(slaXml);
				if(component.getApplication().getKanbanId() != null && !component.getApplication().getKanbanId().equals("")){
					String errorMessage = restClient.updateSession(component.getApplication().getKanbanId(), component.getKanbanId(), jwtToken, userId, null, null, null, response.getBody());
					if(errorMessage != null){
						return new ResponseEntity<JsonResponse>(new JsonResponse(Status.KO, errorMessage, null), HttpStatus.BAD_REQUEST);
					}
				}

				component.setComosedSlaId(response.getBody());
				componentRepository.save(component);
			}else{
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new JsonResponse(Status.KO, "Error while store sla to Sla Manager!", null));
			}
		}

		return ResponseEntity.status(HttpStatus.OK).body(new JsonResponse(Status.OK, "Composed SLAS retieved successfully", application.getComponents()));
	}

	private String addServiceResourceToSla(String slatId, String assessedSlatId, String slaXml){
		String slaIdToUse = slatId != null && !slatId.equals("") ? slatId : assessedSlatId;

		logger.info("SLA id to get Service Resources for Composed SLA: "+slaIdToUse);
		ResponseEntity<String> response = restClient.getSlaFromRepo(slaIdToUse);
		if(response.getStatusCode() == HttpStatus.OK){
			logger.info("SLAT in Sla Manager found");
			String slatXml = response.getBody();
			AgreementOffer slat = slaGeneratorHelper.buildOfferFromXml(slatXml);

			ServiceDescriptionTerm slatServiceDescriptionTerm = null; 
			for(Term term : slat.getTerms().getAll().getAll()){
				if(term instanceof ServiceDescriptionTerm){
					slatServiceDescriptionTerm = ((ServiceDescriptionTerm) term);
				}
				break;
			}
			if(slatServiceDescriptionTerm != null){
				logger.info("Service Description Term in SLAT found");
				AgreementOffer composedSla = slaGeneratorHelper.buildOfferFromXml(slaXml);
				for(Term term : composedSla.getTerms().getAll().getAll()){
					if(term instanceof ServiceDescriptionTerm){
						logger.info("Service Description Term in SLA added");
						((ServiceDescriptionTerm) term).getServiceDescription().setServiceResources(slatServiceDescriptionTerm.getServiceDescription().getServiceResources());
					}
					break;
				}
				try {
					return(slaGeneratorHelper.buildXmlFromOffer(composedSla));
				} catch (JAXBException e) {
					e.printStackTrace();
				}
			}else{
				logger.info("Service Description Term in SLAT not found");
			}
		}else{
			logger.info("SLAT in Sla Manager not found");
		}

		return slaXml;
	}

	@Override
	public void setMACMinNeo() {
		//TODO:sostituire metodo from file con metodo from string prelevando 
		//il modello dal link usando l'appId settato nel costruttore
		//http://modeller-dev.musa-project.eu:8080/eu.musa.modeller.ws/webresources/camelfilews/getCamel/39/MACM

		try {
			neo.executeFromFile("src/main/resources/CS3M/webapp.cyber");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void setSLATinNeo(String provider) {
		logger.info("[Set SLAT] set SLAT colled with provider: "+provider);
		Component component = componentRepository.findByApplicationIdAndName(application.getId(), provider);
		if(application != null && component != null){
			String slaId;
			if(application.getState().equals(ApplicationState.SLATs_GENERATED)){
				slaId = component.getSlaId();
			}else if(application.getState().equals(ApplicationState.SLATs_ASSESSED)){
				slaId = component.getAssessedSlaId();
			}else{
				logger.error("[Set SLAT] Application state is not correct!");
				errorMessage = "[Set SLAT] Application state is not correct!";
				return;
			}
			ResponseEntity<String> response = restClient.getSlaFromRepo(slaId);
			if(response.getStatusCode() == HttpStatus.OK){
				String slaXml = response.getBody();
				wsag2neo.wsagGraph(slaXml, "SLAT", provider+"_slat",Integer.valueOf(application.getMacmId()));
				mcapp.addSLAT(provider+"_slat");
				Relationship r = null;
				if(application.getState().equals(ApplicationState.SLATs_GENERATED)){
					//					r=mcapp.addRequires(provider, provider+"_slat");
					r=mcapp.addSupports(provider, provider+"_slat");
				}else if(application.getState().equals(ApplicationState.SLATs_ASSESSED)){
					r=mcapp.addSupports(provider, provider+"_slat");
				}
				mcapp.syncNeoRelationship(r);
				macmSLAT slat=new macmSLAT(provider+"_slat", Integer.valueOf(application.getMacmId()));
				slat.readNeo(provider+"_slat");
				slar.addSLATtoKB(slat);
				component.setSlatSync(true);
				componentRepository.save(component);
			}else{
				logger.error("[Set SLAT] Retieve of sla failed!");
				errorMessage = "[Set SLAT] Retieve of sla failed!";
				return;
			}
		}else{
			logger.error("[Set SLAT] Component not found!");
			errorMessage = "[Set SLAT] Component not found!";
		}		
	}

	public void setSLATForVMinNeo(String vmName, String hardwareCore, String hardwareRam, String cspName, String appMacmId) {
		logger.info("[Set SLAT] set SLAT colled with vm: "+vmName);

		String storedSlaId = restClient.getSlaIdFromRepoByAnnotation("VM",
				"VM_"+hardwareCore+"core_"+hardwareRam+"RAM_"+cspName.toUpperCase());
		ResponseEntity<String> response = restClient.getSlaFromRepo(storedSlaId);
		if(response.getStatusCode() == HttpStatus.OK){
			String slaXml = response.getBody();
			wsag2neo.wsagGraph(slaXml, "SLAT", vmName+"_slat", Integer.valueOf(appMacmId));
			mcapp.addSLAT(vmName+"_slat");
			Relationship r = null;
			r=mcapp.addSupports(vmName, vmName+"_slat");
			mcapp.syncNeoRelationship(r);
			macmSLAT slat=new macmSLAT(vmName+"_slat", Integer.valueOf(appMacmId));
			slat.readNeo(vmName+"_slat");
			slar.addSLATtoKB(slat);
		}else{
			logger.error("[Set SLA] Retieve of sla failed!");
			errorMessage = "[Set SLA] Retieve of sla failed!";
		}
	}

	@Override
	public void setSLAinNeo(String provider) {

	}

	public void setSLAinNeo(String provider, String appMacmId) {

		String storedSlaId = restClient.getSlaIdFromRepoByAnnotation("CSP",
				provider);
		logger.info("CSP sla id: "+storedSlaId);
		if(storedSlaId != null){
			ResponseEntity<String> response = restClient.getSlaFromRepo(storedSlaId);
			if(response.getStatusCode() == HttpStatus.OK){
				String slaXml = response.getBody();
				wsag2neo.wsagGraph(slaXml, "SLA", provider+"_sla", Integer.valueOf(appMacmId));
				mcapp.addSLA(provider+"_sla");
				Relationship r = null;
				r=mcapp.addGrants(provider, provider+"_sla");
				mcapp.syncNeoRelationship(r);
				macmSLA sla=new macmSLA(provider+"_sla", Integer.valueOf(appMacmId));
				sla.readNeo(provider+"_sla");
				slar.addSLAtoKB(sla);
			}else{
				logger.error("[Set SLA] Retieve of sla failed!");
				errorMessage = "[Set SLA] Retieve of sla failed!";
			}
		}else{
			logger.error("[Set SLA] Retieve of sla id for csp failed!");
			errorMessage = "[Set SLA] Retieve of sla id for csp failed!";
		}
	}

	private Peer getCspPeerForVm(MACM appMacm, Peer vmPeer){

		List<Relationship> relationships = appMacm.getRelationship();
		for (Relationship relationship:relationships){
			if(relationship.type.equals(macmRelationshipType.provides)){
				logger.info("Relationship provides found!");
				logger.info("Start node is null: "+(relationship.startNode != null ? "NO" : "YES"));
				logger.info("End node is null: "+(relationship.endNode != null ? "NO" : "YES"));

				if(relationship.startNode != null){
					logger.info("Start node: Name-"+relationship.startNode.getName()+" Type-"+relationship.startNode.getType());
				}

				if(relationship.endNode != null){
					logger.info("End node: Name-"+relationship.endNode.getName()+" Type-"+relationship.endNode.getType());
				}

				if(relationship.startNode != null && relationship.endNode != null){
					Peer otherPeer = null;
					otherPeer = (relationship.startNode.getName().equals(vmPeer.getName()) && relationship.endNode.getType().equals(macmPeerType.CSP)) ? relationship.endNode : null;

					if(otherPeer == null){
						otherPeer = (relationship.endNode.getName().equals(vmPeer.getName()) && relationship.startNode.getType().equals(macmPeerType.CSP)) ? relationship.startNode : null;
					}

					if(otherPeer != null && otherPeer.getType().equals(macmPeerType.CSP)){
						logger.info("CSP peer found!");
						return otherPeer;
					}
				}
			}
		}
		logger.info("CSP peer NOT found! I use Aimes default.");
		return null;
	}


	@Override
	public macmSLA getSLAfromNeo(String provider) {
		// TODO Auto-generated method stub
		macmSLA sla =new macmSLA(provider+"_sla", application.getMacmId());
		sla.readNeo(provider+"_sla");
		//		sla.print();
		return sla;
	}

	@Override
	public macmSLAT getSLATfromNeo(String provider) {
		// TODO Auto-generated method stub
		macmSLAT sla = new macmSLAT(provider+"_slat", Integer.valueOf(application.getMacmId()));

		sla.readNeo(provider+"_slat");
		//		sla.print();
		return sla;
	}

	public void exportDB() {
		slar.DB();
	}





}



