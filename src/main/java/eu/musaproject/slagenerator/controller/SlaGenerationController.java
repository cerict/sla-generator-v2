package eu.musaproject.slagenerator.controller;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import eu.musaproject.enumeration.ApplicationState;
import eu.musaproject.enumeration.ComponentState;
import eu.musaproject.model.Application;
import eu.musaproject.model.Component;
import eu.musaproject.model.ComponentControl;
import eu.musaproject.model.ComponentControlMetric;
import eu.musaproject.model.ComponentControlQuestion;
import eu.musaproject.model.Control;
import eu.musaproject.model.Metric;
import eu.musaproject.slagenerator.entity.JsonResponse;
import eu.musaproject.slagenerator.entity.Status;
import eu.musaproject.slagenerator.repositories.ApplicationRepository;
import eu.musaproject.slagenerator.repositories.ComponentControlMetricRepository;
import eu.musaproject.slagenerator.repositories.ComponentRepository;
import eu.musaproject.slagenerator.service.RestClient;
import eu.musaproject.slagenerator.service.SlaGeneratorHelper;

@RestController
@RequestMapping("/sla-generator/api/slaGeneration")
public class SlaGenerationController {

	public static final Logger logger = LoggerFactory.getLogger(SlaGenerationController.class);

	@Autowired ApplicationRepository applicationRepository;
	@Autowired ComponentRepository componentRepository;
	@Autowired ComponentControlMetricRepository componentControlMetricRepository;

	@Autowired SlaGeneratorHelper slaGeneratorHelper;
	@Autowired RestClient restClient;

	@RequestMapping(path="/componentMetrics", method = RequestMethod.GET)
	public ResponseEntity<?> getMetricsList(@RequestParam(value = "componentId") Long componentId, UriComponentsBuilder ucBuilder) {

		logger.info("Get Metrics List called: "+componentId);

		Component component = componentRepository.findOne(componentId);

		if(component.getState() != ComponentState.CREATED){
			//Store the metrics for the component
			List<ComponentControlMetric> componentControlMetrics = new ArrayList<ComponentControlMetric>();
//			logger.info("Number of controls found: "+component.getComponentControls().size());;
			for(ComponentControl componentControl : component.getComponentControls()){

				if(componentControl.getControlMetrics() == null ||
						(componentControl.getControlMetrics() != null && 
						componentControl.getControlMetrics().size()==0)){
					for(Metric metric : componentControl.getControl().getMetrics()){
//						logger.info("Metric found: "+metric.getMetricId());
						ComponentControlMetric componentControlMetric = new ComponentControlMetric();
						componentControlMetric.setMetric(metric);
						componentControlMetric.setSelected(true);
						componentControlMetric.setValue(metric.getDefaultValue());
						componentControl.addThreatControlMetric(componentControlMetric);
						componentControlMetrics.add(componentControlMetric);
					}
				}
			}
			componentRepository.save(component);

			//Retrieve the metrics for the component after the store to retrieve the id
			componentControlMetrics = new ArrayList<ComponentControlMetric>();
			List<String> metricsAdded = new ArrayList<String>();

			for(ComponentControl componentControl : component.getComponentControls()){
//				logger.info("Control Metrics found: "+componentControl.getControlMetrics().size());
				Iterator<ComponentControlMetric> iterator = componentControl.getControlMetrics().iterator();
				while(iterator.hasNext()){
//					logger.info("Metric added");
					ComponentControlMetric compControlMetricToAdd = iterator.next();
					if(!metricsAdded.contains(compControlMetricToAdd.getMetric().getMetricName())){
						List<Control> associatedControls = new ArrayList<Control>();
						associatedControls.add(componentControl.getControl());
						compControlMetricToAdd.setAssociatedControls(associatedControls);
						compControlMetricToAdd.setSelected(true);
						componentControlMetrics.add(compControlMetricToAdd);
						metricsAdded.add(compControlMetricToAdd.getMetric().getMetricName());
					}else{
						for(ComponentControlMetric componentControlMetric : componentControlMetrics){
							if(componentControlMetric.getMetric().getMetricName().equals(compControlMetricToAdd.getMetric().getMetricName())){
								componentControlMetric.getAssociatedControls().add(componentControl.getControl());
							}
						}
					}
				}
			}

			return new ResponseEntity<List<ComponentControlMetric>>(componentControlMetrics, HttpStatus.OK);
		}else{
			return new ResponseEntity<JsonResponse>(new JsonResponse(Status.KO, "The component is in CREATED state. No risk profile associated to the component found!", null), HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(path="/componentAssessedMetrics", method = RequestMethod.GET)
	public ResponseEntity<?> getAssessedMetricsList(@RequestParam(value = "componentId") Long componentId, UriComponentsBuilder ucBuilder) {

		logger.info("Get Assessed Metrics List called: "+componentId);

		Component component = componentRepository.findOne(componentId);

		//Retrieve the metrics for the component related to the assessed controls with information
		ArrayList<ComponentControlMetric> componentControlMetrics = new ArrayList<ComponentControlMetric>();
		for(ComponentControl componentControl : component.getComponentControls()){

			//Per ogni threat control ci sono n questions. Devo verificare che siano tutte YES
			boolean isComponentAdded = true;
			Iterator<ComponentControlQuestion> iteratorComponentControlQuestions = componentControl.getControlQuestions().iterator();
			while(iteratorComponentControlQuestions.hasNext()){
				ComponentControlQuestion componentControlQuestion = iteratorComponentControlQuestions.next();
				if(componentControlQuestion.getAnswer() != null){
					if(!componentControlQuestion.getAnswer().toLowerCase().equals("yes") && 
							!componentControlQuestion.getAnswer().toLowerCase().equals("not applicable")){
//						logger.info("The metrics associated to the control "+componentControl.getControl().getControlName()+" will not added!");
						isComponentAdded = false;
						break;
					}
				}else{
					isComponentAdded = false;
					break;
				}
			}


			logger.info("Threat Control Metrics found: "+componentControl.getControlMetrics().size());
			Iterator<ComponentControlMetric> iteratorComponentControlMetric = componentControl.getControlMetrics().iterator();
			while(iteratorComponentControlMetric.hasNext()){
//				logger.info("Metric added");
				ComponentControlMetric  toAdd = iteratorComponentControlMetric.next();
//				logger.info("Iscomponentadded: "+isComponentAdded);
//				logger.info("getSelected: "+toAdd.getSelected());
				toAdd.setSelected(toAdd.getSelected() == null ? false : toAdd.getSelected()); 
				if(isComponentAdded && toAdd.getSelected()){
					toAdd.setAssessedTrue(true);
					toAdd.setAssessedSelected(true);
				}else{
					toAdd.setAssessedTrue(false);
					toAdd.setAssessedSelected(false);
				}
				componentControlMetrics.add(toAdd);
				componentControlMetricRepository.save(toAdd);
			}
		}
//		logger.info("componentControlMetrics returned: "+componentControlMetrics.size());

		return new ResponseEntity<List<ComponentControlMetric>>(componentControlMetrics, HttpStatus.OK);

	}

	@RequestMapping(path="/generateSla", method = RequestMethod.POST)
	public ResponseEntity<JsonResponse> generateSla(@RequestParam(value = "componentMetrics") String componentMetrics, @RequestParam(value = "componentId") Long componentId, UriComponentsBuilder ucBuilder) {

		logger.info("Generate Sla called: "+componentMetrics);
		Type listType = new TypeToken<ArrayList<ComponentControlMetric>>(){}.getType();

		List<ComponentControlMetric> componentControlMetrics = new Gson().fromJson(componentMetrics, listType);

		//Check if all selected metrics have a value
		for(ComponentControlMetric componentControlMetric : componentControlMetrics){
			if((componentControlMetric.getSelected() && componentControlMetric.getValue() == null) ||
					(componentControlMetric.getSelected() && componentControlMetric.getValue() != null && componentControlMetric.getValue().equals(""))){
				JsonResponse response = new JsonResponse(Status.OK, "Error! Some metrics have an empty value!", null);
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
			}
		}
		
		//Update componentThreatControlMetrics to set the selected metrics
		for(ComponentControlMetric componentControlMetric : componentControlMetrics){
//			logger.info("Find component control metric by id: "+componentControlMetric.getId());
			ComponentControlMetric componentControlMetricGet = componentControlMetricRepository.findOne(componentControlMetric.getId());
			if(componentControlMetricGet != null){
				componentControlMetricGet.setSelected(componentControlMetric.getSelected());
				componentControlMetricGet.setValue(componentControlMetric.getValue());
				componentControlMetricRepository.save(componentControlMetricGet);
			}else{
				logger.error("[error] - component control metric not found in db!");
			}
		}

		String slaXml = slaGeneratorHelper.generateSla(componentId, false);

		Component component = componentRepository.findOne(componentId);
		JsonResponse response = new JsonResponse(Status.OK, "Sla generated successfully", slaXml);

		return ResponseEntity.status(HttpStatus.OK).body(response);

	}

	@RequestMapping(path="/generateAssessedSla", method = RequestMethod.POST)
	public ResponseEntity<JsonResponse> generateAssesedSla(@RequestParam(value = "componentMetrics") String componentMetrics, @RequestParam(value = "componentId") Long componentId, UriComponentsBuilder ucBuilder) {

		logger.info("Generate Sla called: "+componentMetrics);
		Type listType = new TypeToken<ArrayList<ComponentControlMetric>>(){}.getType();

		List<ComponentControlMetric> componentControlMetrics = new Gson().fromJson(componentMetrics, listType);

		//Update componentThreatControlMetrics to set the selected metrics
		for(ComponentControlMetric componentControlMetric : componentControlMetrics){
			ComponentControlMetric componentControlMetricGet = componentControlMetricRepository.findOne(componentControlMetric.getId());
			componentControlMetricGet.setSelected(componentControlMetric.getSelected());
			componentControlMetricGet.setValue(componentControlMetric.getValue());
			componentControlMetricGet.setAssessedSelected(componentControlMetric.getAssessedSelected());
			componentControlMetricRepository.save(componentControlMetricGet);
		}

		String slaXml = slaGeneratorHelper.generateSla(componentId, true);

		Component component = componentRepository.findOne(componentId);
		JsonResponse response = new JsonResponse(Status.OK, "Sla generated successfully", slaXml);

		return ResponseEntity.status(HttpStatus.OK).body(response);

	}

	@RequestMapping(path = "/storeSla", method = RequestMethod.POST)
	public ResponseEntity<?> storeSla(@RequestParam(value = "componentId") Long componentId, @RequestParam(value = "slaXml") String slaXml, @RequestParam(value = "jwtToken") String jwtToken, @RequestParam(value = "userId") String userId, @RequestParam(value = "isAssessedActive") boolean isAssessedActive) {

		logger.info("Component id value: "+componentId);

		Component component = componentRepository.getOne(componentId);
		if(component != null){

			String slaType = "GENERATED";
			if(isAssessedActive){
				slaType = "ASSESSED";
			}
			ResponseEntity<String> response = restClient.putSlaToRepo(slaType, component.getSlaId(), component.getAssessedSlaId(), component.getComosedSlaId(), slaXml, component.getApplication().getId().toString(), component.getApplication().getMacmId(), component.getApplication().getKanbanId(), component.getId().toString(), component.getKanbanId());

			Application application = applicationRepository.findOne(component.getApplication().getId());
			if(response.getStatusCode() == HttpStatus.CREATED  || response.getStatusCode() == HttpStatus.OK){
				logger.info("Response from sla manager: "+response.getBody());
				if(!isAssessedActive){
					if(component.getApplication().getKanbanId() != null && !component.getApplication().getKanbanId().equals("")){
						String errorMessage = restClient.updateSession(component.getApplication().getKanbanId(), component.getKanbanId(), jwtToken, userId, "SLAT_GENERATED", response.getBody(), null, null);
						if(errorMessage != null){
							return new ResponseEntity<JsonResponse>(new JsonResponse(Status.KO, errorMessage, null), HttpStatus.BAD_REQUEST);
						}
					}

					component.setState(ComponentState.SLAT_GENERATED);
					component.setSlaId(response.getBody());

					Set<Component> components = application.getComponents();
					boolean isSLATsGENERATED = true;
					for(Component comp : components){
						if(!comp.getId().equals(componentId) && 
								(comp.getState().equals(ComponentState.SLAT_GENERATED) ||
										comp.getState().equals(ComponentState.SLAT_ASSESSED) ||
										comp.getState().equals(ComponentState.SLA_GENERATED))){
							isSLATsGENERATED = true;
							if(comp.getState().equals(ComponentState.SLA_GENERATED)){
								logger.info("Component is in Generated state! Proceed with removing SLA");
								comp.setSlaId(null);
								if(comp.getAssessedSlaId() != null && !comp.getAssessedSlaId().equals("")){
									comp.setState(ComponentState.SLAT_ASSESSED);
								}else{
									comp.setState(ComponentState.SLAT_GENERATED);
								}
								componentRepository.save(comp);
							}
						}else if(comp.getId().equals(componentId)){
							isSLATsGENERATED = true;
						}else{
							isSLATsGENERATED = false;
							break;
						}
					}
					if(isSLATsGENERATED){
						application.setState(ApplicationState.SLATs_GENERATED);
					}

				}else{
					if(component.getApplication().getKanbanId() != null && !component.getApplication().getKanbanId().equals("")){
						String errorMessage = restClient.updateSession(component.getApplication().getKanbanId(), component.getKanbanId(), jwtToken, userId, "SLAT_ASSESSED", null, response.getBody(), null);
						if(errorMessage != null){
							return new ResponseEntity<JsonResponse>(new JsonResponse(Status.KO, errorMessage, null), HttpStatus.BAD_REQUEST);
						}
					}

					component.setState(ComponentState.SLAT_ASSESSED);
					component.setAssessedSlaId(response.getBody());
					Set<Component> components = application.getComponents();
					boolean isSLATsASSESSED = true;
					for(Component comp : components){
						if(!comp.getId().equals(componentId) && 
								(comp.getState().equals(ComponentState.SLAT_ASSESSED) ||
										comp.getState().equals(ComponentState.SLA_GENERATED))){
							isSLATsASSESSED = true;
							if(comp.getState().equals(ComponentState.SLA_GENERATED)){
								logger.info("Component is in Generated state! Proceed with removing SLA");
								comp.setSlaId(null);
								if(comp.getAssessedSlaId() != null && !comp.getAssessedSlaId().equals("")){
									comp.setState(ComponentState.SLAT_ASSESSED);
								}else{
									comp.setState(ComponentState.SLAT_GENERATED);
								}
								componentRepository.save(comp);
							}
						}else if(comp.getId().equals(componentId)){
							isSLATsASSESSED = true;
						}else{
							isSLATsASSESSED = false;
							break;
						}	
					}
					if(isSLATsASSESSED){
						application.setState(ApplicationState.SLATs_ASSESSED);
					}
				}
				applicationRepository.save(application);
				componentRepository.save(component);

				return response;
			}
		}
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

}



