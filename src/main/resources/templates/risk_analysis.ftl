<style>
table, th, td {
	border: 0px solid grey;
	border-collapse: collapse;
	padding: 5px;
}

textarea {
	width: 100%;
}

input[type=number] {
	width: 30px;
}

img {
	width: 100%;
}

input[type=checkbox] {
	/* All browsers except webkit*/
	transform: scale(2);
	/* Webkit browsers*/
	-webkit-transform: scale(2);
}

a.tip {
	text-decoration: none
}

a.tip:hover {
	cursor: help;
	position: relative
}

a.tip span {
	display: none
}

a.tip:hover span {
	border: #c0c0c0 1px dotted;
	padding: 5px 20px 5px 5px;
	display: block;
	z-index: 100;
	background: #f0f0f0 no-repeat 100% 5%;
	left: 0px;
	margin: 10px;
	width: 250px;
	position: absolute;
	top: 10px;
	text-decoration: none
}

a.tip2 {
	color: #000000;
	text-decoration: none
}

a.tip2:hover {
	cursor: help;
	position: relative
}

a.tip2 span {
	display: none
}

a.tip2:hover span {
	border: #c0c0c0 1px dotted;
	padding: 5px 20px 5px 5px;
	display: block;
	z-index: 100;
	background: #f0f0f0 no-repeat 100% 5%;
	left: 0px;
	margin: 10px;
	width: 250px;
	position: absolute;
	top: 10px;
	text-decoration: none
}

.CSSTableGenerator {
	margin: 0px;
	padding: 0px;
	width: 100%;
	box-shadow: 10px 10px 5px #888888;
	border: 1px solid #000000;
	-moz-border-radius-bottomleft: 0px;
	-webkit-border-bottom-left-radius: 0px;
	border-bottom-left-radius: 0px;
	-moz-border-radius-bottomright: 0px;
	-webkit-border-bottom-right-radius: 0px;
	border-bottom-right-radius: 0px;
	-moz-border-radius-topright: 0px;
	-webkit-border-top-right-radius: 0px;
	border-top-right-radius: 0px;
	-moz-border-radius-topleft: 0px;
	-webkit-border-top-left-radius: 0px;
	border-top-left-radius: 0px;
}

.CSSTableGenerator table {
	border-collapse: collapse;
	border-spacing: 0;
	width: 100%;
	height: 100%;
	margin: 0px;
	padding: 0px;
}

.CSSTableGenerator tr:last-child td:last-child {
	-moz-border-radius-bottomright: 0px;
	-webkit-border-bottom-right-radius: 0px;
	border-bottom-right-radius: 0px;
}

.CSSTableGenerator table tr:first-child td:first-child {
	-moz-border-radius-topleft: 0px;
	-webkit-border-top-left-radius: 0px;
	border-top-left-radius: 0px;
}

.CSSTableGenerator table tr:first-child td:last-child {
	-moz-border-radius-topright: 0px;
	-webkit-border-top-right-radius: 0px;
	border-top-right-radius: 0px;
}

.CSSTableGenerator tr:last-child td:first-child {
	-moz-border-radius-bottomleft: 0px;
	-webkit-border-bottom-left-radius: 0px;
	border-bottom-left-radius: 0px;
}

.CSSTableGenerator tr:hover td {
	background-color: #f2f2f2;
}

.CSSTableGenerator td {
	vertical-align: middle;
	background-color: #ffffff;
	border: 1px solid #000000;
	border-width: 0px 1px 1px 0px;
	text-align: center;
	padding: 7px;
	font-size: 14px;
	font-family: Arial;
	font-weight: normal;
	color: #000000;
}

.CSSTableGenerator tr:last-child td {
	border-width: 0px 1px 0px 0px;
}

.CSSTableGenerator tr td:last-child {
	border-width: 0px 0px 1px 0px;
}

.CSSTableGenerator tr:last-child td:last-child {
	border-width: 0px 0px 0px 0px;
}

.CSSTableGenerator tr:first-child td {
	background: -o-linear-gradient(bottom, #ffffff 5%, #ffffff 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ffffff
		), color-stop(1, #ffffff));
	background: -moz-linear-gradient(center top, #ffffff 5%, #ffffff 100%);
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#ffffff",
		endColorstr="#ffffff");
	background: -o-linear-gradient(top, #ffffff, ffffff);
	background-color: #ffffff;
	border: 0px solid #000000;
	text-align: center;
	border-width: 0px 0px 1px 1px;
	font-size: 14px;
	font-family: Arial;
	font-weight: bold;
	color: #000000;
}

.CSSTableGenerator tr:first-child:hover td {
	background: -o-linear-gradient(bottom, #ffffff 5%, #ffffff 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ffffff
		), color-stop(1, #ffffff));
	background: -moz-linear-gradient(center top, #ffffff 5%, #ffffff 100%);
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#ffffff",
		endColorstr="#ffffff");
	background: -o-linear-gradient(top, #ffffff, ffffff);
	background-color: #ffffff;
}

.CSSTableGenerator tr:first-child td:first-child {
	border-width: 0px 0px 1px 0px;
}

.CSSTableGenerator tr:first-child td:last-child {
	border-width: 0px 0px 1px 1px;
}
</style>

<div class=container>

	</br>

	<ul class="nav nav-pills nav-wizard">
		<li ng-class="{ active: getTabSelected('Start')}"
			ng-click="ctrl.selectedTab = 'Start'"><a
			style="pointer-events: none; cursor: default;" data-toggle="tab"
			ng-click="ctrl.selectedTab = 'Start'">Start</a>
			<div class="nav-arrow"></div></li>
		<li ng-class="{ active: getTabSelected('Threats Selection')}"
			ng-click="ctrl.selectedTab = 'Threats Selection'"><div
				class="nav-wedge"></div> <a
			style="pointer-events: none; cursor: default;" data-toggle="tab">Threats
				Selection</a>
			<div class="nav-arrow"></div></li>
		<li ng-class="{ active: getTabSelected('Risks Evaluation')}"
			ng-click="ctrl.selectedTab = 'Risks Evaluation'"><div
				class="nav-wedge"></div> <a
			style="pointer-events: none; cursor: default;" data-toggle="tab">Risks
				Evaluation</a>
			<div class="nav-arrow"></div></li>
		<li ng-class="{ active: getTabSelected('Controls Selection')}"
			ng-click="ctrl.selectedTab = 'Controls Selection'"><div
				class="nav-wedge"></div> <a
			style="pointer-events: none; cursor: default;" data-toggle="tab">Controls
				Selection</a>
			<div class="nav-arrow"></div></li>
		<li ng-class="{ active: getTabSelected('Analysis Results')}"
			ng-click="ctrl.selectedTab = 'Analysis Results'"><div
				class="nav-wedge"></div> <a
			style="pointer-events: none; cursor: default;" data-toggle="tab">Analysis
				Results</a></li>

	</ul>

	</br>
	<!-- START -->
	<div class="panel panel-default" ng-show="ctrl.selectedTab == 'Start'">
		<div class="panel-heading text-center">
			<span class="lead">
				<h2>Risk Analysis ({{ctrl.application.activeComponent.name}})</h2>
			</span>
		</div>

		<div class="panel-body">

			<p>This section is useful to execute a Risk Analysis about the
				components of your application to .....</p>
			<br>
			<p>To do this you have to:</p>
			<li>Respond to the questions about the threats that can infect
				your component</li>
			<li>Define the value of the impact factors</li>
			<li>Select the security controls related to the threats
				previously chosen</li> <br>
			<div style="text-align: center;">
				<button style="text-align: center;" class="btn btn-primary"
					ng-click="ctrl.startRiskAnalysis()">Start with Risk
					Analysis</button>
			</div>

		</div>
	</div>

	<!-- THREATS SELECTION -->
	<div class="panel panel-default"
		ng-show="ctrl.selectedTab == 'Threats Selection'">
		<div class="panel-heading text-center">
			<span class="lead">
				<h2>Threats Selection
					({{ctrl.application.activeComponent.name}})</h2>
			</span>
		</div>

		<ul class="pager">
			<li class="previous"><a ng-click="ctrl.selectedTab = 'Start'">Previous</a></li>
			<li class="next"><a ng-click="ctrl.nextToRiskEvaluation()">Next</a></li>
		</ul>

		<div class="panel-body">

			<div
				ng-show="ctrl.application.id == null || ctrl.application.id == ''">
				There isn't an application to execute Risk Analysis.<br> To
				start with the Risk Analysis select "SetUp Application" button on
				top right of header bar and load an application.
			</div>

			<div
				ng-show="ctrl.application.id != null && ctrl.application.id != ''">

				<p>To execute the Threats Selection click the button "LOAD
					Questions" below</p>
				<p>
					<b>NB:</b> To proceed with the SLA generation do not forget to
					execute all Risk Analysis process!
				</p>

				<button style="text-align: center;" class="btn btn-primary"
					ng-click="ctrl.loadQuestions()"
					ng-disabled="ctrl.application.activeComponent.componentGroup!='saas' && ctrl.application.activeComponent.componentGroup!='SaaS'">LOAD
					Questions</button>

			</div>
			<br>
			<div class="panel panel-default">
				<div class="panel-heading text-center">
					<h2>Answer the Questions for Threats definition</h2>
				</div>
				<div class="panel-body">
					<div
						ng-show="ctrl.application.activeComponent.threatQuestions != null">
						<table border="1" style="width: 100%;">
							<tr
								ng-repeat="threatQuestion in ctrl.application.activeComponent.threatQuestions">

								<td style="padding: 10px;" width="5%"><b>{{threatQuestion.questionId}}</b></td>
								<td style="padding: 10px;" width="75%"><b>Question:</b><br>{{threatQuestion.description}}
								</td>
								<td style="padding: 10px;" width="20%"><label
									class="checkbox-inline">
										<div class="btn-group btn-toggle">
											<button
												ng-class="{'true':'btn btn-primary active','false':'btn btn-default'}[threatQuestion.selected == 'true']"
												ng-click="threatQuestion.selected = 'true'">YES</button>
											<button
												ng-class="{'true':'btn btn-primary active','false':'btn btn-default'}[threatQuestion.selected == 'false']"
												ng-click="threatQuestion.selected = 'false'">NO</button>
										</div></td>
							</tr>
						</table>
						<div style="text-align: center;">
							<button style="text-align: center;" class="btn btn-primary"
								ng-click="ctrl.submitQuestions()">Submit Questions</button>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="panel panel-default">
				<div class="panel-heading text-center">
					<h2>Threats list</h2>
				</div>
				<div class="panel-body">
					<div ng-show="ctrl.application.activeComponent.threats != null">
						<table border="1" style="width: 100%">
							<tr
								ng-repeat="threat in ctrl.application.activeComponent.threats"
								style="border-bottom: 1px solid #ddd">

								<td style="padding: 10px;" width="25%">
									<div style="padding: 3px;">
										<b>Id: </b> {{threat.threat.threatId}}
									</div>
									<div style="padding: 3px;">
										<b>Strides: </b>
										<p ng-repeat="stride in threat.threat.strides">{{stride.name}}</p>
									</div>

								</td>
								<td style="padding: 10px;" width="75%"><div
										style="padding: 3px; font-size: 120%;">
										<b>{{threat.threat.threatName}}</b>
									</div> <b>Description:</b>
									<div style="padding: 3px;">{{threat.threat.threatDescription}}</div></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
		<ul class="pager">
			<li class="previous"><a ng-click="ctrl.selectedTab = 'Start'">Previous</a></li>
			<li class="next"><a ng-click="ctrl.nextToRiskEvaluation()">Next</a></li>
		</ul>
	</div>


	<!-- RISKS EVALUATION -->
	<div class="panel panel-default"
		ng-show="ctrl.selectedTab == 'Risks Evaluation'">
		<div class="panel-heading text-center">
			<span class="lead">
				<h2>Risks Evaluation
					({{ctrl.application.activeComponent.name}})</h2>
			</span>
		</div>

		<ul class="pager">
			<li class="previous"><a
				ng-click="ctrl.selectedTab = 'Threats Selection'">Previous</a></li>
			<li class="next"><a ng-click="ctrl.nextToControlsSelection()">Next</a></li>
		</ul>

		<div class="panel-body">

			<div
				ng-show="ctrl.application.id == null || ctrl.application.id == ''">
				There isn't an application to execute Risk Analysis.<br> To
				start with the Risk Analysis select "SetUp Application" button on
				top right of header bar and load an application.
			</div>

			<div
				ng-show="ctrl.application.id != null && ctrl.application.id != ''">

				<p>By following the approach here, it is possible to estimate
					the severity of all of these risks to the business and make an
					informed decision about what to do about those risks. Having a
					system in place for rating risks will save time and eliminate
					arguing about priorities.</p>
				<p>Below are shown all the selected threats of previous section.
					For each of them please insert appropriate values to assess impact
					overall system.</p>
				<p>The lieklood level is already set and it is no editable.</p>
				<p>At the end, you'll get an overall risk level according to the
					scale shown below:</p>
				<table align="center">
					<tr>
						<td align="center" width="90px">VERY LOW</td>
						<td align="center" width="90px">LOW</td>
						<td align="center" width="90px">MEDIUM</td>
						<td align="center" width="90px">HIGH</td>
						<td align="center" width="90px">CRITICAL</td>
					</tr>
					<tr>
						<td style="background: green; padding: 5px;"></td>
						<td style="background: #d9d926; padding: 5px;"></td>
						<td style="background: orange; padding: 5px;"></td>
						<td style="background: red; padding: 5px;"></td>
						<td style="background: #ff00ff; padding: 5px;"></td>
					</tr>
				</table>
				<br>
				<div ng-show="ctrl.application.activeComponent.threats != null">
					<table style="width: 100%; border: 1px solid grey;">

						<tr ng-repeat='threat in ctrl.application.activeComponent.threats'
							style="border: 1px solid grey;">
							<td width="20%"><a class="tip2">{{threat.threat.threatName}}<br>
									<span>{{threat.threat.threatDescription}}</span></a></td>
							<td align="center"><span
								ng-style="{'VERY LOW':{color:'green'},'LOW':{color:'#d9d926'},'MEDIUM':{color:'orange'},'HIGH':{color:'red'},'CRITICAL':{color:'#ff00ff'}}[threat.riskSeverity]"><b>OVERALL
										RISK SEVERITY = {{threat.riskSeverity}}</b></span>

								<table>
									<td align="center" valign="top"><b>LIKELIHOOD =
											{{threat.likehood}}</b>

										<table>

											<tr>
												<td align="center"><b>Threat Agent Factors</b></td>
												<td align="center"><b>Vulnerability Factors</b></td>
											</tr>
										</table>
										<table>
											<tr>
												<td><a class="tip">Skill level<span>How
															technically skilled is this group of threat agents?
															Security penetration skills (9), network and programming
															skills (6), advanced computer user (5), some technical
															skills (3), no technical skills (1)</span></a></td>
												<td><input type="number"
													ng-change="ctrl.updateRiskEvaluation()"
													ng-model="threat.skillLevel" min="0" max="9"
													ng-disabled="true"></td>
												<td><a class="tip">Ease of discovery<span>How
															easy is it for this group of threat agents to discover
															this vulnerability? Practically impossible (1), difficult
															(3), easy (7), automated tools available (9)</span></a></td>
												<td><input type="number"
													ng-change="ctrl.updateRiskEvaluation()"
													ng-model="threat.easeOfDiscovery" min="0" max="9"></td>
											</tr>
											<tr>
												<td><a class="tip">Motive<span>How motivated
															are the threat agents to find and exploit this
															vulnerability? Low or no reward (1), possible reward (4),
															high reward (9)</span></a></td>
												<td><input type="number"
													ng-change="ctrl.updateRiskEvaluation()"
													ng-model="threat.motive" min="0" max="9" ng-disabled="true"></td>
												<td><a class="tip">Ease of exploit<span>How
															easy is it for this group of threat agents to actually
															exploit this vulnerability? Theoretical (1), difficult
															(3), easy (5), automated tools available (9)</span></a></td>
												<td><input type="number"
													ng-change="ctrl.updateRiskEvaluation()"
													ng-model="threat.easeOfExploit" min="0" max="9"
													ng-disabled="true"></td>
											</tr>
											<tr>
												<td><a class="tip">Opportunity<span>What
															resources and opportunities are required for this group
															of threat agents to find and exploit this vulnerability?
															Full access or expensive resources required (0), special
															access or resources required (4), some access or
															resources required (7), no access or resources required
															(9)</span></a></td>
												<td><input type="number"
													ng-change="ctrl.updateRiskEvaluation()"
													ng-model="threat.opportunity" min="0" max="9"
													ng-disabled="true"></td>
												<td><a class="tip">Awareness<span>How well
															known is this vulnerability to this group of threat
															agents? Unknown (1), hidden (4), obvious (6), public
															knowledge (9)</span></a></td>
												<td><input type="number"
													ng-change="ctrl.updateRiskEvaluation()"
													ng-model="threat.awareness" min="0" max="9"
													ng-disabled="true"></td>
											</tr>
											<tr>
												<td><a class="tip">Size<span>How large is
															this group of threat agents? Developers (2), system
															administrators (2), intranet users (4), partners (5),
															authenticated users (6), anonymous Internet users (9)</span></a></td>
												<td><input type="number"
													ng-change="ctrl.updateRiskEvaluation()"
													ng-model="threat.size" min="0" max="9" ng-disabled="true"></td>
												<td><a class="tip">Intrusion detection<span>How
															likely is an exploit to be detected? Active detection in
															application (1), logged and reviewed (3), logged without
															review (8), not logged (9)</span></a></td>
												<td><input type="number"
													ng-change="ctrl.updateRiskEvaluation()"
													ng-model="threat.intrusionDetection" min="0" max="9"
													ng-disabled="true"></td>
											</tr>

										</table></td>

									<td align="center" valign="top"><b>IMPACT =
											{{threat.impact}}</b>

										<table>

											<tr>
												<td align="center"><b>Technical Impact Factors</b></td>
												<td align="center"><b>Business Impact Factors</b></td>
											</tr>
										</table>
										<table>
											<tr>
												<td><a class="tip">Loss of confidentiality<span>How
															much data could be disclosed and how sensitive is it?
															Minimal non-sensitive data disclosed (2), minimal
															critical data disclosed (6), extensive non-sensitive data
															disclosed (6), extensive critical data disclosed (7), all
															data disclosed (9)</span></a></td>
												<td><input type="number"
													ng-change="ctrl.updateRiskEvaluation()"
													ng-model="threat.lossOfConfidentiality" min="0" max="9"
													value="0" ng-disabled="true"></td>
												<td><a class="tip">Financial damage<span>How
															much financial damage will result from an exploit? Less
															than the cost to fix the vulnerability (1), minor effect
															on annual profit (3), significant effect on annual profit
															(7), bankruptcy (9)</span></a></td>
												<td><input type="number"
													ng-change="ctrl.updateRiskEvaluation()"
													ng-model="threat.financialDamage" min="0" max="9" value="0"></td>
											</tr>
											<tr>
												<td><a class="tip">Loss of integrity<span>How
															much data could be corrupted and how damaged is it?
															Minimal slightly corrupt data (1), minimal seriously
															corrupt data (3), extensive slightly corrupt data (5),
															extensive seriously corrupt data (7), all data totally
															corrupt (9)</span></a></td>
												<td><input type="number"
													ng-change="ctrl.updateRiskEvaluation()"
													ng-model="threat.lossOfIntegrity" min="0" max="9" value="0"
													ng-disabled="true"></td>
												<td><a class="tip">Reputation damage<span>Would
															an exploit result in reputation damage that would harm
															the business? Minimal damage (1), Loss of major accounts
															(4), loss of goodwill (5), brand damage (9)</span></a></td>
												<td><input type="number"
													ng-change="ctrl.updateRiskEvaluation()"
													ng-model="threat.reputationDamage" min="0" max="9"
													value="0"></td>
											</tr>
											<tr>
												<td><a class="tip">Loss of availability<span>How
															much service could be lost and how vital is it? Minimal
															secondary services interrupted (1), minimal primary
															services interrupted (5), extensive secondary services
															interrupted (5), extensive primary services interrupted
															(7), all services completely lost (9)</span></a></td>
												<td><input type="number"
													ng-change="ctrl.updateRiskEvaluation()"
													ng-model="threat.lossOfAvailability" min="0" max="9"
													value="0" ng-disabled="true"></td>
												<td><a class="tip">Non-compliance<span>How
															much exposure does non-compliance introduce? Minor
															violation (2), clear violation (5), high profile
															violation (7)</span></a></td>
												<td><input type="number"
													ng-change="ctrl.updateRiskEvaluation()"
													ng-model="threat.nonCompliance" min="0" max="9" value="0"></td>
											</tr>
											<tr>
												<td><a class="tip">Loss of accountability<span>Are
															the threat agents' actions traceable to an individual?
															Fully traceable (1), possibly traceable (7), completely
															anonymous (9)</span></a></td>
												<td><input type="number"
													ng-change="ctrl.updateRiskEvaluation()"
													ng-model="threat.lossOfAccountability" min="0" max="9"
													value="0" ng-disabled="true"></td>
												<td><a class="tip">Privacy violation<span>How
															much personally identifiable information could be
															disclosed? One individual (3), hundreds of people (5),
															thousands of people (7), millions of people (9)</span></a></td>
												<td><input type="number"
													ng-change="ctrl.updateRiskEvaluation()"
													ng-model="threat.privacyViolation" min="0" max="9"
													value="0"></td>
											</tr>

										</table></td>

								</table></td>


						</tr>

					</table>
				</div>
				<br>
				<div style="text-align: center;">
					<button style="text-align: center;" class="btn btn-primary"
						ng-click="ctrl.saveEvaluation()">Save Evaluation</button>
				</div>
				<br>

				<div class="panel panel-default">
					<table class="CSSTableGenerator" border="1" ; width=100%>
						<tr>
							<td width=30%></td>
							<td align="center">{{ctrl.application.activeComponent.name}}</td>
						</tr>
						<tr>
							<td><b>Spoofing</b></td>
							<td
								ng-style="{'VERY LOW':{background:'green'},'LOW':{background:'#d9d926'},'MEDIUM':{background:'orange'},'HIGH':{background:'red'},'CRITICAL':{background:'#ff00ff'}}[ctrl.getStrideRisk('SPOOFING')]"
								align="center">{{ctrl.getStrideRisk('SPOOFING')}}</td>
						</tr>
						<tr>
							<td><b>Tampering</b></td>
							<td
								ng-style="{'VERY LOW':{background:'green'},'LOW':{background:'#d9d926'},'MEDIUM':{background:'orange'},'HIGH':{background:'red'},'CRITICAL':{background:'#ff00ff'}}[ctrl.getStrideRisk('TAMPERING')]"
								align="center">{{ctrl.getStrideRisk('TAMPERING')}}</td>
						</tr>
						<tr>
							<td><b>Repudiation</b></td>
							<td
								ng-style="{'VERY LOW':{background:'green'},'LOW':{background:'#d9d926'},'MEDIUM':{background:'orange'},'HIGH':{background:'red'},'CRITICAL':{background:'#ff00ff'}}[ctrl.getStrideRisk('REPUDIATION')]"
								align="center">{{ctrl.getStrideRisk('REPUDIATION')}}</td>
						</tr>
						<tr>
							<td><b>Information Disclosure</b></td>
							<td
								ng-style="{'VERY LOW':{background:'green'},'LOW':{background:'#d9d926'},'MEDIUM':{background:'orange'},'HIGH':{background:'red'},'CRITICAL':{background:'#ff00ff'}}[ctrl.getStrideRisk('INFORMATION DISCLOSURE')]"
								align="center">{{ctrl.getStrideRisk('INFORMATION DISCLOSURE')}}</td>
						</tr>
						<tr>
							<td><b>Denial of Service</b></td>
							<td
								ng-style="{'VERY LOW':{background:'green'},'LOW':{background:'#d9d926'},'MEDIUM':{background:'orange'},'HIGH':{background:'red'},'CRITICAL':{background:'#ff00ff'}}[ctrl.getStrideRisk('DENIAL OF SERVICE')]"
								align="center">{{ctrl.getStrideRisk('DENIAL OF SERVICE')}}</td>
						</tr>
						<tr>
							<td><b>Elevation of Privileges</b></td>
							<td
								ng-style="{'VERY LOW':{background:'green'},'LOW':{background:'#d9d926'},'MEDIUM':{background:'orange'},'HIGH':{background:'red'},'CRITICAL':{background:'#ff00ff'}}[ctrl.getStrideRisk('ELEVATION OF PRIVILEGES')]"
								align="center">{{ctrl.getStrideRisk('ELEVATION OF PRIVILEGES')}}</td>
						</tr>
					</table>
				</div>
			</div>
		</div>



		<ul class="pager">
			<li class="previous"><a
				ng-click="ctrl.selectedTab = 'Threats Selection'">Previous</a></li>
			<li class="next"><a ng-click="ctrl.nextToControlsSelection()">Next</a></li>
		</ul>
	</div>

	<!-- CONTROLS SELECTION -->
	<div class="panel panel-default"
		ng-show="ctrl.selectedTab == 'Controls Selection'">
		<div class="panel-heading text-center">
			<span class="lead">
				<h2>Controls Selection
					({{ctrl.application.activeComponent.name}})</h2>
			</span>
		</div>

		<ul class="pager">
			<li class="previous"><a
				ng-click="ctrl.selectedTab = 'Risks Evaluation'">Previous</a></li>
			<li class="next"><a ng-click="ctrl.nextToAnalysisResult()">Next</a></li>
		</ul>

		<div class="panel-body">
			<div
				ng-show="ctrl.application.id == null || ctrl.application.id == ''">
				There isn't an application to execute Risk Analysis.<br> To
				start with the Risk Analysis select "SetUp Application" button on
				top right of header bar and load an application.
			</div>

			<div
				ng-show="ctrl.application.id != null && ctrl.application.id != ''">

				<p>Below there is a list of security controls suggested on the
					threats that you have previously chosen.</p>
				<p>To proceed with the Risk Analysis, select the appropriate
					security controls!</p>

			</div>
			<br>

			<div class="panel panel-default">
				<div class="panel-heading text-center">
					<h2>Controls list</h2>
				</div>
				<div class="panel-body">
					<div ng-show="ctrl.application.activeComponent.controls != null">
						<table border="1" style="width: 100%">
							<tr
								ng-repeat="control in ctrl.application.activeComponent.controls"
								style="border-bottom: 1px solid #ddd">

								<td style="padding: 10px;" width="20%">
									<div style="padding: 3px;">
										<b>Id: </b> {{control.controlId}}
									</div>
									<div style="padding: 2px;">
										<b>Family: </b>
										<p>{{control.familyName}}</p>
									</div>
									
									<div style="padding: 3px;">
										<b>Risk: </b>
										<p ng-style="{'LOW':{color:'green'},'MOD':{color:'orange'},'HIGH':{color:'red'}}[control.risk]">{{control.risk}}</p>
									</div>

								</td>
								<td style="padding: 10px;" width="60%"><div
										style="padding: 3px; font-size: 120%;">
										<b>{{control.controlName}}</b>
									</div> <b>Description:</b>
									<div style="padding: 3px;">{{control.controlDescription}}</div></td>


								<td style="padding: 5px;" width="20%">
									<div class="btn-group btn-toggle">
										<button
											ng-class="{'true':'btn btn-primary active','false':'btn btn-default'}[control.selected == true]"
											ng-click="control.selected = true">YES</button>
										<button
											ng-class="{'true':'btn btn-primary active','false':'btn btn-default'}[control.selected == false]"
											ng-click="control.selected = false">NO</button>
									</div>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>

			<br>
			<div style="text-align: center;">
				<button style="text-align: center;" class="btn btn-primary"
					ng-click="ctrl.submitControls()">Submit Controls</button>
			</div>
			<br>
		</div>

		<ul class="pager">
			<li class="previous"><a
				ng-click="ctrl.selectedTab = 'Risks Evaluation'">Previous</a></li>
			<li class="next"><a ng-click="ctrl.nextToAnalysisResult()">Next</a></li>
		</ul>

	</div>

	<!-- ANALYSIS RESULT -->
	<div class="panel panel-default"
		ng-show="ctrl.selectedTab == 'Analysis Results'">
		<div class="panel-heading text-center">
			<span class="lead">
				<h2>Analysis Results
					({{ctrl.application.activeComponent.name}})</h2>
			</span>
		</div>

		<ul class="pager">
			<li class="previous"><a
				ng-click="ctrl.selectedTab = 'Controls Selection'">Previous</a></li>
		</ul>

		<div class="panel-body">

			<div class="panel panel-default">
				<div class="panel-heading text-center">
					<h2>Risks Evaluation Results</h2>
				</div>
				<div class="panel-body">
					<table class="CSSTableGenerator" border="1" ; width=100%>
						<tr>
							<td width=30%></td>
							<td align="center">{{ctrl.application.activeComponent.name}}</td>
						</tr>
						<tr>
							<td><b>Spoofing</b></td>
							<td
								ng-style="{'VERY LOW':{background:'green'},'LOW':{background:'#d9d926'},'MEDIUM':{background:'orange'},'HIGH':{background:'red'},'CRITICAL':{background:'#ff00ff'}}[ctrl.getStrideRisk('SPOOFING')]"
								align="center">{{ctrl.getStrideRisk('SPOOFING')}}</td>
						</tr>
						<tr>
							<td><b>Tampering</b></td>
							<td
								ng-style="{'VERY LOW':{background:'green'},'LOW':{background:'#d9d926'},'MEDIUM':{background:'orange'},'HIGH':{background:'red'},'CRITICAL':{background:'#ff00ff'}}[ctrl.getStrideRisk('TAMPERING')]"
								align="center">{{ctrl.getStrideRisk('TAMPERING')}}</td>
						</tr>
						<tr>
							<td><b>Repudiation</b></td>
							<td
								ng-style="{'VERY LOW':{background:'green'},'LOW':{background:'#d9d926'},'MEDIUM':{background:'orange'},'HIGH':{background:'red'},'CRITICAL':{background:'#ff00ff'}}[ctrl.getStrideRisk('REPUDIATION')]"
								align="center">{{ctrl.getStrideRisk('REPUDIATION')}}</td>
						</tr>
						<tr>
							<td><b>Information Disclosure</b></td>
							<td
								ng-style="{'VERY LOW':{background:'green'},'LOW':{background:'#d9d926'},'MEDIUM':{background:'orange'},'HIGH':{background:'red'},'CRITICAL':{background:'#ff00ff'}}[ctrl.getStrideRisk('INFORMATION DISCLOSURE')]"
								align="center">{{ctrl.getStrideRisk('INFORMATION DISCLOSURE')}}</td>
						</tr>
						<tr>
							<td><b>Denial of Service</b></td>
							<td
								ng-style="{'VERY LOW':{background:'green'},'LOW':{background:'#d9d926'},'MEDIUM':{background:'orange'},'HIGH':{background:'red'},'CRITICAL':{background:'#ff00ff'}}[ctrl.getStrideRisk('DENIAL OF SERVICE')]"
								align="center">{{ctrl.getStrideRisk('DENIAL OF SERVICE')}}</td>
						</tr>
						<tr>
							<td><b>Elevation of Privileges</b></td>
							<td
								ng-style="{'VERY LOW':{background:'green'},'LOW':{background:'#d9d926'},'MEDIUM':{background:'orange'},'HIGH':{background:'red'},'CRITICAL':{background:'#ff00ff'}}[ctrl.getStrideRisk('ELEVATION OF PRIVILEGES')]"
								align="center">{{ctrl.getStrideRisk('ELEVATION OF PRIVILEGES')}}</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading text-center">
					<h2>Threats list</h2>
				</div>
				<div class="panel-body">
					<div ng-show="ctrl.application.activeComponent.threats != null">

						<div class="form-group">
							<label for="sel1">Choose Threat to show:</label> <select
								class="form-control"
								ng-options="threat.threat as threat.threat.threatName for threat in ctrl.application.activeComponent.threats track by threat.threat.threatName"
								ng-model="selectedThreat"></select>
						</div>

						<div ng-show=selectedThreat>
							<table border="1" style="width: 100%;">
								<tr style="border-bottom: 1px solid #ddd">

									<td style="padding: 10px;" width="25%">
										<div style="padding: 3px;">
											<b>Id: </b> {{selectedThreat.threatId}}
										</div>
										<div style="padding: 3px;">
											<b>Strides: </b>
											<p ng-repeat="stride in selectedThreat.strides">{{stride.name}}</p>
										</div>

									</td>
									<td style="padding: 10px;" width="75%"><div
											style="padding: 3px; font-size: 120%;">
											<b>{{selectedThreat.threatName}}</b>
										</div> <b>Description:</b>
										<div style="padding: 3px;">{{selectedThreat.threatDescription}}</div></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>

			<br>



			<div class="panel panel-default">
				<div class="panel-heading text-center">
					<h2>Selected Controls list</h2>
				</div>
				<div class="panel-body">
					<div ng-show="ctrl.application.activeComponent.controls != null">

						<div class="form-group">
							<label for="sel1">Choose Control to show:</label> <select
								class="form-control"
								ng-options="control as control.controlName for control in ctrl.application.activeComponent.controls  | filter: { selected: 'true' } track by control.controlName"
								ng-model="selectedControl"></select>
						</div>

						<div ng-show=selectedControl>
							<table border="1" style="width: 100%">
								<tr style="border-bottom: 1px solid #ddd">

									<td style="padding: 10px;" width="20%">
										<div style="padding: 3px;">
											<b>Id: </b> {{selectedControl.controlId}}
										</div>
										<div style="padding: 3px;">
											<b>Family: </b>
											<p>{{selectedControl.familyName}}</p>
										</div>
										
										<div style="padding: 3px;">
										<b>Risk: </b>
										<p ng-style="{'LOW':{color:'green'},'MOD':{color:'orange'},'HIGH':{color:'red'}}[selectedControl.risk]">{{selectedControl.risk}}</p>
									</div>

									</td>
									<td style="padding: 10px;" width="60%"><div
											style="padding: 3px; font-size: 120%;">
											<b>{{selectedControl.controlName}}</b>
										</div> <b>Description:</b>
										<div style="padding: 3px;">{{selectedControl.controlDescription}}</div></td>

								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>


			<ul class="pager">
				<li class="previous"><a
					ng-click="ctrl.selectedTab = 'Controls Selection'">Previous</a></li>
			</ul>
		</div>

	</div>
</div>





