<div class=container>
	<div class="panel panel-default">
		<div class="panel-heading text-center">
			<span class="lead">
				<h2>Implementation Plan Viewer</h2>
			</span>
		</div>

		<div class="panel-body">

			<div class="input-group-btn">

				<div class="input-group">
					<span class="input-group-btn"> <label
						class="btn btn-primary btn-file">Load an Implementation
							Plan <input type="file" id="input-plan" style="display: none">
					</label>
					</span>
				</div>

			</div>
			</br>
			<div class="table-responsive" ng-show="results">
				<table class=table>
					<thead class=active>
						<tr>
							<th style="padding: 10px;" width="30%" class=text-center>SLA
								Identifier</th>
							<th style="padding: 10px;" width="30%" class=text-center>Creation
								Time</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="padding: 10px;" width="30%" class=text-center>{{results.sla_id}}</td>
							<td style="padding: 10px;" width="30%" class=text-center>{{results.creation_time}}</td>
						</tr>

					</tbody>
				</table>
				<hr>
				<div ng-repeat="csp in results.csps">
					<h2 style="padding: 10px;" width="100%" class=text-center>
						Cloud Service Provider <small>({{csp.iaas.provider}})</small>
					</h2>
					<table class=table>
						<thead class=active>
							<tr>
								<th style="padding: 10px;" width="20%" class=text-center>Provider</th>
								<th style="padding: 10px;" width="20%" class=text-center>Zone</th>
								<th style="padding: 10px;" width="20%" class=text-center>User</th>
								<th style="padding: 10px;" width="40%" class=text-center>Network</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="padding: 10px;" width="20%" class=text-center>{{csp.iaas.provider}}</td>
								<td style="padding: 10px;" width="20%" class=text-center>{{csp.iaas.zone}}</td>
								<td style="padding: 10px;" width="20%" class=text-center>{{csp.iaas.user}}</td>
								<td style="padding: 10px;" width="40%" class=text-center>{{csp.iaas.network}}</td>
							</tr>

						</tbody>
					</table>


					<!--  VMS -->
					<div ng-repeat="vm in csp.pools[0].vms">
						<h3 style="padding: 10px;" width="100%" class=text-center>
							VM <small>({{vm.vm_seq_num}})</small>
						</h3>
						<table class=table>
							<thead class=active>
								<tr>
									<th style="padding: 10px;" width="50%" class=text-center>Appliance</th>
									<th style="padding: 10px;" width="50%" class=text-center>Hardware</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="padding: 10px;" width="50%" class=text-center>{{vm.appliance}}</td>
									<td style="padding: 10px;" width="50%" class=text-center>{{vm.hardware}}</td>
								</tr>

							</tbody>
						</table>

						<!--  COMPONENTS -->
						<table class=table>
							<thead class=active>
								<tr>
									<th style="padding: 10px;" width="100%" class=text-center
										colspan="7"><h3>Components</h3></th>
								</tr>
							</thead>
							<tbody ng-repeat="component in vm.components">
								<tr class="success" width="100%">
									<td style="padding: 10px;" colspan=1><a
										class="btn btn-link" ng-init="toggle[$index] = false"
										ng-click="toggle[$index] = !toggle[$index]"> <span
											class="glyphicon glyphicon-plus-sign" ng-if="!toggle[$index]"></span>
											<span class="glyphicon glyphicon-minus-sign"
											ng-if="toggle[$index]"></span>
									</a></td>
									<td style="padding: 10px;" colspan=6 class=text-center><h4>{{component.component_id}}</h4></td>

								</tr>
								
								<tr width="100%" colspan=7 ng-if="toggle[$index]">

									<th style="padding: 10px;" width="20%" class=text-center>Implementation
										Step</th>
									<th style="padding: 10px;" width="40%" class=text-center>Cookbook</th>
									<th style="padding: 10px;" width="40%" class=text-center>Recipe</th>
								</tr>
								<tr width="100%" ng-if="toggle[$index]">
									<td style="padding: 10px;" width="20%" class=text-center>{{component.implementation_step}}</td>
									<td style="padding: 10px;" width="40%" class=text-center>{{component.cookbook}}</td>
									<td style="padding: 10px;" width="40%" class=text-center>{{component.recipe}}</td>

								</tr>
								<tr class="active" ng-if="toggle[$index]">
									<th style="padding: 10px;" width="100%" class=text-center
										colspan="7"><h4>FIREWALL</h4></th>
								</tr>

								<tr ng-repeat="proto in component.firewall.incoming.proto" ng-if="toggle[$parent.$index]">
									<td width="100%" class=text-center colspan="6"><h5>Incoming
											Rules</h5>
										<table class=table>
											<tbody>
												<tr>

													<th style="padding: 10px;" width="50%" class=text-center>Port
														TYPE</th>
													<th style="padding: 10px;" width="50%" class=text-center>PortList</th>
												</tr>


												<tr>
													<td style="padding: 10px;" width="50%" class=text-center>
														<p>{{proto.type}}</p>
													</td>

													<td style="padding: 10px;" width="50%" class=text-center>
														<p ng-repeat="portList in proto.port_list">
															{{portList}}</p>
													</td>

												</tr>
											</tbody>
										</table>
									<td>
								</tr>

								<tr ng-repeat="proto in component.firewall.outcoming.proto" ng-if="toggle[$parent.$index]">
									<td width="100%" class=text-center colspan="6"><h5>Outcoming
											Rules</h5>
										<table class=table>
											<tbody>
												<tr>

													<th style="padding: 10px;" width="50%" class=text-center>Port
														TYPE</th>
													<th style="padding: 10px;" width="50%" class=text-center>PortList</th>
												</tr>


												<tr>
													<td style="padding: 10px;" width="50%" class=text-center>
														<p>{{proto.type}}</p>
													</td>

													<td style="padding: 10px;" width="50%" class=text-center>
														<p ng-repeat="portList in proto.port_list">
															{{portList}}</p>
													</td>

												</tr>
											</tbody>
										</table>
									<td>
								</tr>
							
							</tbody>
						</table>

						</br>
					</div>

					</br>
				</div>


				<br>
			</div>

		</div>
	</div>

</div>