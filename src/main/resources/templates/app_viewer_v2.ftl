<div class=container>

	<div class="panel panel-default">
		<div class="panel-heading text-center">
			<span class="lead">
				<h2>Application Details</h2>
			</span>
		</div>

		<div class="panel-body">

			<table class="table table-hover">
				<thead>
					<tr style="width: 100%;">
						<td style="width: 50%;"><b>Application</b></td>
						<td style="width: 50%;"><b>Active Component</b></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="width: 50%;"><p>
								<b>Name: </b> {{ctrl.application.kanbanId === null ?
								ctrl.application.name : ctrl.application.kanbanId}}
							</p></td>
						<td style="width: 50%;"><p>
								<b>Name: </b> {{ctrl.application.activeComponent.name}}
							</p></td>
					</tr>
					<tr>
						<td style="width: 50%;"><p>
								<b>State: </b> {{ctrl.application.state}}
							</p></td>
						<td style="width: 50%;"><p>
								<b>State: </b> {{ctrl.application.activeComponent.state}}
							</p></td>
					</tr>
					<!-- <tr>
						<td style="width: 50%;"><button style="text-align: center;"
								class="btn btn-primary" ng-click="ctrl.syncAppWithComposer()">Sync
								App with Composer</button> <label class="btn btn-primary btn-file"
							id="input-cypher"> Submit MACM <input type="file"
								style="display: none;">
						</label></td>
						<td style="width: 50%;"></td>
					</tr> -->
				</tbody>
			</table>

		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading text-center">
			<span class="lead">
				<h2>Components Details</h2>
			</span>
		</div>

		<div class="panel-body">
			<div style="text-align: left;">Is your component a MUSA AC Agent? You
				can use this SLA id: 5A05BBDB2CDCEE6A29AA2EB1</div>
			</br>
			<div style="text-align: left;">Is your component a MUSA IDM agent?
				You can use this SLA id:</div>
			</br>
			<div class="panel panel-default"
				ng-repeat="component in ctrl.application.components">
				<div class="panel-body">
					<table border="1" style="width: 100%;">
						<tr>

							<td style="width: 25%;">
								<div style="padding: 3px; font-size: 140%;">
									<b>{{component.name}}</b>
								</div>
								<div style="padding: 3px; font-size: 120%;">
									<b>State: </b> {{component.state}}
								</div>
								<div style="padding: 3px; font-size: 120%;">
									<b>Group: </b> {{component.componentGroup}}
								</div>
								<button style="text-align: center; width: 100px;"
									class="btn btn-success"
									ng-show="component.id == ctrl.application.activeComponent.id"
									ng-disabled="true">Selected</button>
								<button style="text-align: center; width: 100px;"
									class="btn btn-warning"
									ng-click="ctrl.changeSelectedComponent(component)"
									ng-show="component.id != ctrl.application.activeComponent.id">Select</button>
							</td>
							<td style="width: 75%; height: 100%;">

								<div
									style="border-style: dotted; border-width: 1px; padding: 10px;">
									<div style="text-align: center; font-size: 120%;">
										<b>SLAT and SLA</b>
									</div>
									<table style="width: 100%;">
										<tr>

											<td style="width: 40%;"><div
													style="text-align: left; font-size: 120%;">
													<b>SLAT State: </b> {{component.state == 'SLAT_GENERATED' ?
													'REQUIREMENT' : component.state == 'SLAT_ASSESSED'  ?
													'ASSESSED' : 'NOT AVAILABLE'}}
												</div>
												<div style="text-align: left; font-size: 120%;">
													<b>SLA: </b> <span
														ng-class="component.state != 'SLA_GENERATED' ? 'text-danger' : 'text-success'">{{component.state
														!= 'SLA_GENERATED' ? 'NOT AVAILABLE' : 'AVAILABLE'}}</span>
												</div></td>

											<td style="width: 60%;">Set an existent required SLA:
												<div class="input-group-btn">


													<div class="input-group" style="width: 100%;">

														<span class="input-group-btn"> <label
															class="btn btn-primary btn-file"
															ng-click="ctrl.submitGeneratedSLA(component)"
															id="input-sla">Submit </label>
														</span> <input type="text" class="form-control"
															ng-model="component.slaId" placeholder="SLA ID">
													</div>


												</div> </br> Set an existent assessed SLA:
												<div class="input-group-btn">


													<div class="input-group" style="width: 100%;">

														<span class="input-group-btn"> <label
															class="btn btn-primary btn-file"
															ng-click="ctrl.submitAssessedSLA(component)"
															id="input-sla">Submit </label>
														</span> <input type="text" class="form-control"
															ng-model="component.assessedSlaId" placeholder="SLA ID">
													</div>


												</div>

											</td>
										</tr>
									</table>
								</div>
							</td>


						</tr>
					</table>
				</div>
			</div>


		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading text-center">
			<span class="lead">
				<h2>Add New Component</h2>
			</span>
		</div>


		<div class="panel-body">
			<input type="text" class="form-control" id="compname"
				ng-model="ctrl.compname" placeholder="Component Name"></br> <select
				class="form-control"
				ng-options="compType for compType in ctrl.compTypes"
				ng-model="ctrl.selectedCompType" ng-change="ctrl.loadSlaContent()"></select>
			</br>
			<div style="text-align: center;">
				<button style="text-align: center;" class="btn btn-primary"
					ng-click="ctrl.addNewComponent()">Add Component</button>
			</div>
		</div>
	</div>

</div>