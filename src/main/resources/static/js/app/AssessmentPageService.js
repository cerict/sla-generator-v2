'use strict';

angular.module('slaGenerator').factory('AssessmentPageService',
		['$localStorage', '$http', '$q', 'urls',
			function ($localStorage, $http, $q, urls) {

			var factory = {
					getControlFamilies : getControlFamilies,
					submitControlFamilies : submitControlFamilies,
					getComponentQuestions : getComponentQuestions,
					submitQuestionnaire : submitQuestionnaire,
					getComponentAssessedMetrics : getComponentAssessedMetrics,
					generateSla : generateSla,
					storeSla : storeSla
			};


			return factory;

			function getControlFamilies() {

				var deferred = $q.defer();

				$http(
						{
							method : 'GET',
							url : urls.ASSESSMENT_NEW_API+'/getControlFamilies'
						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}
			
			function submitControlFamilies(componentId, selectedControlFamilies){
				var deferred = $q.defer();

				$http(
						{
							method : 'POST',
							url : urls.ASSESSMENT_NEW_API+'/submitControlFamilies',
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
									obj) {
								var str = [];
								for ( var p in obj)
									str
									.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'componentId' : componentId,
								'controlFamilies' : JSON.stringify(selectedControlFamilies),
							}

						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										response) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}
			
			function getComponentQuestions(componentId) {

				var deferred = $q.defer();

				$http(
						{
							method : 'GET',
							url : urls.ASSESSMENT_NEW_API+'/getComponentQuestions?componentId='+componentId
						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}

			function submitQuestionnaire(questions) {

				var deferred = $q.defer();

				var fd = new FormData();
				fd.append('activeComponentQuestions', JSON.stringify(questions));
				fd.append('data', 'string');
				$http.post(urls.ASSESSMENT_NEW_API+"/submitQuestionnaire", fd, {
					transformRequest: angular.identity,
					headers: {'Content-Type': undefined}
				})
				.success(function(response){
					deferred.resolve(response.data);
				})
				.error(function(errResponse){
					deferred.reject(errResponse);
				});
				return deferred.promise;
			}

			function getComponentAssessedMetrics(componentId) {

				var deferred = $q.defer();

				$http(
						{
							method : 'GET',
							url : urls.SLAGENERATOR_API+'/componentAssessedMetrics?componentId='+componentId
						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}

			function generateSla(componentId, componentMetrics) {
				var deferred = $q.defer();

				$http(
						{
							method : 'POST',
							url : urls.SLAGENERATOR_API+'/generateAssessedSla',
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
									obj) {
								var str = [];
								for ( var p in obj)
									str
									.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'componentId' : componentId,
								'componentMetrics' : JSON.stringify(componentMetrics),
							}

						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										response) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}

			function storeSla(componentId, slaXml, isAssessedActive, musaJwtToken, musaUserId) {
				var deferred = $q.defer();

				$http(
						{
							method : 'POST',
							url : urls.SLAGENERATOR_API+"/storeSla",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
									obj) {
								var str = [];
								for ( var p in obj)
									str
									.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'slaXml' : slaXml,
								'componentId' : componentId,
								'isAssessedActive' : isAssessedActive,
								'jwtToken' : musaJwtToken,
								'userId' : musaUserId
							}

						})
						.then(
								function successCallback(
										response) {
									console.log("Response in Service: "+response);
									console.log("Response data in Service: "+response.data);
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									console.error('Error while storing the sla : '+errResponse.data.errorMessage);
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}
		}
		]);