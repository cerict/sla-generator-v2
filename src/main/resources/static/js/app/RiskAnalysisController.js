'use strict';

angular.module('slaGenerator').controller('RiskAnalysisController',
		['$sessionStorage', 'RiskAnalysisService', '$scope', '$location', '$state', '$window',  function( $sessionStorage, RiskAnalysisService, $scope, $location, $state, $window) {

			console.log("AppViewController called");

			var self = this;

			console.log("RiskAnalysisController -> musa jwt token: "+$sessionStorage.musaJwtToken);
			console.log("AppSetupController -> musa user id: "+$sessionStorage.musaUserId);
			
			//Variables
			self.application = $sessionStorage.application;
			self.selectedTab = "Start";

			console.log(self.application);

			//Functions
			self.startRiskAnalysis = startRiskAnalysis;
			self.loadQuestions = loadQuestions;
			self.submitQuestions = submitQuestions;
			self.updateRiskEvaluation = updateRiskEvaluation;
			self.saveEvaluation = saveEvaluation;
			self.getStrideRisk = getStrideRisk;
			self.submitControls = submitControls;
			
			self.nextToRiskEvaluation = nextToRiskEvaluation;
			self.nextToControlsSelection = nextToControlsSelection;
			self.nextToAnalysisResult = nextToAnalysisResult;
			
			$scope.getTabSelected = function (tabName) {
				return (tabName === self.selectedTab) ? 'active' : '';
			}

			function startRiskAnalysis(){
				self.selectedTab = "Threats Selection";
			}

			function loadQuestions(){
				return RiskAnalysisService.getComponentQuestions(self.application.activeComponent.id)
				.then(function (response) {
					console.log('Questions get successfully '+JSON.stringify(response));
					$sessionStorage.application.activeComponent.threatQuestions = response;
					for (var i = 0 ; i < $sessionStorage.application.activeComponent.threatQuestions.length; i++) {
						console.log("add selected");
						$sessionStorage.application.activeComponent.threatQuestions[i].selected = "true";
					}
					self.application = $sessionStorage.application;
					alert("Questions are successfully loaded!\nPlease answer the questions!");
				},
				function (errResponse) {
					console.error('Error while get Questions ' + errResponse.data.message);
					self.errorMessage = 'Error while get Questions: ' + errResponse.data.message;
					alert('Error while get Questions!\n' + errResponse.data.message);
				}
				);
			}

			function submitQuestions(){
				return RiskAnalysisService.submitQuestions(self.application.activeComponent.id, self.application.activeComponent.threatQuestions)
				.then(function (response) {
					console.log('Threats get successfully '+JSON.stringify(response));
					$sessionStorage.application.activeComponent.threats = response;
					self.application = $sessionStorage.application;
					alert("Threats are successfully loaded!\n");
				},
				function (errResponse) {
					console.error('Error while get Threats ' + errResponse.data.message);
					self.errorMessage = 'Error while get Threats: ' + errResponse.data.message;
					alert('Error while get Threats!\n' + errResponse.data.message);
				}
				);
			}

			function updateRiskEvaluation(){
				for ( var i = 0; i < self.application.activeComponent.threats.length; i++){
					var totaleImpactBusiness = (self.application.activeComponent.threats[i].financialDamage +
							self.application.activeComponent.threats[i].reputationDamage +
							self.application.activeComponent.threats[i].nonCompliance +
							self.application.activeComponent.threats[i].privacyViolation) / 4;

					var totaleImpactTechnical = (self.application.activeComponent.threats[i].lossOfConfidentiality +
							self.application.activeComponent.threats[i].lossOfIntegrity +
							self.application.activeComponent.threats[i].lossOfAvailability +
							self.application.activeComponent.threats[i].lossOfAccountability) / 4;

					if(totaleImpactBusiness==0){
						self.application.activeComponent.threats[i].impact=totaleImpactTechnical;
					}else{
						self.application.activeComponent.threats[i].impact=totaleImpactBusiness;
					}

					if(self.application.activeComponent.threats[i].impact >= 6){
						if (self.application.activeComponent.threats[i].likehood < 3){
							self.application.activeComponent.threats[i].riskSeverity = "MEDIUM";
						}else if (self.application.activeComponent.threats[i].likehood >= 3 && 
								self.application.activeComponent.threats[i].likehood < 6){
							self.application.activeComponent.threats[i].riskSeverity = "HIGH";
						}else if (self.application.activeComponent.threats[i].likehood >= 6){
							self.application.activeComponent.threats[i].riskSeverity = "CRITICAL";
						}
					}else if(self.application.activeComponent.threats[i].impact >= 3 && 
							self.application.activeComponent.threats[i].impact < 6){
						if (self.application.activeComponent.threats[i].likehood < 3){
							self.application.activeComponent.threats[i].riskSeverity = "LOW";
						}else if (self.application.activeComponent.threats[i].likehood >= 3 && 
								self.application.activeComponent.threats[i].likehood < 6){
							self.application.activeComponent.threats[i].riskSeverity = "MEDIUM";
						}else if (self.application.activeComponent.threats[i].likehood >= 6){
							self.application.activeComponent.threats[i].riskSeverity = "HIGH";
						}
					}else if(self.application.activeComponent.threats[i].impact < 3){
						if (self.application.activeComponent.threats[i].likehood < 3){
							self.application.activeComponent.threats[i].riskSeverity = "VERY LOW";
						}else if (self.application.activeComponent.threats[i].likehood >= 3 && 
								self.application.activeComponent.threats[i].likehood < 6){
							self.application.activeComponent.threats[i].riskSeverity = "LOW";
						}else if (self.application.activeComponent.threats[i].likehood >= 6){
							self.application.activeComponent.threats[i].riskSeverity = "MEDIUM";
						}
					}
				}

				$sessionStorage.application = self.application;
			}

			function saveEvaluation(){
				return RiskAnalysisService.saveEvaluation(self.application.activeComponent.id, self.application.activeComponent.threats,self.getStrideRisk('SPOOFING'), self.getStrideRisk('TAMPERING'), self.getStrideRisk('REPUDIATION'), self.getStrideRisk('INFORMATION DISCLOSURE'), self.getStrideRisk('DENIAL OF SERVICE'), self.getStrideRisk('ELEVATION OF PRIVILEGES'))
				.then(function (response) {
					console.log('Evaluation saved successfully '+JSON.stringify(response));
					$sessionStorage.application.activeComponent.controls = response;
					self.application = $sessionStorage.application;
					alert("Evaluation saved successfully!\n");
				},
				function (errResponse) {
					console.error('Error while save evaluation ' + errResponse.data.message);
					self.errorMessage = 'Error while save evaluation: ' + errResponse.data.message;
					alert('Error while save evaluation!\n' + errResponse.data.message);
				}
				);
			}

			function getStrideRisk(strideName){
				console.log("getStrideRisk called");
				var strideValue = 0;
				var numOfElements = 0;
				for ( var i = 0; i < self.application.activeComponent.threats.length; i++){
					for ( var j = 0; j < self.application.activeComponent.threats[i].threat.strides.length; j++){
						if(self.application.activeComponent.threats[i].threat.strides[j].name == strideName){
							if(self.application.activeComponent.threats[i].riskSeverity == "VERY LOW"){
								strideValue = strideValue + 1;
								numOfElements++;
							}else if(self.application.activeComponent.threats[i].riskSeverity == "LOW"){
								strideValue = strideValue + 3;
								numOfElements++;
							}else if(self.application.activeComponent.threats[i].riskSeverity == "MEDIUM"){
								strideValue = strideValue + 5;
								numOfElements++;
							}else if(self.application.activeComponent.threats[i].riskSeverity == "HIGH"){
								strideValue = strideValue + 7;
								numOfElements++;
							}else if(self.application.activeComponent.threats[i].riskSeverity == "CRITICAL"){
								strideValue = strideValue + 9;
								numOfElements++;
							}
						}
					}
				}
				if(numOfElements > 0){
					strideValue = strideValue / numOfElements;
				}
				
				if(strideValue>= 0 && strideValue < 2){
					return "VERY LOW";
				}else if(strideValue>= 2 && strideValue < 4){
					return "LOW";
				}else if(strideValue>= 4 && strideValue < 6){
					return "MEDIUM";
				}else if(strideValue>= 6 && strideValue < 8){
					return "HIGH";
				}else if(strideValue>= 8){
					return "CRITICAL";
				}
			}
			
			function submitControls(){
				return RiskAnalysisService.submitControls(self.application.activeComponent.id, self.application.activeComponent.controls, $sessionStorage.musaJwtToken, $sessionStorage.musaUserId)
				.then(function (response) {
					console.log('Controls saved successfully '+JSON.stringify(response));
					alert("Controls saved successfully!\n");
				},
				function (errResponse) {
					console.error('Error while save Controls ' + errResponse.data.message);
					self.errorMessage = 'Error while save Controls: ' + errResponse.data.message;
					alert('Error while save Controls!\n' + errResponse.data.message);
				}
				);
			}
			
			function nextToRiskEvaluation(){
				self.selectedTab = 'Risks Evaluation';
				$window.scrollTo(0, 0);
			}
			
			function nextToControlsSelection(){
				self.selectedTab = 'Controls Selection';
				$window.scrollTo(0, 0);
			}
			
			function nextToAnalysisResult(){
				self.selectedTab = 'Analysis Results';
				$window.scrollTo(0, 0);
			}

		}]);