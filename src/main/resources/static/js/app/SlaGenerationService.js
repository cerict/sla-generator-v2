'use strict';

angular.module('slaGenerator').factory('SlaGenerationService',
		['$sessionStorage','$localStorage', '$http', '$q', 'urls',
			function ($sessionStorage, $localStorage, $http, $q, urls) {

			var factory = {
					getComponentMetrics: getComponentMetrics,
					generateSla: generateSla,
					storeSla: storeSla,
					useAssessmentResult: useAssessmentResult,
			};

			return factory;
			

			function getComponentMetrics(componentId) {

				var deferred = $q.defer();

				$http(
						{
							method : 'GET',
							url : urls.SLAGENERATOR_API+'/componentMetrics?componentId='+componentId
						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}

			function generateSla(componentId, componentMetrics) {
				var deferred = $q.defer();

				$http(
						{
							method : 'POST',
							url : urls.SLAGENERATOR_API+'/generateSla',
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
									obj) {
								var str = [];
								for ( var p in obj)
									str
									.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'componentId' : componentId,
								'componentMetrics' : JSON.stringify(componentMetrics),
							}

						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}

			
			function storeSla(componentId, slaXml, isAssessedActive, musaJwtToken, musaUserId) {
				var deferred = $q.defer();

				$http(
						{
							method : 'POST',
							url : urls.SLAGENERATOR_API+"/storeSla",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
									obj) {
								var str = [];
								for ( var p in obj)
									str
									.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'slaXml' : slaXml,
								'componentId' : componentId,
								'isAssessedActive' : isAssessedActive,
								'jwtToken' : musaJwtToken,
								'userId' : musaUserId
							}

						})
						.then(
								function successCallback(
										response) {
									console.log("Response in Service: "+response);
									console.log("Response data in Service: "+response.data);
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									console.error('Error while storing the sla : '+errResponse.data.errorMessage);
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}
			
			function useAssessmentResult(slat, componentId) {
				console.log('Uploading Risk Json from Assessment Result, componentId '+componentId);
				var deferred = $q.defer();

				$http(
						{
							method : 'POST',
							url : urls.RISKJSON_API+"/fromAssessment",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
									obj) {
								var str = [];
								for ( var p in obj)
									str
									.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'componentId' : componentId,
								'assessmentJson' : slat
							}

						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										response) {
									console.error('Error while uploading Risk Json from Assessment Result : '+errResponse.data.errorMessage);
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}

		}
		]);