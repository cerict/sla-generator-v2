'use strict';

angular.module('slaGenerator').factory('AppSetupService',
		['$sessionStorage','$localStorage', '$http', '$q', 'urls',
			function ($sessionStorage, $localStorage, $http, $q, urls) {

			var factory = {
					setuppAppFromKanban: setuppAppFromKanban,
					removeAllApps: removeAllApps,
					getAllApps: getAllApps,
					getSlaXml: getSlaXml,
					getAppFromId: getAppFromId,
					createNewApp: createNewApp,
					importAppFromMACM: importAppFromMACM,
					submitAppMacm: submitAppMacm
			};

			return factory;

			function setuppAppFromKanban(appId, componentId, jwtToken, userId){
				console.log("Call setuppAppFromKanban");
				var deferred = $q.defer();

				$http(
						{
							method : 'POST',
							url : urls.APPSETUP+"/setuppAppFromKanban",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
									obj) {
								var str = [];
								for ( var p in obj)
									str
									.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'appId' : appId,
								'componentId' : componentId,
								'jwtToken' : jwtToken,
								'userId' : userId,
							}

						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}

			function removeAllApps(){
				var deferred = $q.defer();

				$http(
						{
							method : 'GET',
							url : urls.APPSETUP+'/removeAllApps'
						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}

			function getAllApps(){
				var deferred = $q.defer();

				$http(
						{
							method : 'GET',
							url : urls.APPSETUP+'/applications'
						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}

			function getAppFromId(appId){
				var deferred = $q.defer();

				$http(
						{
							method : 'GET',
							url : urls.APPSETUP+'/applications?appId='+appId
						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}

			function getSlaXml(slaId){
				var deferred = $q.defer();

				$http(
						{
							method : 'GET',
							url : urls.MANAGESLA_API+'/getSlaContent?slaId='+slaId
						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}
			
			function createNewApp(appName){
				var deferred = $q.defer();

				$http(
						{
							method : 'POST',
							url : urls.APPSETUP+"/applications/new",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
									obj) {
								var str = [];
								for ( var p in obj)
									str
									.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'appName' : appName,
							}

						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}
			
			function importAppFromMACM(appId){
				var deferred = $q.defer();

				$http(
						{
							method : 'GET',
							url : urls.APPSETUP+'/importAppFromMACM?appId='+appId
						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}
			
			function submitAppMacm(appMacm, appMacmId){
				var deferred = $q.defer();

				$http(
						{
							method : 'POST',
							url : urls.APPSETUP+"/setupAppFromMACM",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
									obj) {
								var str = [];
								for ( var p in obj)
									str
									.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'appMacm' : appMacm,
								'appMacmId' : appMacmId,
							}

						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}

		}
		]);