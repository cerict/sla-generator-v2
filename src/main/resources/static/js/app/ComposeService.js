'use strict';

angular.module('slaGenerator').factory('ComposeService',
		['$sessionStorage','$localStorage', '$http', '$q', 'urls',
			function ($sessionStorage, $localStorage, $http, $q, urls) {

			var factory = {
					composeApplication: composeApplication,
					retieveComposedSLAS: retieveComposedSLAS,
					loadSlaContent: loadSlaContent,
					prepareComposition: prepareComposition
			};

			return factory;
			
			function prepareComposition(appId, musaJwtToken, musaUserId) {
				console.log('Prepare composition for app with id: '+appId);
				var deferred = $q.defer();

				$http(
						{
							method : 'POST',
							url : urls.COMPOSER+"/prepareComposition",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
									obj) {
								var str = [];
								for ( var p in obj)
									str
									.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'appId': appId,
								'jwtToken' : musaJwtToken,
								'userId' : musaUserId
							}

						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}
			
			function composeApplication(appId, musaJwtToken, musaUserId) {
				console.log('Composing app with id: '+appId);
				var deferred = $q.defer();

				$http(
						{
							method : 'POST',
							url : urls.COMPOSER+"/composeApplication",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
									obj) {
								var str = [];
								for ( var p in obj)
									str
									.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'appId': appId,
								'jwtToken' : musaJwtToken,
								'userId' : musaUserId
							}

						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}
			
			
			function retieveComposedSLAS(appId, musaJwtToken, musaUserId) {
				console.log('Retriving SLA for app with id: '+appId);
				var deferred = $q.defer();

				$http(
						{
							method : 'POST',
							url : urls.COMPOSER+"/retieveComposedSLAS",
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							transformRequest : function(
									obj) {
								var str = [];
								for ( var p in obj)
									str
									.push(encodeURIComponent(p)
											+ "="
											+ encodeURIComponent(obj[p]));
								return str.join("&");
							},
							data : {
								'appId': appId,
								'jwtToken' : musaJwtToken,
								'userId' : musaUserId
							}

						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
								},
								function errorCallback(
										errResponse) {
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}
			
			function loadSlaContent(slaId) {

				var deferred = $q.defer();

				$http(
						{
							method : 'GET',
							url : urls.MANAGESLA_API+'/getSlaContent?slaId='+slaId
						})
						.then(
								function successCallback(
										response) {
									deferred.resolve(response.data);
									console.log("Sla content: "+response.data);
									
								},
								function errorCallback(
										response) {
									console.error('Error while load Sla content : '+errResponse.data.errorMessage);
									deferred.reject(errResponse);
								});

				return deferred.promise;
			}
		}
		]);