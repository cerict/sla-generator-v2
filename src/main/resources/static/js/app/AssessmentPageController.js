'use strict';

angular.module('slaGenerator')
.controller('AssessmentPageController',
		['$sessionStorage', 'AssessmentPageService', '$scope', '$window', '$stateParams', '$location', '$state', function($sessionStorage, AssessmentPageService, $scope, $window, $stateParams, $location, $state) {

			var self = this;

			/* Session variables used:
			 * $sessionStorage.application // contains the application in use
			 */

			self.application = $sessionStorage.application;

			console.log("SlaGenerationController -> musa jwt token: "+$sessionStorage.musaJwtToken);
			console.log("AppSetupController -> musa user id: "+$sessionStorage.musaUserId);

			//Local variables
			self.activeControl = null;
			self.controlFamilies = [];
			self.selectedControlFamilies = [];
			self.activeComponentQuestions = [];

			//Funzioni
			self.getControlFamilies = getControlFamilies;
			self.addSelectedControlFamily = addSelectedControlFamily;
			self.submitControlFamilies = submitControlFamilies;
			self.getComponentQuestions = getComponentQuestions;
			self.submitQuestionnaire = submitQuestionnaire;
			self.generateSla = generateSla;
			self.storeSla = storeSla;
			self.updateQuestionsAnswer = updateQuestionsAnswer;

			if(self.application.activeComponent.assessedSlaXml != null){
				LoadXMLString('slaXmlString',self.application.activeComponent.assessedSlaXml);
			}
			
			console.log("self.activeComponentQuestions.length "+self.activeComponentQuestions.length)
			if(self.application.activeComponent.hasQuestions != null && 
					self.application.activeComponent.hasQuestions == true){
				getComponentQuestions();
			}


			function addSelectedControlFamily(){
				self.selectedControlFamilies.push($scope.selectedControlFamily);
				$scope.selectedControlFamily = null;
			}

			function getControlFamilies(){
				return AssessmentPageService.getControlFamilies()
				.then(function (response) {
					console.log("Get Control Families response: "+JSON.stringify(response));
					self.controlFamilies = response;
				},
				function (errResponse) {
					console.log('Error while get Control Families'+JSON.stringify(errResponse));
					self.errorMessage = 'Error while get Control Families: ' + errResponse.data.message;
					alert('Error while get Control Families: ' + errResponse.data.message);
				}
				);
			}

			function submitControlFamilies(){
				return AssessmentPageService.submitControlFamilies(self.application.activeComponent.id, self.selectedControlFamilies)
				.then(function (response) {
					for(var  i = 0; i < response.length; i++){
						response[i].control.collapsed = 'false';
					}
					console.log('Submit Control Families successfully');
					console.log("Number of controls for questions on Control Families: "+response.length);
					self.activeComponentQuestions = response;
					$sessionStorage.application.activeComponent.hasQuestions = true;
					self.application = $sessionStorage.application;
					$state.reload();
				},
				function (errResponse) {
					console.log('Error while submitControlFamilies '+JSON.stringify(errResponse));
					self.errorMessage = 'Error while Submit Control Families: ' + errResponse.data.message;
					alert('Error while Submit Control Families: ' + errResponse.data.message);
				}
				);
			}

			function getComponentQuestions(){
				return AssessmentPageService.getComponentQuestions(self.application.activeComponent.id)
				.then(function (response) {
					console.log("Get Component Questions OK!");
					console.log("Number of controls for questions on Component: "+response.length);

					for(var  i = 0; i < response.length; i++){
						response[i].control.collapsed = 'false';
						for(var j = 0; j < response[i].controlQuestions.length; j++){
							if(response[i].controlQuestions[j].answer == ""){
								response[i].controlQuestions[j].answer = null;
							}
						}
					}
					console.log("TEST TEST Control Questions: "+JSON.stringify(response));
					self.activeComponentQuestions = response;
					$sessionStorage.application.activeComponent.hasQuestions = true;
					self.application = $sessionStorage.application;
				},
				function (errResponse) {
					console.log('Error while get questions '+JSON.stringify(errResponse));
					self.errorMessage = 'Error while get questions: ' + errResponse.data.message;
					alert('Error while get questions: ' + errResponse.data.message);
				}
				);
			}

			function submitQuestionnaire(){
				$sessionStorage.application = self.application;
				console.log("Number of control for questions submit: "+self.activeComponentQuestions.length);

				for(var i = 0; i < self.activeComponentQuestions.length; i=i+20){
					console.log("Send questions from "+(i+1)+" to "+(i+20));
					var limit = 20;
					if((self.activeComponentQuestions.length - i) < limit){
						limit = self.activeComponentQuestions.length - i;
					}
					var questionsToSend = [];
					for(var j = i; j < (i+limit); j++){
						questionsToSend.push(self.activeComponentQuestions[j]);
					}
					if(i+20 >= self.activeComponentQuestions.length){
						return AssessmentPageService.submitQuestionnaire(questionsToSend)
						.then(function (response) {
							console.log("questions from "+i+" to "+(i+20)+" submitted successfully");
							if(i+20 >= self.activeComponentQuestions.length){
								alert('Questionnaire submitted successfully!\n');
								getComponentAssessedMetrics();
							}
						},
						function (errResponse) {
							console.error('Error while submitting questionnaire');
							alert('Error while submitting questionnaire!\n' + errResponse.data);
							self.errorMessage = 'Error while submitting questionnaire' + errResponse.data;
						}
						);
					}else{
						AssessmentPageService.submitQuestionnaire(questionsToSend)
						.then(function (response) {
							console.log("questions from "+i+" to "+(i+20)+" submitted successfully");
						},
						function (errResponse) {
							console.error('Error while submitting questionnaire');
							alert('Error while submitting questionnaire!\n' + errResponse.data);
							self.errorMessage = 'Error while submitting questionnaire' + errResponse.data;
							return;
						}
						);
					}

				}
			}

			function getComponentAssessedMetrics(){
				console.log("getComponentAssessedMetrics called");
				return AssessmentPageService.getComponentAssessedMetrics(self.application.activeComponent.id)
				.then(function (response) {
					console.log('Assessed Metrics get successfully')
					console.log('Number of metrics: '+response.length);
					$sessionStorage.application.activeComponent.assessedMetrics = response;
					self.application = $sessionStorage.application;
					//LoadXMLString('slaXmlString',self.componentDetails.slaXml);
					//alert("SLOs from SLA Assessment are successfully loaded!\nChoose your SLOs!");
				},
				function (errResponse) {
					console.error('Error while get Assessed Metrics');
					self.errorMessage = 'Error while get Assessed Metrics: ' + errResponse.data.errorMessage;
				}
				);
			}

			function generateSla(){
				return AssessmentPageService.generateSla(self.application.activeComponent.id, self.application.activeComponent.assessedMetrics)
				.then(function (response) {
					console.log('Assessed Sla generated successfully'+response.data);
					alert("Assessed SLA generated successfully'");
					$sessionStorage.application.activeComponent.assessedSlaXml = response.data;
					self.application = $sessionStorage.application;
					LoadXMLString('slaXmlString',self.application.activeComponent.assessedSlaXml);
				},
				function (errResponse) {
					console.error('Error while generating Sla');
					self.errorMessage = 'Error while generating Sla: ' + errResponse.data.errorMessage;
				}
				);
			}

			function storeSla(){
				console.log("store Sla called");
				return AssessmentPageService.storeSla(self.application.activeComponent.id, self.application.activeComponent.assessedSlaXml, true, $sessionStorage.musaJwtToken, $sessionStorage.musaUserId)
				.then(function (response) {
					console.log('Assessed Sla stored successfully');
					alert("Assessed SLA stored successfully with id "+response);
					self.application.activeComponent.assessedSlaId = response;
					self.application.activeComponent.state = "SLAT_ASSESSED";

					for( var i = 0; i < self.application.components.length; i++){
						if(self.application.components[i].id == self.application.activeComponent.id &&
								self.application.components[i].kanbanId == self.application.activeComponent.kanbanId){
							self.application.components[i].assessedSlaId = response;
							self.application.components[i].state = "SLAT_ASSESSED";
							break;
						}
					}

					$sessionStorage.application = self.application;
				},
				function (errResponse) {
					console.error('Error while storing Sla');
					self.errorMessage = 'Error while storing Sla: ' + errResponse.data.errorMessage;
				}
				);
			}

			function updateQuestionsAnswer(question){
				console.log("Changed item source id: "+JSON.stringify(question.questionControl.sourceId));
				console.log("Answer: "+JSON.stringify(question.answer));

				for(var i = 0; i < self.activeComponentQuestions.length; i++){
					for(var j = 0; j < self.activeComponentQuestions[i].controlQuestions.length; j++){
						if(self.activeComponentQuestions[i].controlQuestions[j].questionControl.sourceId == question.questionControl.sourceId){
							console.log("Another Question with same id found!");
							self.activeComponentQuestions[i].controlQuestions[j].answer = question.answer;
						}
					}
				}
			}

		}

		]);